// $Id: fusion-marketshare-script.js 7196 2010-04-26 16:49:22Z jeremy $

Drupal.behaviors.fusion_marketshareFirstWord = function (context) {
  $('#site-name a').each(function(){
      var bt = $(this);
      bt.html( bt.text().replace(/(^\w+)/,'<span class="first-word">$1</span>') );
  });
  $('.marketshare-dualcolortitle-topgradientbg h2.block-title').each(function(){
      var bt = $(this);
      bt.html( bt.text().replace(/(^\w+)/,'<span class="first-word">$1</span>') );
  });
};

Drupal.behaviors.fusion_marketshareRoundedCorner = function (context) {
  $(".marketshare-goldblocktitle-taupebg-menulistseparators .inner").corner("bottom 5px"); 
  $("#footer-inner .marketshare-dark-background-blue .inner").corner("top 5px"); 
  $(".marketshare-dark-background-blue .inner").corner("5px"); 
};