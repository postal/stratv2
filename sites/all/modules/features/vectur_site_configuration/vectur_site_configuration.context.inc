<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function vectur_site_configuration_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'advanced_search_blocks';
  $context->description = 'Advanced search block in different pages';
  $context->tag = 'Search';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'relationships' => 'relationships',
        'user/*' => 'user/*',
        'bookmarks' => 'bookmarks',
        'groups' => 'groups',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-1' => array(
          'module' => 'block',
          'delta' => 1,
          'region' => 'sidebar_first',
          'weight' => 10,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Advanced search block in different pages');
  t('Search');

  $export['advanced_search_blocks'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'catalog-main-page';
  $context->description = '';
  $context->tag = 'Catalog';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'catalog' => 'catalog',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uc_catalog-0' => array(
          'module' => 'uc_catalog',
          'delta' => 0,
          'region' => 'sidebar_first',
          'weight' => 0,
        ),
        'vectur_filters-7' => array(
          'module' => 'vectur_filters',
          'delta' => 7,
          'region' => 'sidebar_first',
          'weight' => 1,
        ),
        'views-news_and_updates-block_1' => array(
          'module' => 'views',
          'delta' => 'news_and_updates-block_1',
          'region' => 'sidebar_last',
          'weight' => 0,
        ),
        'views-featured_products-block_1' => array(
          'module' => 'views',
          'delta' => 'featured_products-block_1',
          'region' => 'content_bottom',
          'weight' => 0,
        ),
        'views-3cd5088d8ed91f666884301780c05c62' => array(
          'module' => 'views',
          'delta' => '3cd5088d8ed91f666884301780c05c62',
          'region' => 'content_bottom',
          'weight' => 1,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Catalog');

  $export['catalog-main-page'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'catalog-solr-view';
  $context->description = 'Solr search filters in catalog view';
  $context->tag = 'Search';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'catalog/*' => 'catalog/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'apachesolr_search-im_vid_6' => array(
          'module' => 'apachesolr_search',
          'delta' => 'im_vid_6',
          'region' => 'sidebar_first',
          'weight' => 0,
        ),
        'apachesolr_ubercart-fs_uc_sell_price' => array(
          'module' => 'apachesolr_ubercart',
          'delta' => 'fs_uc_sell_price',
          'region' => 'sidebar_first',
          'weight' => 1,
        ),
        'apachesolr_votingapi-sis_votes_average' => array(
          'module' => 'apachesolr_votingapi',
          'delta' => 'sis_votes_average',
          'region' => 'sidebar_first',
          'weight' => 2,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Search');
  t('Solr search filters in catalog view');

  $export['catalog-solr-view'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'category_sublinks';
  $context->description = 'Category sublinks block';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'catalog' => 'catalog',
        'catalog/*' => 'catalog/*',
        'product/search/*' => 'product/search/*',
        'product/search' => 'product/search',
        'content/*' => 'content/*',
        'bestsellers' => 'bestsellers',
        'bestsellers/*' => 'bestsellers/*',
        'deals' => 'deals',
        'deals/*' => 'deals/*',
      ),
    ),
    'theme' => array(
      'values' => array(
        'cleancommerce' => 'cleancommerce',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'vectur_filters-6' => array(
          'module' => 'vectur_filters',
          'delta' => 6,
          'region' => 'header_top',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Category sublinks block');
  t('blocks');

  $export['category_sublinks'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'category_sublinks_for_marketshare';
  $context->description = 'Category sublinks block for market share theme';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'catalog' => 'catalog',
        'catalog/*' => 'catalog/*',
        'product/search/*' => 'product/search/*',
        'product/search' => 'product/search',
        'content/*' => 'content/*',
        'bestsellers' => 'bestsellers',
        'bestsellers/*' => 'bestsellers/*',
        'deals' => 'deals',
        'deals/*' => 'deals/*',
      ),
    ),
    'theme' => array(
      'values' => array(
        'vectur' => 'vectur',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'vectur_filters-6' => array(
          'module' => 'vectur_filters',
          'delta' => 6,
          'region' => 'header',
          'weight' => 10,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Category sublinks block for market share theme');
  t('blocks');

  $export['category_sublinks_for_marketshare'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'clean_commerce_blocks';
  $context->description = 'Common enabled in clean commerce theme';
  $context->tag = 'blocks';
  $context->conditions = array(
    'theme' => array(
      'values' => array(
        'cleancommerce' => 'cleancommerce',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-user_meta-block_1' => array(
          'module' => 'views',
          'delta' => 'user_meta-block_1',
          'region' => 'header_top',
          'weight' => -20,
        ),
        'commons_core-header_login' => array(
          'module' => 'commons_core',
          'delta' => 'header_login',
          'region' => 'preface_top',
          'weight' => 0,
        ),
        'registration-my_login_block' => array(
          'module' => 'registration',
          'delta' => 'my_login_block',
          'region' => 'preface_top',
          'weight' => 1,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Common enabled in clean commerce theme');
  t('blocks');

  $export['clean_commerce_blocks'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'clean_commerce_site_header_image';
  $context->description = 'Site header image block for clean commerce theme';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user' => 'user',
      ),
    ),
    'theme' => array(
      'values' => array(
        'cleancommerce' => 'cleancommerce',
      ),
    ),
    'user' => array(
      'values' => array(
        'anonymous user' => 'anonymous user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'boxes-site_header_image' => array(
          'module' => 'boxes',
          'delta' => 'site_header_image',
          'region' => 'header_top',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Site header image block for clean commerce theme');
  t('blocks');

  $export['clean_commerce_site_header_image'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'fusion_mrkt_blocks';
  $context->description = 'Blocks for fusion marketshare theme';
  $context->tag = 'blocks';
  $context->conditions = array(
    'theme' => array(
      'values' => array(
        'vectur' => 'vectur',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'commons_core-header_login' => array(
          'module' => 'commons_core',
          'delta' => 'header_login',
          'region' => 'header',
          'weight' => 0,
        ),
        'views-user_meta-block_1' => array(
          'module' => 'views',
          'delta' => 'user_meta-block_1',
          'region' => 'header',
          'weight' => 1,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks for fusion marketshare theme');
  t('blocks');

  $export['fusion_mrkt_blocks'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'fusion_mrkt_search_blocks';
  $context->description = 'Fusion marketshare search block';
  $context->tag = 'blocks';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'search/*' => 'search/*',
      ),
    ),
    'theme' => array(
      'values' => array(
        'vectur' => 'vectur',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'apachesolr_search-type' => array(
          'module' => 'apachesolr_search',
          'delta' => 'type',
          'region' => 'sidebar_first',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Fusion marketshare search block');
  t('blocks');

  $export['fusion_mrkt_search_blocks'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'market_solr_search_filters';
  $context->description = 'Solr search filters';
  $context->tag = 'Search';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'search/*' => 'search/*',
        'admin/store/products/uc_prodimport/review' => 'admin/store/products/uc_prodimport/review',
        'admin/store/products/uc_prodimport/review/*' => 'admin/store/products/uc_prodimport/review/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'apachesolr_search-im_vid_4' => array(
          'module' => 'apachesolr_search',
          'delta' => 'im_vid_4',
          'region' => 'sidebar_first',
          'weight' => 0,
        ),
        'apachesolr_search-im_vid_6' => array(
          'module' => 'apachesolr_search',
          'delta' => 'im_vid_6',
          'region' => 'sidebar_first',
          'weight' => 1,
        ),
        'apachesolr_ubercart-fs_uc_sell_price' => array(
          'module' => 'apachesolr_ubercart',
          'delta' => 'fs_uc_sell_price',
          'region' => 'sidebar_first',
          'weight' => 2,
        ),
        'apachesolr_votingapi-sis_votes_average' => array(
          'module' => 'apachesolr_votingapi',
          'delta' => 'sis_votes_average',
          'region' => 'sidebar_first',
          'weight' => 3,
        ),
        'apachesolr-mlt-001' => array(
          'module' => 'apachesolr',
          'delta' => 'mlt-001',
          'region' => 'sidebar_last',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Search');
  t('Solr search filters');

  $export['market_solr_search_filters'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product-blocks';
  $context->description = 'Blocks on the product pages';
  $context->tag = 'Product';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'product' => 'product',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uc_cart-0' => array(
          'module' => 'uc_cart',
          'delta' => 0,
          'region' => 'sidebar_last',
          'weight' => 0,
        ),
        'product_view_fixes-video' => array(
          'module' => 'product_view_fixes',
          'delta' => 'video',
          'region' => 'sidebar_last',
          'weight' => 1,
        ),
        'product_view_fixes-specs' => array(
          'module' => 'product_view_fixes',
          'delta' => 'specs',
          'region' => 'sidebar_last',
          'weight' => 2,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks on the product pages');
  t('Product');

  $export['product-blocks'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_search_bar';
  $context->description = 'Puts product search on its place';
  $context->tag = 'blocks';
  $context->conditions = array(
    'user' => array(
      'values' => array(
        'authenticated user' => 'authenticated user',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'vectur_filters-0' => array(
          'module' => 'vectur_filters',
          'delta' => 0,
          'region' => 'mainbar',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Puts product search on its place');
  t('blocks');

  $export['product_search_bar'] = $context;
  return $export;
}
