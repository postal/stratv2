<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function vectur_site_configuration_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: primary-links:admin/store/products/uc_prodimport
  $menu_links['primary-links:admin/store/products/uc_prodimport'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'admin/store/products/uc_prodimport',
    'router_path' => 'admin/store/products/uc_prodimport',
    'link_title' => 'Import and export products',
    'options' => array(
      'attributes' => array(
        'title' => 'Import and export products into/from CSV file',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '30',
    'parent_path' => 'user',
  );
  // Exported menu link: primary-links:node/add
  $menu_links['primary-links:node/add'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Create content',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '1',
    'weight' => '20',
    'parent_path' => 'user',
  );
  // Exported menu link: primary-links:node/add/news
  $menu_links['primary-links:node/add/news'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'node/add/news',
    'router_path' => 'node/add/news',
    'link_title' => 'Add news',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
    'parent_path' => 'node/add',
  );
  // Exported menu link: primary-links:node/add/product
  $menu_links['primary-links:node/add/product'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'node/add/product',
    'router_path' => 'node/add/product',
    'link_title' => 'Add a Product',
    'options' => array(
      'attributes' => array(
        'title' => 'This node displays the representation of a product for sale on the website. It includes all the unique information that can be attributed to a specific model number.',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
    'parent_path' => 'node/add',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add a Product');
  t('Add news');
  t('Create content');
  t('Import and export products');


  return $menu_links;
}
