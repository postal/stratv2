<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function vectur_site_configuration_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_deals
  $groups['product-group_deals'] = array(
    'group_type' => 'standard',
    'type_name' => 'product',
    'group_name' => 'group_deals',
    'label' => 'Deals',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => 'Information about current deal.',
      ),
      'display' => array(
        'weight' => '16',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'description' => '',
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '17',
    'fields' => array(
      '0' => 'field_promotion_dates',
      '1' => 'field_deal_description',
    ),
  );

  // Exported group: group_images
  $groups['product-group_images'] = array(
    'group_type' => 'standard',
    'type_name' => 'product',
    'group_name' => 'group_images',
    'label' => 'Images',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'sticky' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'grid' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'list' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '3',
    'fields' => array(
      '0' => 'field_image_cache',
    ),
  );

  // Exported group: group_specs
  $groups['product-group_specs'] = array(
    'group_type' => 'standard',
    'type_name' => 'product',
    'group_name' => 'group_specs',
    'label' => 'Specifications',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'sticky' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'grid' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'list' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '6',
    'fields' => array(
      '0' => 'field_spec',
    ),
  );

  // Exported group: group_technical
  $groups['product-group_technical'] = array(
    'group_type' => 'standard',
    'type_name' => 'product',
    'group_name' => 'group_technical',
    'label' => 'Technical Details',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'sticky' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'grid' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'list' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '5',
    'fields' => array(
      '0' => 'field_technical',
      '1' => 'field_size',
      '2' => 'field_volume',
      '3' => 'field_volume_units',
      '4' => 'field_concentration',
      '5' => 'field_concentration_units',
      '6' => 'field_format',
      '7' => 'field_storage_temp',
      '8' => 'field_shelf_life',
      '9' => 'field_shelf_life_units',
      '10' => 'field_storage_cons',
      '11' => 'field_product_cons',
      '12' => 'field_safety_cons',
      '13' => 'field_sertifcates',
      '14' => 'field_guaranty',
      '15' => 'field_guaranty_units',
      '16' => 'field_warranty',
      '17' => 'field_warranty_units',
    ),
  );

  // Exported group: group_video
  $groups['product-group_video'] = array(
    'group_type' => 'standard',
    'type_name' => 'product',
    'group_name' => 'group_video',
    'label' => 'Video',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'sticky' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'grid' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'list' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '4',
    'fields' => array(
      '0' => 'field_video',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Deals');
  t('Images');
  t('Specifications');
  t('Technical Details');
  t('Video');

  return $groups;
}
