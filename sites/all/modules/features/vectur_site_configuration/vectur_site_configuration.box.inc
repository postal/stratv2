<?php

/**
 * Implementation of hook_default_box().
 */
function vectur_site_configuration_default_box() {
  $export = array();
  $box = new stdClass;
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'logout_block';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'Logout block';
  $box->options = array(
    'body' => '<p>#primary_links_tree#</p><div id="account-block"><a href="/user">My account</a></div><div id="logout-block"><a href="/logout">Logout</a></div>',
    'format' => '1',
  );

  $export['logout_block'] = $box;
  $box = new stdClass;
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'site_header_image';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'site_header_image';
  $box->options = array(
    'body' => '<div id="site-header"><img src="/sites/all/themes/cleancommerce/images/site_header.png"></div>',
    'format' => '1',
  );

  $export['site_header_image'] = $box;
  $box = new stdClass;
  $box->disabled = FALSE; /* Edit this to true to make a default box disabled initially */
  $box->api_version = 1;
  $box->delta = 'site_logo';
  $box->plugin_key = 'simple';
  $box->title = '';
  $box->description = 'logo';
  $box->options = array(
    'body' => '<div id="site-logo"><a href="/"><img src="/sites/all/themes/cleancommerce/images/site_logo.png"></a></div>',
    'format' => '1',
  );

  $export['site_logo'] = $box;
  return $export;
}
