<?php

/**
 * Implementation of hook_user_default_roles().
 */
function vectur_site_configuration_user_default_roles() {
  $roles = array();

  // Exported role: Customer
  $roles['Customer'] = array(
    'name' => 'Customer',
  );

  // Exported role: Nx Management
  $roles['Nx Management'] = array(
    'name' => 'Nx Management',
  );

  // Exported role: Nx Support
  $roles['Nx Support'] = array(
    'name' => 'Nx Support',
  );

  // Exported role: Product administrator
  $roles['Product administrator'] = array(
    'name' => 'Product administrator',
  );

  // Exported role: Rep
  $roles['Rep'] = array(
    'name' => 'Rep',
  );

  // Exported role: Sales Support
  $roles['Sales Support'] = array(
    'name' => 'Sales Support',
  );

  // Exported role: Seller Agent
  $roles['Seller Agent'] = array(
    'name' => 'Seller Agent',
  );

  // Exported role: anonymous user
  $roles['anonymous user'] = array(
    'name' => 'anonymous user',
  );

  // Exported role: authenticated user
  $roles['authenticated user'] = array(
    'name' => 'authenticated user',
  );

  return $roles;
}
