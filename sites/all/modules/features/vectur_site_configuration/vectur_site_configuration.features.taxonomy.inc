<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function vectur_site_configuration_taxonomy_default_vocabularies() {
  return array(
    'manufacturer' => array(
      'name' => 'Manufacturer',
      'description' => '',
      'help' => '',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '1',
      'required' => '1',
      'tags' => '0',
      'module' => 'features_manufacturer',
      'weight' => '0',
      'nodes' => array(
        'product' => 'product',
      ),
    ),
  );
}
