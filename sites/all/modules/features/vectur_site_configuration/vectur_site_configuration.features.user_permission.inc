<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function vectur_site_configuration_user_default_permissions() {
  $permissions = array();

  // Exported permission: Use PHP input for field settings (dangerous - grant with care)
  $permissions['Use PHP input for field settings (dangerous - grant with care)'] = array(
    'name' => 'Use PHP input for field settings (dangerous - grant with care)',
    'roles' => array(),
  );

  // Exported permission: act as seller
  $permissions['act as seller'] = array(
    'name' => 'act as seller',
    'roles' => array(
      '0' => 'Commission level 1',
      '1' => 'Commission level 2',
      '2' => 'Commission level 3',
      '3' => 'Commission level 4',
      '4' => 'Commission level 5',
      '5' => 'Seller Agent',
      '6' => 'site admin',
    ),
  );

  // Exported permission: administer custom breadcrumbs
  $permissions['administer custom breadcrumbs'] = array(
    'name' => 'administer custom breadcrumbs',
    'roles' => array(
      '0' => 'site admin',
    ),
  );

  // Exported permission: administer own product features
  $permissions['administer own product features'] = array(
    'name' => 'administer own product features',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'Seller Agent',
      '2' => 'site admin',
    ),
  );

  // Exported permission: administer product classes
  $permissions['administer product classes'] = array(
    'name' => 'administer product classes',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: administer product features
  $permissions['administer product features'] = array(
    'name' => 'administer product features',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: administer products
  $permissions['administer products'] = array(
    'name' => 'administer products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: administer sellers
  $permissions['administer sellers'] = array(
    'name' => 'administer sellers',
    'roles' => array(
      '0' => 'site admin',
    ),
  );

  // Exported permission: create news content
  $permissions['create news content'] = array(
    'name' => 'create news content',
    'roles' => array(
      '0' => 'Nx Support',
      '1' => 'content manager',
    ),
  );

  // Exported permission: create products
  $permissions['create products'] = array(
    'name' => 'create products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: create profile content
  $permissions['create profile content'] = array(
    'name' => 'create profile content',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'authenticated user',
      '2' => 'content manager',
    ),
  );

  // Exported permission: delete all products
  $permissions['delete all products'] = array(
    'name' => 'delete all products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: delete any news content
  $permissions['delete any news content'] = array(
    'name' => 'delete any news content',
    'roles' => array(
      '0' => 'Nx Management',
    ),
  );

  // Exported permission: delete any profile content
  $permissions['delete any profile content'] = array(
    'name' => 'delete any profile content',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'content manager',
      '2' => 'site admin',
    ),
  );

  // Exported permission: delete own news content
  $permissions['delete own news content'] = array(
    'name' => 'delete own news content',
    'roles' => array(
      '0' => 'Nx Management',
    ),
  );

  // Exported permission: delete own products
  $permissions['delete own products'] = array(
    'name' => 'delete own products',
    'roles' => array(
      '0' => 'Product administrator',
    ),
  );

  // Exported permission: delete own profile content
  $permissions['delete own profile content'] = array(
    'name' => 'delete own profile content',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'authenticated user',
      '2' => 'content manager',
      '3' => 'site admin',
    ),
  );

  // Exported permission: edit all products
  $permissions['edit all products'] = array(
    'name' => 'edit all products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: edit any news content
  $permissions['edit any news content'] = array(
    'name' => 'edit any news content',
    'roles' => array(
      '0' => 'Nx Management',
    ),
  );

  // Exported permission: edit any profile content
  $permissions['edit any profile content'] = array(
    'name' => 'edit any profile content',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'content manager',
    ),
  );

  // Exported permission: edit field_brand
  $permissions['edit field_brand'] = array(
    'name' => 'edit field_brand',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_catalog
  $permissions['edit field_catalog'] = array(
    'name' => 'edit field_catalog',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_concentration
  $permissions['edit field_concentration'] = array(
    'name' => 'edit field_concentration',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_concentration_units
  $permissions['edit field_concentration_units'] = array(
    'name' => 'edit field_concentration_units',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_deal_description
  $permissions['edit field_deal_description'] = array(
    'name' => 'edit field_deal_description',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_featured
  $permissions['edit field_featured'] = array(
    'name' => 'edit field_featured',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_format
  $permissions['edit field_format'] = array(
    'name' => 'edit field_format',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_guaranty
  $permissions['edit field_guaranty'] = array(
    'name' => 'edit field_guaranty',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_guaranty_units
  $permissions['edit field_guaranty_units'] = array(
    'name' => 'edit field_guaranty_units',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_image_cache
  $permissions['edit field_image_cache'] = array(
    'name' => 'edit field_image_cache',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_manufacturer
  $permissions['edit field_manufacturer'] = array(
    'name' => 'edit field_manufacturer',
    'roles' => array(),
  );

  // Exported permission: edit field_poll_body
  $permissions['edit field_poll_body'] = array(
    'name' => 'edit field_poll_body',
    'roles' => array(),
  );

  // Exported permission: edit field_product_cons
  $permissions['edit field_product_cons'] = array(
    'name' => 'edit field_product_cons',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_promotion_dates
  $permissions['edit field_promotion_dates'] = array(
    'name' => 'edit field_promotion_dates',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_safety_cons
  $permissions['edit field_safety_cons'] = array(
    'name' => 'edit field_safety_cons',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_sertifcates
  $permissions['edit field_sertifcates'] = array(
    'name' => 'edit field_sertifcates',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_shelf_life
  $permissions['edit field_shelf_life'] = array(
    'name' => 'edit field_shelf_life',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_shelf_life_units
  $permissions['edit field_shelf_life_units'] = array(
    'name' => 'edit field_shelf_life_units',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_size
  $permissions['edit field_size'] = array(
    'name' => 'edit field_size',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_spec
  $permissions['edit field_spec'] = array(
    'name' => 'edit field_spec',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_storage_cons
  $permissions['edit field_storage_cons'] = array(
    'name' => 'edit field_storage_cons',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_storage_temp
  $permissions['edit field_storage_temp'] = array(
    'name' => 'edit field_storage_temp',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_technical
  $permissions['edit field_technical'] = array(
    'name' => 'edit field_technical',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_video
  $permissions['edit field_video'] = array(
    'name' => 'edit field_video',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_volume
  $permissions['edit field_volume'] = array(
    'name' => 'edit field_volume',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_volume_units
  $permissions['edit field_volume_units'] = array(
    'name' => 'edit field_volume_units',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_warranty
  $permissions['edit field_warranty'] = array(
    'name' => 'edit field_warranty',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit field_warranty_units
  $permissions['edit field_warranty_units'] = array(
    'name' => 'edit field_warranty_units',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit own news content
  $permissions['edit own news content'] = array(
    'name' => 'edit own news content',
    'roles' => array(
      '0' => 'Nx Support',
    ),
  );

  // Exported permission: edit own products
  $permissions['edit own products'] = array(
    'name' => 'edit own products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'Seller Agent',
    ),
  );

  // Exported permission: edit own profile content
  $permissions['edit own profile content'] = array(
    'name' => 'edit own profile content',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'authenticated user',
      '2' => 'content manager',
    ),
  );

  // Exported permission: export own products
  $permissions['export own products'] = array(
    'name' => 'export own products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'Seller Agent',
      '2' => 'site admin',
    ),
  );

  // Exported permission: export products
  $permissions['export products'] = array(
    'name' => 'export products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: fulfill own orders
  $permissions['fulfill own orders'] = array(
    'name' => 'fulfill own orders',
    'roles' => array(
      '0' => 'Seller Agent',
      '1' => 'site admin',
    ),
  );

  // Exported permission: import products
  $permissions['import products'] = array(
    'name' => 'import products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: update any products
  $permissions['update any products'] = array(
    'name' => 'update any products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'site admin',
    ),
  );

  // Exported permission: update own products
  $permissions['update own products'] = array(
    'name' => 'update own products',
    'roles' => array(
      '0' => 'Product administrator',
      '1' => 'Seller Agent',
      '2' => 'site admin',
    ),
  );

  // Exported permission: use php in custom breadcrumbs
  $permissions['use php in custom breadcrumbs'] = array(
    'name' => 'use php in custom breadcrumbs',
    'roles' => array(
      '0' => 'site admin',
    ),
  );

  // Exported permission: view field_brand
  $permissions['view field_brand'] = array(
    'name' => 'view field_brand',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_catalog
  $permissions['view field_catalog'] = array(
    'name' => 'view field_catalog',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_concentration
  $permissions['view field_concentration'] = array(
    'name' => 'view field_concentration',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_concentration_units
  $permissions['view field_concentration_units'] = array(
    'name' => 'view field_concentration_units',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_deal_description
  $permissions['view field_deal_description'] = array(
    'name' => 'view field_deal_description',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_featured
  $permissions['view field_featured'] = array(
    'name' => 'view field_featured',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_format
  $permissions['view field_format'] = array(
    'name' => 'view field_format',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_guaranty
  $permissions['view field_guaranty'] = array(
    'name' => 'view field_guaranty',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_guaranty_units
  $permissions['view field_guaranty_units'] = array(
    'name' => 'view field_guaranty_units',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_image_cache
  $permissions['view field_image_cache'] = array(
    'name' => 'view field_image_cache',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_manufacturer
  $permissions['view field_manufacturer'] = array(
    'name' => 'view field_manufacturer',
    'roles' => array(),
  );

  // Exported permission: view field_poll_body
  $permissions['view field_poll_body'] = array(
    'name' => 'view field_poll_body',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_product_cons
  $permissions['view field_product_cons'] = array(
    'name' => 'view field_product_cons',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_promotion_dates
  $permissions['view field_promotion_dates'] = array(
    'name' => 'view field_promotion_dates',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_safety_cons
  $permissions['view field_safety_cons'] = array(
    'name' => 'view field_safety_cons',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_sertifcates
  $permissions['view field_sertifcates'] = array(
    'name' => 'view field_sertifcates',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_shelf_life
  $permissions['view field_shelf_life'] = array(
    'name' => 'view field_shelf_life',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_shelf_life_units
  $permissions['view field_shelf_life_units'] = array(
    'name' => 'view field_shelf_life_units',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_size
  $permissions['view field_size'] = array(
    'name' => 'view field_size',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_spec
  $permissions['view field_spec'] = array(
    'name' => 'view field_spec',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_storage_cons
  $permissions['view field_storage_cons'] = array(
    'name' => 'view field_storage_cons',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_storage_temp
  $permissions['view field_storage_temp'] = array(
    'name' => 'view field_storage_temp',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_technical
  $permissions['view field_technical'] = array(
    'name' => 'view field_technical',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_video
  $permissions['view field_video'] = array(
    'name' => 'view field_video',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_volume
  $permissions['view field_volume'] = array(
    'name' => 'view field_volume',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_volume_units
  $permissions['view field_volume_units'] = array(
    'name' => 'view field_volume_units',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_warranty
  $permissions['view field_warranty'] = array(
    'name' => 'view field_warranty',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view field_warranty_units
  $permissions['view field_warranty_units'] = array(
    'name' => 'view field_warranty_units',
    'roles' => array(
      '0' => 'anonymous user',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: view own products
  $permissions['view own products'] = array(
    'name' => 'view own products',
    'roles' => array(
      '0' => 'Seller Agent',
      '1' => 'site admin',
    ),
  );

  // Exported permission: write own order comments
  $permissions['write own order comments'] = array(
    'name' => 'write own order comments',
    'roles' => array(
      '0' => 'Seller Agent',
      '1' => 'site admin',
    ),
  );

  return $permissions;
}
