// $Id$

/**
 * @file
 * Customer-facing JavaScript for UC Variable Price.
 */

Drupal.behaviors.ucVarPriceShowArb = function(context) {
  var varpriceSel = $('#edit-varprice-sel');
  if (varpriceSel.length) {
    // Using drop-down menu
    if (varpriceSel.val() !== 'other') {
      $('#edit-varprice-arb-wrapper:not(.processed)').addClass('processed').hide();
    }
    varpriceSel.change(function() {
      if ($(this).val() === 'other') {
        $('#edit-varprice-arb-wrapper').show('fast');
        $('#edit-varprice-arb').focus();
      }
      else {
        $('#edit-varprice-arb-wrapper').hide('fast');
      }
    });
  }
  else {
    // Using radio buttons
    if ($('input[name="varprice_sel"]:checked').val() !== 'other') {
      $('#edit-varprice-arb-wrapper:not(.processed)').addClass('processed').hide();
    }
    $('input[name="varprice_sel"]').change(function() {
      if ($('input[name="varprice_sel"]:checked').val() === 'other') {
        $('#edit-varprice-arb-wrapper').show('fast');
        $('#edit-varprice-arb').focus();
      }
      else {
        $('#edit-varprice-arb-wrapper').hide('fast');
      }
    });
  }
};
