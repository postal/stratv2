<?php

function views_ipp_views_data_alter(&$data) {
  $data['views']['ipp'] = array(
    'title' => t('Items per page'),
    'help' => t('User selectable.'),
    'filter' => array(
      'handler' => 'views_handler_filter_views_ipp',
    ),
  );

  return $data;
}

function views_ipp_views_handlers() {
  return array(
    'handlers' => array(
      'views_handler_filter_views_ipp' => array(
        'parent' => 'views_handler_filter',
      ),
    )
  );
}