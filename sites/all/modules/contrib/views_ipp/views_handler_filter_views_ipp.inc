<?php

class views_handler_filter_views_ipp extends views_handler_filter {
  var $no_single = TRUE;
  var $no_operator = TRUE;
  
  function options(&$options) {
    parent::options($options);
    $options['allowed_values'] = '10,15,25,50,100';  
  }
  
  function get_value_options() {
    $tmp = explode(',', $this->options['allowed_values']);
    $this->value_options = array_combine($tmp, $tmp);
  }
  
  function has_extra_options() { return TRUE; }
  
  function extra_options_form(&$form, &$form_state) {
    $form['allowed_values'] = array(
      '#type' => 'textfield',
      '#title' => t('Allowed values'),
      '#default_value' => $this->options['allowed_values'],
      '#description' => t('Comma separated values.'),
    );
  }
  
  function extra_options_validate($form, &$form_state) {
    $allowed_values = explode(',', $form_state['values']['options']['allowed_values']);
    
    if (empty($form_state['values']['options']['allowed_values']) || !is_array($allowed_values)) {
      form_error($form['allowed_values'], t('Error.'));
    }
  }
  
  function value_form(&$form, &$form_state) {
    $this->get_value_options();
    $options = $this->value_options;
    $default_value = $this->value;
    
    $form['value'] = array(
      '#type' => 'select',
      '#title' => empty($form_state['exposed']) ? t('Value') : '',
      '#options' => $options,
      '#default_value' => $default_value,
    );
    
    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      if (!isset($form_state['input'][$identifier])) {
        $form_state['input'][$identifier] = $default_value;
      }
    }
  }
    
  function query() {
    $this->view->set_items_per_page($this->value);
  }
}