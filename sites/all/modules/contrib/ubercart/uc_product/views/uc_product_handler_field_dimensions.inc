<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

class uc_product_handler_field_dimensions extends views_handler_field {

  public function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function element_type() {
    if ( isset($this->definition['element type']) ){
      return $this->definition['element type'];
    }
    return 'span';
  }

  function render($values) {
    return sprintf(
      '%s x %s x %s %s',
      $values->{$this->aliases['height']},
      $values->{$this->aliases['length']},
      $values->{$this->aliases['width']},
      $values->{$this->aliases['length_units']}
    );
  }

}