<?php
// $Id: uc_rec.admin.inc,v 1.1 2009/07/04 18:16:42 danithaca Exp $

/**
 * @see http://drupal.org/project/uc_rec
 */

function uc_rec_settings_form() {
  $form = array();
  
  $form['display_num'] = array(
    '#title' => t('Number of products to display in the block.'),
    '#type' => 'select',
    '#default_value' => variable_get('uc_rec_display_num', 5),
    '#options' => array(
      3 => '3',
      5 => '5',
      8 => '8',
      10 => '10',
      15 => '15',
    ),
    '#description' => t('Please specify how many products to show in the recommendation block list. After setup here, please generate recommendation at admin/settings/recommender and then enable the blocks in the admin/build/block section'), 
  );

  
  /*$form['boost_recency'] = array(
    '#title' => t('Boost recent items'),
    '#type' => 'checkbox',
    '#description' => t('Check the box to boost recommendation based on order recency.'),
    '#default_value' => variable_get('uc_rec_boost_recency', 1),
  );*/
  
  /*$form['algorithm'] = array(
    '#title' => t('Choose recommender algorithm'),
    '#type' => 'radios',
    '#default_value' => variable_get('uc_rec_algorithm', 'classical'),
    '#options' => array(
      'cooccurrence' => t("Cooccurrence (less accurate; better performance)"),
      'classical' => t("Classical (more accurate; slower performance)"),
    ),
    '#description' => t('Different algorithms are useful in different situations. Please leave it as the default classical algorithm unless you have large number of nodes/users.')
  );*/
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  
  return $form;
}

function uc_rec_settings_form_submit($form, &$form_state) {
  $display_num = $form_state['values']['display_num'];
  //$boost_recency = $form_state['values']['boost_recency'];
  //$algorithm = $form_state['values']['algorithm']; // default is the classical algorithm
  
  variable_set('uc_rec_display_num', $display_num);
  //variable_set('uc_rec_boost_recency', $boost_recency);
  //variable_set('uc_rec_algorithm', $algorithm);
  
  drupal_set_message(t("The configuration options have been saved."));
}