README -- Ubercart Recommender

This module provides two blocks:
1) Users who ordered this product also ordered
2) Recommended for you

This module requires Recommender API at http://drupal.org/project/recommender.

After configure/setup, please go to admin/settings/recommender to generate the
recommendations. And then turn on the blocks.

Enjoy!