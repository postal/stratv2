Description
-----------
This simple module replaces default search path 'search/apachesolr_search/%' 
by Apache Solr Search Integration module with more appealing
'search/content/%' as discussed on http://drupal.org/node/524366.

Requirements
------------
Drupal 6
Apache Solr Search Integration module

Installation
------------
1. Download and extract the recommended version of the module from
http://drupal.org/project/custom_search_path to modules directory of your Drupal installation.

2. Login as an administrator. Enable the module in the "Administer" -> "Site
   Building" -> "Modules"

