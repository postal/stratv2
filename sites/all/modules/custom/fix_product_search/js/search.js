if ( Drupal.jsEnabled ){
  Drupal.behaviors.fixAdvancedSearch = function(){
    if ( $("#search-form #edit-keys").length > 0 && $("#search-box input[type=text]").length > 0){
      $("#search-box form").submit(function(){
        $("#search-form #edit-keys").val($("#search-box input[type=text]").val());
        $("#search-form").submit();
        return false;
      });
    }

    if ( $("#vectur-filters-advanced-search-form").length > 0 && $("#vectur-filters-form input[type=text]").length > 0){
      $("#vectur-filters-form").submit(function(){
        $("#vectur-filters-advanced-search-form input[name='any']").val('');
        $("#vectur-filters-advanced-search-form input[name='phrase']").val($("#vectur-filters-form input[type=text]").val());
        cat = parseint($('#vectur-filters-form select[name="vectur_category"]').val());
        if ( cat > 0 ) {
          $('#vectur-filters-advanced-search-form #edit-category-1 option').removeAttr('selected');
          $('#vectur-filters-advanced-search-form #edit-category-1 option[value='+ cat + ']').attr('selected', 'selected');
          $("#vectur-filters-advanced-search-form").submit();
        }
        return false;
      });
    }


  }
}
