<?php

/*
 *
 */

function views_improvements_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'views_improvements') . '/includes',
    ),
    'handlers' => array(
      'views_improvements_catalog_sort' => array(
        'parent' => 'views_handler_sort',
      ),
      'views_improvements_predefined_date_range_filter' => array(
        'parent' => 'views_handler_filter',
      ),
      'views_improvements_fivestar_filter' => array(
        'parent' => 'views_handler_filter',
      ),
      'views_improvements_handler_filter_fulltext' => array(
        'parent' => 'views_handler_filter_string',
      ),
      'views_improvements_handler_filter_node_permissions' => array(
        'parent' => 'views_handler_filter_many_to_one',
      ),
      'views_improvements_handler_filter_curent_user_permissions' => array(
        'parent' => 'views_handler_filter_in_operator',
      )
    )
  );
}

function views_improvements_views_data() {
  $data = array( );
  $data['content_type_product']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );
  $data['uc_products']['catalog_predefined'] = array(
    'title' => t('Catalog predefined'),
    'help' => t('Sort by bestsellers, price and so on'),
    'sort' => array(
      'handler' => 'views_improvements_catalog_sort',
    )
  );
  $data['uc_products']['date_predefined'] = array(
    'title' => t('Predefined date ranges'),
    'help' => t('Filter by selected date range'),
    'filter' => array(
      'handler' => 'views_improvements_predefined_date_range_filter',
    )
  );
  $data['votingapi_cache']['fivestar'] = array(
    'title' => t('Fivestar Rating'),
    'help' => t('Filter by rating of 5'),
    'filter' => array(
      'handler' => 'views_improvements_fivestar_filter',
    ),
  );
  $data['uc_products']['complex_filter'] = array(
    'title' => t('Complex search'),
    'help' => t('Search text in different fields'),
    'filter' => array(
      'handler' => 'views_improvements_handler_filter_fulltext',
      'search_fields' => array(
        'node.title',
        'uc_products.model',
        'content_type_product.field_technical_value',
        'content_type_product.field_deal_description_value',
        'content_type_product.field_featured_value',
        'content_type_product.field_volume_value',
        'content_type_product.field_volume_units_value',
        'content_type_product.field_concentration_value',
        'content_type_product.field_concentration_units_value',
        'content_type_product.field_storage_temp_value',
        'content_type_product.field_format_value',
        'content_type_product.field_size_value',
        'content_type_product.field_shelf_life_units_value',
        'content_type_product.field_storage_cons_value',
        'content_type_product.field_product_cons_value',
        'content_type_product.field_safety_cons_value',
        'content_type_product.field_guaranty_units_value',
        'content_type_product.field_warranty_units_value',
      )
    )
  );
  $data['node']['perm'] = array(
    'title' => t('Permissions'),
    'real field' => 'uid',
    'filter' => array(
      'handler' => 'views_improvements_handler_filter_node_permissions',
    ),
  );
  $data['users']['current_have_access'] = array(
    'title' => t('Current access'),
    'help' => t('Check current user access'),
    //'real field' => 'uid',//useless
    'filter' => array(
      'handler' => 'views_improvements_handler_filter_curent_user_permissions',
    ),
  );
  return $data;
}
