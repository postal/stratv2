<?php

/**
 * Description of views_improvements_handler_fulltext_filter
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
class views_improvements_handler_filter_fulltext extends views_handler_filter_string {

  public $search_fields = array( );

  public function init(&$view, $options) {
    parent::init($view, $options);
    if ( isset($this->definition['search_fields']) ){
      $this->search_fields = $this->definition['search_fields'];
    }
  }

  public function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  public function query() {
    /* @var $query views_query */
    $query = &$this->query;

    $this->ensure_my_table();
    //$field = "$this->table_alias.$this->real_field";
    $upper = $this->case_transform();
    $info = $this->operators();
    $this->options['group'] = $query->set_where_group('OR');

    foreach( $this->search_fields as $field ){
      $tmp = explode('.', $field);
      if ( null !== $alias = $query->ensure_table($tmp[0]) ){
        if ( !empty($info[$this->operator]['method']) ){
          $this->{$info[$this->operator]['method']}($alias.'.'.$tmp[1], $upper);
        }
      }
    }
  }

}
