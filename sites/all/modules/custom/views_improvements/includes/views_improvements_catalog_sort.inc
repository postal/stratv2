<?php

/**
 * Description of views_improvements_catalog_sort
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
class views_improvements_catalog_sort extends views_handler_sort {

  function option_defenition() {
    $options = parent::option_definition();
    $options['order'] = array(
      'default' => 'sell_price_desc',
    );
  }

  public function query() {
    parent::query();

    if ( !empty($this->options['exposed']) && !empty($this->view->exposed_input[$this->options['expose']['identifier']]) ){
      $sort = drupal_strtolower($this->view->exposed_input[$this->options['expose']['identifier']]);
    }else{
      $sort = drupal_strtolower($this->options['order']);
    }

    // Ensure sort is valid and add the field.
    $this->ensure_my_table();
    /* @var $query views_query */
    $query = &$this->query;

    switch( $sort ){
      case 'sell_price_desc':
        $query->add_orderby($this->table_alias, 'sell_price', 'desc');
        break;
      case 'sell_price_asc':
        $query->add_orderby($this->table_alias, 'sell_price', 'asc');
        break;
      case 'votes_average_desc':
        $join = new views_join();
        $join->construct(
          'votingapi_cache',
          'node',
          'nid',
          'content_id',
          array(
            array(
              'field' => 'value_type',
              'value' => 'percent',
            ),
            array(
              'field' => 'tag',
              'value' => 'vote',
            ),
            array(
              'field' => 'function',
              'value' => 'average',
            )
          ),
          'LEFT'
        );
        $query->add_relationship('votingapi_cache_node_percent_vote_average', $join, 'node');
        $query->add_orderby('votingapi_cache_node_percent_vote_average', 'value', 'desc');
        break;
      case 'common_num_sold_desc':
        $query->add_table('uc_order_products_qty_vw', 'uc_order_products_qty_vw');
        $query->add_orderby('uc_order_products_qty_vw', 'order_count', 'desc');
        break;
      default:
        break;
    }
  }

  public function sort_options() {
    return array(
      'sell_price_desc' => t('Highest to lowest price'),
      'sell_price_asc' => t('Lowest to highest price'),
      'votes_average_desc' => t('Best rated'),
      'common_num_sold_desc' => t('Most popular'),
    );
  }

  function expose_form_left(&$form, &$form_state) {
    $form['expose']['identifier'] = array(
      '#type' => 'textfield',
      '#default_value' => $this->options['expose']['identifier'],
      '#title' => t('Sort identifier'),
      '#size' => 40,
      '#description' => t('This will appear in the URL after the ? to identify this sort. Cannot be blank.'),
    );
  }

  /**
   * Handle the 'left' side fo the exposed options form.
   */
  function expose_form_right(&$form, &$form_state) {
    $form['expose']['order'] = array(
      '#type' => 'value',
      '#value' => 'sell_price_desc',
    );
  }

  /**
   * Provide default options for exposed sorts.
   */
  public function expose_options() {
    $this->options['expose'] = array(
      'order' => $this->options['order'],
      'identifier' => $this->options['id'] . '_sort',
    );
  }

  function admin_summary() {
    return '';
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['exposed'] = array( 'default' => FALSE );
    $options['order'] = array( 'default' => 'sell_price_desc' );
    $options['identifier'] = array( 'default' => 'unsorted' );

    return $options;
  }

  function exposed_info() {
    if ( empty($this->options['exposed']) ){
      return;
    }

    return array(
      'order' => $this->options['expose']['order'],
      'label' => t('Sort by: '),
      'value' => $this->options['expose']['identifier'],
    );
  }

}
