<?php

/*
 *  License text is not available
 */

class views_improvements_fivestar_filter extends views_handler_filter {

  /**
   * Plugin options
   * @return array
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['operator'] = array( 'default' => '>=' );
    return $options;
  }

  /**
   * Labels for rating values
   * @return array
   */
  private function ratings() {
    return array(
      1 => 'Poor (1 star)',    //1 star
      2 => 'Okay (2 stars)',   //2 stars
      3 => 'Good (3 stars)',   //3 stars
      4 => 'Great (4 stars)',  //4 stars
      5 => 'Awesome (5 stars)',
    );
  }

  public function value_form(&$form, &$form_state) {
    $options = $this->ratings();
    $form['value'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->getValue(),
    );
    //without this the "illegal choice" error appears. I don't know why.
    $form['value']['#value'] = $form['value']['#default_value'];
//todo
    if ( !empty($form_state['exposed']) ){
      $identifier = $this->options['expose']['identifier'];
      if ( !isset($form_state['input'][$identifier]) ){
        $form_state['input'][$identifier] = $this->value;
      }
    }
  }
  
  public function query() {
    $selected = $this->getValue() * 20; //i.e. $selected * 100 / 5
    if ( $selected == 0 ){
      return; //any
    }
    
    /* @var $query views_query */
    $query = &$this->query;
    $join = new views_join();
    $join->construct(
      'votingapi_cache', 'node', 'nid', 'content_id',
      array(
        array(
          'field' => 'value_type',
          'value' => 'percent',
        ),
        array(
          'field' => 'tag',
          'value' => 'vote',
        ),
        array(
          'field' => 'function',
          'value' => 'average',
        )
      ), 
      'LEFT'
    );
    $query->add_relationship(
      'votingapi_cache_node_percent_vote_average', 
      $join,
      'node'
    );
    $query->add_where(
      0, 
      'votingapi_cache_node_percent_vote_average.value >= %d',
      $selected
    );
    
  }

  public function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    $options = $this->ratings();
    if ( empty( $this->value) || $this->value == 'All' ){
      return t('exposed');
    }
    return t('At least %value', array('%value' => $options[$this->value]) );
  } 

  /**
   * Retreive current filter value
   * @return integer
   */
  private function getValue(){
     if ( !empty($this->options['exposed']) && !empty($this->view->exposed_input[$this->options['expose']['identifier']]) ){
      return (int)$this->view->exposed_input[$this->options['expose']['identifier']]['value'];
    }else{
      return (int)$this->options['value'];
    }
  }
}