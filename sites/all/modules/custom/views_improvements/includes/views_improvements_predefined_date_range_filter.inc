<?php

require_once drupal_get_path('module', 'date_api') . DIRECTORY_SEPARATOR . 'date_api_sql.inc';
/**
 * Description of views_improvements_predefined_date_range_filter
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
class views_improvements_predefined_date_range_filter extends views_handler_filter {

  private $query_fields;
  
  public function option_definition() {
    $options = parent::option_definition();
    $options['date_fields'] = array( 'default' => array( ) );
    $options['value']['default'] = 'current';
    unset($options['expose']['contains']['operator']);
    return $options;
  }

  public function has_extra_options() {
    return true;
  }

  private function get_date_fields() {
    $cid = 'views_improvements_fields_node';
    if ( 0 != $cached = cache_get($cid, 'cache_views') ){
      $fields = $cached->data;
      return $fields;
    }
    cache_clear_all($cid, 'cache_views');
    

    require_once('./' . drupal_get_path('module', 'views') . '/includes/admin.inc');
    $all_fields = views_fetch_fields('node', 'field');
    foreach( (array) $all_fields as $name => $val ){
      $fromto = array( );
      $tmp = explode('.', $name);
      $field_name = $tmp[1];
      $table_name = $tmp[0];
      $alias = str_replace('.', '_', $name);

      if ( !$handler = views_get_handler($table_name, $field_name, 'field') ){
        continue;
      }

      $handler_name = $handler->definition['handler'];
      $type = '';

      // For cck fields, get the date type.
      $custom = array( );
      if ( isset($handler->content_field) ){
        if ( $handler->content_field['type'] == 'date' ){
          $type = 'cck_string';
        }elseif ( $handler->content_field['type'] == 'datestamp' ){
          $type = 'cck_timestamp';
        }elseif ( $handler->content_field['type'] == 'datetime' ){
          $type = 'cck_datetime';
        }
      }

      // Allow custom modules to provide date fields.
      // The is_a() function makes this work for any handler
      // that was derived from 'views_handler_field_date'.
      // Unfortunately is_a() is deprecated in PHP 5.2, so we need
      // a more convoluted test.
      elseif ( (version_compare(PHP_VERSION, '5.2', '<') && is_a($handler, 'views_handler_field_date')) || ($handler instanceof views_handler_field_date) ){
        foreach( module_implements('date_api_fields') as $module ){
          $function = $module . '_date_api_fields';
          if ( $custom = $function("$table_name.$field_name") ){
            $type = 'custom';
            break;
          }
        }
      }

      // Don't do anything if this is not a date field we can handle.
      if ( !empty($type) ){

        // Handling for simple timestamp fields
        $fromto = array( $alias, $alias );
        $tz_handling = 'site';
        $related_fields = array( );
        $timezone_field = '';
        $offset_field = '';
        $rrule_field = '';
        $delta_field = '';
        $granularity = array( 'year', 'month', 'day', 'hour', 'minute' );

        // Handling for content field dates
        if ( isset($handler->content_field['tz_handling']) ){
          $tz_handling = $handler->content_field['tz_handling'];
          $db_info = content_database_info($handler->content_field);
          if ( $tz_handling == 'date' ){
            $offset_field = $table_name . '.' . $db_info['columns']['offset']['column'];
          }
          $related_fields = array(
            $table_name . '.' . $field_name
          );
          if ( isset($db_info['columns']['value2']['column']) ){
            $related_fields = array_merge($related_fields, array( $table_name . '.' . $db_info['columns']['value2']['column'] ));
          }
          if ( isset($db_info['columns']['timezone']['column']) ){
            $related_fields = array_merge($related_fields, array( $table_name . '.' . $db_info['columns']['timezone']['column'] ));
            $timezone_field = $table_name . '.' . $db_info['columns']['timezone']['column'];
          }
          if ( isset($db_info['columns']['rrule']['column']) ){
            $related_fields = array_merge($related_fields, array( $table_name . '.' . $db_info['columns']['rrule']['column'] ));
            $rrule_field = $table_name . '.' . $db_info['columns']['rrule']['column'];
          }
        }
        // Get the delta value into the query.
        if ( !empty($handler->content_field['multiple']) ){
          array_push($related_fields, "$table_name.delta");
          $delta_field = $table_name . '_delta';
        }

        // Handling for cck fromto dates
        if ( isset($handler->content_field) ){
          switch( $handler->content_field['type'] ){
            case 'date':
            case 'datetime':
            case 'datestamp':
              $db_info = content_database_info($handler->content_field);
              $fromto = array(
                $table_name . '_' . $db_info['columns']['value']['column'],
                $table_name . '_' . (!empty($handler->content_field['todate'])
                  ? $db_info['columns']['value2']['column']
                  : $db_info['columns']['value']['column']),
              );
              break;
          }
          $granularity = !empty($handler->content_field['granularity'])
            ? $handler->content_field['granularity']
            : array( 'year', 'month', 'day', 'hour', 'minute' );
        }

        // CCK fields append a column name to the field, others do not
        // need a real field_name with no column name appended for cck date formatters
        switch( $type ){
          case 'cck_string':
            $sql_type = DATE_ISO;
            break;
          case 'cck_datetime':
            $sql_type = DATE_DATETIME;
            break;
          default:
            $sql_type = DATE_UNIX;
            break;
        }
        $fields['name'][$name] = array(
          'type' => $type,
          'sql_type' => $sql_type,
          'label' => $val['group'] . ': ' . $val['title'],
          'granularity' => $granularity,
          'fullname' => $name,
          'table_name' => $table_name,
          'field_name' => $field_name,
          'query_name' => $alias,
          'fromto' => $fromto,
          'tz_handling' => $tz_handling,
          'offset_field' => $offset_field,
          'timezone_field' => $timezone_field,
          'rrule_field' => $rrule_field,
          'related_fields' => $related_fields,
          'delta_field' => $delta_field,
        );

        // Allow the custom fields to over-write values.
        if ( !empty($custom) ){
          foreach( $custom as $key => $val ){
            $fields['name'][$name][$key] = $val;
          }
        }
        if ( isset($handler->content_field) ){
          if ( substr($field_name, -1) == '2' ){
            $len = (strlen($field_name) - 7);
          }else{
            $len = (strlen($field_name) - 6);
          }
          $fields['name'][$name]['real_field_name'] = substr($field_name, 0, $len);
        }else{
          $fields['name'][$name]['real_field_name'] = $field_name;
        }
        $fields['alias'][$alias] = $fields['name'][$name];
      }
    }
    cache_set($cid, $fields, 'cache_views');
    return $fields;
  }

  public function extra_options_form(&$form, &$form_state) {
    $options = array( );
    $fields = $this->get_date_fields();
    foreach( $fields['name'] as $name => $field ){
      $options[$name] = $field['label'];
    }
    if ( empty($this->options['date_fields']) && $this->field != 'date_filter' ){
      $this->options['date_fields'] = array( $this->table . '.' . $this->field );
    }
    $form['date_fields'] = array(
      '#title' => t('Date field(s)'),
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->options['date_fields'],
      '#multiple' => FALSE,
      '#description' => t('Select date field(s) to filter with this argument.'),
    );
  }

  public function extra_options_validate($form, &$form_state) {
    $check_fields = array_filter($form_state['values']['options']['date_fields']);
    if ( empty($check_fields) ){
      form_error($form['date_fields'], t('You must select at least one date field for this filter.'));
    }
  }

  function extra_options_submit($form, &$form_state) {
    $form_state['values']['options']['date_fields'] = array_filter($form_state['values']['options']['date_fields']);
  }

  public function values() {
    return array(
      'current' => t('Current'),
      'week' => t('Past week'),
      'month' => t('Past month'),
      'month3' => t('Past three months'),
      'month6' => t('Past six months'),
      'year' => t('Past year'),
    );
  }

  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    $values = $this->values();
    if (!empty($this->options['expose']['optional'])) {
      array_unshift($values, '< Any >');
    }
    $form['value']['#tree'] = true;
    $form['value']['value'] = array(
      '#type' => 'select',
      '#options' => $values,
    );
  }

  public function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    $options = $this->values();
    if ( empty( $this->value) || $this->value == 'All' ){
      return t('exposed');
    }
    return $options[$this->value];
  }

  public function exposed_validate(&$form, &$form_state) {
    if ( empty($this->options['exposed']) ){
      return;
    }
    $options = $this->values();
    if (!empty($this->options['expose']['optional'])) {
      array_unshift($options, '< Any >');
    }    
    if ( !is_array($options)
      || !in_array($form_state['values']['value'], array_keys($options))
    ){
      form_error($form[$this->field]['value'], 'Illegal choice selected');
    }
  }

  public function exposed_info() {
    if ( empty($this->options['exposed']) ){
      return;
    }
    return array(
      'operator' => $this->options['operator'],
      'value' => $this->options['expose']['identifier'],
      'label' => $this->options['expose']['label'],
    );
  }

  public function query() {
    if ( !empty($this->options['exposed']) && !empty($this->view->exposed_input[$this->options['expose']['identifier']]) ){
      $selected = drupal_strtolower($this->view->exposed_input[$this->options['expose']['identifier']]['value']);
    }else{
      $selected = drupal_strtolower($this->options['value']['value']);
    }    
    list( $min_field, $max_field ) = $this->get_query_fields();
    if ( !$min_field || !$max_field ){
      return;
    }
    $query = &$this->query;
    $query instanceof views_query;
    $dt = new DateTime();
    
    switch ($selected) {
      case 'current':
        $min_bound = strtotime('now');
        break;
      case 'week':
        $dt->modify('1 week ago');
        break;
      case 'month':
        $dt->modify('1 month ago');
        break;
      case 'month3':
        $dt->modify('3 months ago');
        break;
      case 'month6':
        $dt->modify('6 months ago');
        break;
      case 'year':
        $dt->modify('-1 year');
        break;
      default:
        //no changes in query
        return;
    }

    $min_bound = $dt->format('U');
    $max_bound = time();
    
    $query->ensure_table($min_field['field']['table_name']);
    $query->ensure_table($max_field['field']['table_name']);
    $group = $query->set_where_group('OR');
    $query->add_where(
      $group, 
      'UNIX_TIMESTAMP(STR_TO_DATE(' . $min_field['field']['field_name'] . ', "%Y-%m-%dT%T")) < ' . $min_bound
    );
    $query->add_where(
      $group, 
      'UNIX_TIMESTAMP(STR_TO_DATE(' . $max_field['field']['field_name'] . ', "%Y-%m-%dT%T")) > ' . $max_bound
    );
  }

  function get_query_fields() {
    $fields = $this->get_date_fields();
    $fields = $fields['name'];
    $this->query_fields = array( );
    foreach( (array) $this->options['date_fields'] as $delta => $name ){
      if ( array_key_exists($name, $fields) && $field = $fields[$name] ){
        $date_handler = new date_sql_handler();
        $date_handler->construct($field['sql_type'], date_default_timezone_name());
        $date_handler->granularity = $this->options['granularity'];
        date_views_set_timezone($date_handler, $this->view, $field);
        $this->query_fields[] = array( 'field' => $field, 'date_handler' => $date_handler );
      }
    }
    return $this->query_fields;
  }
}