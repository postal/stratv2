<?php

/**
 * Adds TRUE to query if current user have selected permissions
 *
 * @author Юрий
 */
class views_improvements_handler_filter_curent_user_permissions extends views_handler_filter_in_operator {

  public $value_form_type = 'select';

  function get_value_options() {
    $opt = module_invoke_all('perm');
    $this->value_options = array();
    foreach ($opt as $opt) {
      $this->value_options[$opt] = $opt;
    }
  }

  function operators() {
    $operators = array(
      'in' => array(
        'title' => t('Has permission'),
        'short' => t('has'),
        'short_single' => t('='),
        'method' => 'op_like',
        'values' => 1,
      ),
      'not in' => array(
        'title' => t('Does not have permission'),
        'short' => t('lacks'),
        'short_single' => t('<>'),
        'method' => 'op_like',
        'values' => 1,
      ),
    );
    return $operators;
  }

  public function op_like() {
    if (empty($this->value)) {
      return;
    }
    $result = false;
    foreach ($this->value as $permission) {
      if (user_access($permission)) {
        $result = true;
        break;
      }
    }
    if ($this->operator == 'not in') {
      $result = !$result;
    }

    $this->query->add_where($this->options['group'], $result ? 'TRUE' : 'FALSE');
  }

}
