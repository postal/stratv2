<?php
/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */


function views_slideshow_flexible_jcarousel_views_slideshow_modes(){
  $options = array(
    'views_slideshow_flexible_jcarousel' => t('Flexible JCarousel'),
  );
  return $options;
}

function views_slideshow_flexible_jcarousel_views_slideshow_option_definition(){
  $options['views_slideshow_flexible_jcarousel'] = array(
    'contains' => array (
      'main_fields' => array('default' => array()),
    )
  );
  return $options;
}

function views_slideshow_flexible_jcarousel_views_slideshow_options_form(&$form, &$form_state, &$view){
  if ($view->row_plugin->uses_fields()) {
    $options = array();
    foreach ($view->display->handler->get_handlers('field') as $field => $handler) {
      $options[$field] = $handler->ui_name();
    }
    $form['views_slideshow_flexible_jcarousel']['main_fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Main frame fields'),
      '#options' => $options,
      '#default_value' => $view->options['views_slideshow_flexible_jcarousel']['main_fields'],
      '#description' => t("Choose the fields that will appear in the main slide."),
    );
  }
}
