/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
if ( Drupal.jsEnabled ){
  Drupal.behaviors.flexibleJCarousel = function() {
    $.fn.startCarousel = function() {
      var bodywidth = $('.mycont').parent().width() - 80,
      itemwidth = $('.mycarousel li').outerWidth(true),
      mycontwidth = bodywidth > itemwidth ? bodywidth - bodywidth%itemwidth : itemwidth,
      licount = $('.mycarousel li').size(),
      jscroll = 1;

      if(licount > mycontwidth/itemwidth){
        jscroll =  mycontwidth/itemwidth;
      } else {
        jscroll = 0;
        mycontwidth = licount * itemwidth;
      }

      $('.mycont').width(mycontwidth+80);

      options = { scroll: jscroll }
      if ( jscroll == 0 ) {
        options.buttonNextHTML = null;
        options.buttonPrevHTML = null;
      }

      $('.mycarousel').jcarousel(options);
    };

    $(this).startCarousel();

    $(window).resize(function(){
      $(this).startCarousel();
    });

  };
}
