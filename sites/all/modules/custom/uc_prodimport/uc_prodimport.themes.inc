<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

function theme_export_form_table($form) {
  $select_all = array( '#type' => 'checkbox', '#name' => 'select_all' );
  $header = array(
    drupal_render($select_all),
    '', //Image
    t('Product'),
    t('Actions'),
  );
  $rows = array( );
  $items = element_children($form['rows']);
  foreach( $items as $id ){
    $item = &$form['rows'][$id];
    $image = $item['#image'];
    $title = $item['#title'];
    unset($item['#title']);
    unset($item['#image']);
    $rows[] = array(
      drupal_render($item),
      $image,
      $title,
      l(t('Edit'), 'node/' . $id . '/edit'),
    );
  }
  return theme('table', $header, $rows) . theme('pager') . drupal_render($form) ;
}

function template_preprocess_review_list(&$vars) {
  $values = _get_selected_products(false, 'products_to_review');
  $vars['pager'] = theme_pager();
  $i = 0;
  foreach( $vars['items'] as $item ){
    $item['id'] = ++$i;
    $item['checked'] = in_array($item['node']->nid, $values) ? 'checked' : '';
    $vars['review_items'] .= theme('review_list_item', $item);
  }
  $vars['form_bottom'] = drupal_get_form('uc_prodimport_review_form');
}

function template_preprocess_review_list_item(&$vars) {
  $node = node_load($vars['item']['node']->nid);
  $vars['nid'] = $node->nid;
  $vars['url'] = '/' . $node->path;
  $vars['edit_url'] = url('node/' . $vars['item']['nid'] . '/edit');
  $vars['publish_url'] = url('node/' . $vars['item']['nid'] . '/publish');
  $vars['sell_price'] = uc_currency_format($node->sell_price, $sign = TRUE, $thou = TRUE, $dec = NULL);
  $vars['title'] = $vars['item']['title'];
  $vars['image'] = theme('imagecache', 'product_list', $node->field_image_cache[0]['filepath']);
  $vars['checked'] = $vars['item']['checked'];
  if ( intval($vars['id']) % 3 == 1 ){
    $vars['new_line'] = true;
  }
}