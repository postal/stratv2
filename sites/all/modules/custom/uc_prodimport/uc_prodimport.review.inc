<?php

/*
 *  License text is not available
 */
function uc_prodimport_review() {
  global $user;
  //user filtering
  $filters = isset($_GET['filters'])?$_GET['filters']:'';
  //need to filter by status and by is_temporary and by uid
  //only for current user
  $filters .= ' sis_imported_by:' . $user->uid;
  //show only products
  $filters .= ' type:product';
  $filters .= arg(5) == 'new'
  ? ' sis_review_status:' . REVIEW_STATUS_NEW
  : ' sis_review_status:' . REVIEW_STATUS_REVIEW;

//  $solrsort = isset($_GET['solrsort'])
//    ? $_GET['solrsort']
//    : '';
  $page = isset($_GET['page'])
    ? $_GET['page']
    : 0;

  try {
    $results = apachesolr_search_execute('', $filters, $solrsort, $_GET['q'], $page);
    //breadcrumbs
    drupal_set_breadcrumb(array(
      l(t('Product Import'), 'admin/store/products/uc_prodimport/import'),
      l(t('Product Review'), $_GET['q']),
    ));
    if ( $results ){
      return theme('review_list', $results);
    }else{
      return theme('box', t('There is no products in this category'), '');
    }
  }catch(exception $e ){
    /* @var $view view */
    $view = views_get_view('uc_prodimport_products_list');
    if ( empty($view) ){
      die('View not installed');
      return;
    }
    $output = '';
    if ( arg(5) == 'new' ){//
      $output .= $view->render('new_products');
    }else{
      $output .= $view->render('review_products');
    }
  }
  $output .= drupal_get_form('uc_prodimport_review_form');
  return $output;
}

function uc_prodimport_review_form($form_state) {
  $form = array( );
  $form['#tree'] = TRUE;

  $form['back'] = array(
    '#type' => 'button',
    '#value' => t('Go Back'),
    '#url' => 'admin/store/products/uc_prodimport/export',
  );
  $form['save'] = array(
    '#type' => 'button',
    '#value' => t('Save Unpublished Products for Later Review'),
    '#executes_submit_callback' => true,
    '#submit' => array( 'uc_prodimport_review_form_submit_publish_all' ),
  );
  $form['publish_selected'] = array(
    '#type' => 'button',
    '#value' => t('Publish Selected'),
    '#submit' => array('uc_prodimport_publish_selected'),
  );
  $form['delete_selected'] = array(
    '#type' => 'button',
    '#value' => t('Delete Selected'),
    '#url' => 'admin/store/products/uc_prodimport/mass_delete',
  );
  return $form;
}

function uc_prodimport_review_form_validate($form, &$form_state) {
  if ( $form_state['clicked_button']['#type'] == 'button' && !empty($form_state['clicked_button']['#url']) ){
    drupal_goto($form_state['clicked_button']['#url']);
  }
}

function uc_prodimport_review_form_submit_publish_all($form, &$form_state) {
  global $user;

  $query = '
    UPDATE {uc_imported_products} ip, {node} n
    SET
      ip.review_status = 0, n.status = 1
    WHERE
      n.nid = ip.nid
    AND
      n.uid = %d
      ';
  $query = db_query($query, $user->uid);
}

function uc_prodimport_publish_selected($form, &$form_state) {
  $ids = _get_selected_products(false, 'products_to_review');
  foreach( $ids as $id ){
    $node = node_load($id);
    $node->review_status = REVIEW_STATUS_PUBLISHED;
    node_publish_action($node);
    node_save($node);
  }
}

function uc_prodimport_review_form_submit($form, &$form_state) {

}

function uc_prodimport_publish_node($form, &$form_state) {
  $node = node_load($form['nid']['#value']);
  $node->status = 1;
  $node->review_status = REVIEW_STATUS_PUBLISHED;
  node_save($node);
}

function uc_prodimport_edit_later($form, &$form_state) {
  $node = node_load($form['nid']['#value']);
  $node->review_status = REVIEW_STATUS_REVIEW;
  node_save($node);
  drupal_redirect_form($form, 'admin/store/products/uc_prodimport/review/new');
}
