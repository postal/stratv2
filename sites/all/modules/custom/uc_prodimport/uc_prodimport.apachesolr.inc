<?php

/*
 *  License text is not available
 */

/**
 * Implementation of hook_apachesolr_update_index().
 * @param $document
 * @param stdClass $node
 */
function uc_prodimport_apachesolr_update_index(&$document, $node) {
  $document->addField('sis_review_status', $node->review_status);
  if ( !empty($node->imported_by) ){
    $document->addField('sis_imported_by', $node->imported_by->uid);
  }
}
/**
 * Implementation of hook_apachesolr_facets().
 * @return array
 */
function uc_prodimport_apachesolr_facets() {
  $facets = array();
  $facets['sis_review_status'] = array(
    'info' => t('Product Import: filter by review status'),
    'facet_field' => 'sis_review_status',
  );
  return $facets;
}

/**
 *
 * @param Solr_Base_Query $query
 * @param array $params
 * @param string $caller 
 */
function uc_prodimport_apachesolr_modify_query(&$query, &$params, $caller){
  
  $params['fl'] .= ',sis_review_status,sis_imported_by';
  $review_filter = $query->get_filters('sis_review_status');
  if ( empty($review_filter) ){
    $query->add_filter('sis_review_status', 0);
  }
}