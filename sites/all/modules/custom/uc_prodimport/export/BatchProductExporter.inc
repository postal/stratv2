<?php

require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'productExporter.inc';

//the function_exists function doesn't understand arrays as function names
function opFinished($success, $results, $operations){

  if ( $success ){
    $orig = $results;
    file_copy($results);
    unlink($orig);
    $url = 'admin/store/products/uc_prodimport/download/' . basename($results);
    $message = t('Data exported. You can download file from this link: ' . l('download', $url));
  }else{
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = 'An error occurred while processing ' . $error_operation[0] 
      . ' with arguments: ' . print_r($error_operation[0], TRUE);
  }
  drupal_set_message($message);
}

function product_export_batch($list, &$context){
  if ( isset($context['sandbox']['class']) ){
    $batch = unserialize($context['sandbox']['class']);
  }else{
    $batch = new BatchProductExporter();
  }
  $batch->batch($list, $context);
  $context['sandbox']['class'] = serialize($batch);
}

/**
 * Description of BatchProductExporter
 *
 * @author Юрий
 */
class BatchProductExporter extends ProductExporter implements Serializable {

  public function __destruct(){
    //nothing to do
  }

  public function serialize(){
    return serialize(array(
            'dirToArchive' => $this->dirToArchive,
        ));
  }

  public function unserialize($serialized){
    $this->init(unserialize($serialized));
  }

  public function batch($nodes, &$context){
    if ( $context['sandbox']['op'] == 'prepare' || !isset($context['sandbox']['op']) ){
      //prepare
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($nodes) + 1;
      $context['sandbox']['result'] = array( );
      $context['message'] = t('Preparing list of %cnt items',
          array( '%cnt' => count($nodes) ));
      parent::prepare($nodes);
      $context['sandbox']['list'] = $nodes;
      $context['finished'] = 0;
      $context['sandbox']['op'] = 'convert';
      //Insert titles
      $t = new NodeTitleExporter();
      $context['sandbox']['result'][] = $t->process(null);
      return;
    }elseif ( $context['sandbox']['op'] == 'convert' ){
      //convert
      $node = array_shift($context['sandbox']['list']);
      $node = parent::convertNodeToArray($node);
      if ( $node ){
        $context['sandbox']['result'][] = $node;
        $context['message'] = t('Exporting node %name', array( '%name' => $node[0] )); //!!! fixed pointer to result
      }
      $context['sandbox']['progress']++;

      if ( $context['sandbox']['progress'] != $context['sandbox']['max'] && $context['sandbox']['max'] > 0 ){
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }
      if ( empty($context['sandbox']['list']) ){
        $context['sandbox']['op'] = 'csv';
      }
    }elseif ( $context['sandbox']['op'] == 'csv' ){
      parent::saveCSV($context['sandbox']['result']);
      $context['message'] = t('Writing CSV file');
      $context['finished'] = 0.95;
      $context['sandbox']['op'] = 'archive';
      return;
    }elseif ( $context['sandbox']['op'] == 'archive' ){
      $context['results'] = parent::archive();
      $context['message'] = t('Archiving data');
    }
  }

  public function export($nodes_list){
    $operations = array(
        array( 'product_export_batch', array( $nodes_list ) ),
    );

    $batch = array(
        'operations' => $operations,
        'finished' => 'opFinished',
        // We can define custom messages instead of the default ones.
        'title' => t('Export products'),
        'init_message' => t('Initializing.'),
        'progress_message' => '',
        'error_message' => t('Export data has encountered an error.'),
    );
    batch_set($batch);
  }

}