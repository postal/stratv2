<?php

require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'classes.inc';
require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'NodeConverter.inc';
require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'converters.inc';
require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'FieldExportData.inc';

/**
 * Description of productExporter
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
class ProductExporter extends Loadable {

  protected $dirToArchive;

  public function __destruct() {
    fileUtils::delTree($this->dirToArchive);
  }

  public static function allowZip() {
    return class_exists('ZipArchive');
  }

  protected function prepare(&$nodes) {
    $this->dirToArchive = file_directory_temp() . DIRECTORY_SEPARATOR . uniqid(time(),
        true);
    mkdir($this->dirToArchive, 0750, true);
  }

  public function export($nodes_list) {
    $time_limit = ini_get('max_execution_time');
    set_time_limit(0);

    $this->prepare($nodes_list);
    $list = $this->convertNodesToArray($nodes_list);
    $this->saveCSV($list);
    $archive = $this->archive();
    set_time_limit($time_limit);
    //this is must not be inside class
    $orig = $archive;
    file_copy($archive);
    unlink($orig);
    $url = 'admin/store/products/uc_prodimport/download/' . basename($archive);
    $message = t('Data exported. You can download file from this link: ' . l('download', $url));
    drupal_set_message($message);
  }

  /**
   * Convert one node to array
   * @param stdClass|int $node node object or its id
   * @return boolean
   */
  protected function convertNodeToArray($node) {
    if ( !$node instanceof stdClass ){
      $node = node_load($node);
    }
    if ( $node->type != 'product' ){
      return false;
    }
    //before convert
    $node->stock = db_fetch_object(db_query('SELECT * FROM {uc_product_stock} WHERE sku = "%s"',
        $node->model));
    $related = db_result(db_query('SELECT related_products FROM {uc_upsell_products} WHERE nid = %d',
        $node->nid));
    if ( $related ){
      $related = unserialize($related);
      if ( !empty($related) ){
        $result = db_query('SELECT model FROM {uc_products} WHERE nid IN(' . db_placeholders($related) . ')',
          $related);
        while( $item = db_fetch_array($result) ){
          $node->related[] = $item['model'];
        }
      }
    }
    //grants
    if ( module_exists('nodeaccess') ){
      $query = '
        SELECT 
          n.*,
          IFNULL(a.name, r.name) as name
        FROM 
          {node_access} n
        LEFT JOIN
          {role} r ON ( n.gid = r.rid )
        LEFT JOIN
          {nodeaccess_role_alias} a
        ON ( r.rid = a.rid )
        WHERE 
          n.nid = %d
        AND 
          realm IN ("nodeaccess_rid", "nodeaccess_uid") 
      ';
      $result = db_query($query, $node->nid);
      while( $item = db_fetch_object($result) ){
        if ( $item->grant_view ){
          $node->grants_read[] = $item->name;
        }
        if ( $item->grant_update ){
          $node->grants_write[] = $item->name;
        }
      }
    }else{
      $node->grants_read = array( );
      $node->grants_write = array( );
    }
    if ( module_exists('uc_price_visibility') ){
      $query = 'SELECT * FROM {role}';
      $result = db_query($query);
      $roles = array();
      while ( FALSE !== $item = db_fetch_object($result) ){
        $roles[$item->rid] = $item->name;
      }
      
      $query = 'SELECT roles FROM {uc_price_visibility} WHERE nid = %d';
      $data = db_result(db_query($query, $node->nid));
      $data = explode(',', $data);
      foreach ( $data as &$item){
        $item = $roles[$item];
      }
      $node->grants_read_price = $data;
    }else{
      $node->grants_read_price = array();
    }
    //
    $params = array(
      'variables' => array(
        'arcdir' => $this->dirToArchive,
      )
    );
    $parser = new NodeExporter($params);
    $node = $parser->process($node);
    return $node;
  }

  protected function convertNodesToArray($nodes_list) {
    foreach( $nodes_list as &$node ){
      $node = $this->convertNodeToArray($node);
    }
    $nodes_list = array_filter($nodes_list);
    //Insert titles
    $t = new NodeTitleExporter();
    array_unshift($nodes_list, $t->process(null));

    return $nodes_list;
  }

  protected function saveCSV($data) {
    $filename = $this->dirToArchive . DIRECTORY_SEPARATOR . 'export.csv';
    $file = fopen($filename, 'w+');
    foreach( $data as $node_data ){
      fputcsv($file, $node_data);
    }
    fclose($file);
    return $filename;
  }

  /**
   * Archive data
   * @return string
   */
  protected function archive() {
    if ( !is_dir($this->dirToArchive) ){
      return false;
    }

    if ( !self::allowZip() ){
      return false;
    }

    $zip = new ZipArchive();
    $arcname = file_directory_temp() . DIRECTORY_SEPARATOR . uniqid(time(), true) . '.zip';
    $zip->open($arcname, ZipArchive::CREATE);

    $dir = $this->dirToArchive;

    $level = 0;
    $files = scandir($dir);
    $stack = array( );
    $path_names = array( );

    while( $level >= 0 ){
      $file = array_shift($files);

      if ( $file == '.' || $file == '..' ){
        continue;
      }

      if ( !$file ){
        $level--;
        $files = array_pop($stack);
        array_pop($path_names);
        continue;
      }
      if ( !empty($path_names) ){
        $relative_path = implode(DIRECTORY_SEPARATOR, $path_names) . DIRECTORY_SEPARATOR . $file;
      }else{
        $relative_path = $file;
      }
      $absolute_path = $dir . DIRECTORY_SEPARATOR . $relative_path;

      if ( is_dir($absolute_path) ){
        $zip->addEmptyDir($relative_path);
        $level++;
        $path_names[] = $file;
        $stack[] = $files;
        $files = scandir($absolute_path);
      }else{
        $zip->addFile($absolute_path, $relative_path);
      }
    }
    $zip->close();
    return $arcname;
  }

}
