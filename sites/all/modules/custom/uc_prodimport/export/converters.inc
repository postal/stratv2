<?php

/**
 * Converters to export data
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'classes.inc';

class exportNumber extends convertBase {

}

class exportText extends convertBase {

}

class exportCCKHTML extends nodeConvertMethod {

  public function convert($data) {
    $result = '';
    if ( is_array($data) ){
      foreach( $data as $item ){
        $result .= $item['value'];
      }
      return $result;
    }
    return null;
  }

}

class exportCCKArray extends nodeConvertMethod {

  protected function getItem($item) {
    return $item;
  }

  public function convert($data) {
    if ( is_array($data) ){
      foreach( $data as &$item ){
        $item = $this->getItem($item['value']);
      }
      return implode(';', $data);
    }
    return null;
  }

}

class exportCCKTaxonomy extends exportCCKArray {

  public function convert($data) {
    if ( !is_array($data) ){
      return null;
    }
    $data = array_map(create_function('$item', 'return $item["value"];'), $data);
    $data = array_filter($data);
    if ( empty($data) ){
      return null;
    }

    $result = db_query('SELECT tid, name FROM {term_data} WHERE tid IN(' . db_placeholders($data) . ')', $data);
    $items = array( );
    while( $item = db_fetch_array($result) ){
      $items[] = $item['name'];
    }
    return implode(';', $items);
  }

}

class exportCCKFile extends exportCCKArray {

  protected $destination;
  protected $exportdir;

  /**
   * @param array $data
   */
  public function convert($data) {
    $dirname = isset($this->exportdir)
      ? $this->exportdir
      : $this->fieldData->field;

    $dest = $this->destination . DIRECTORY_SEPARATOR . $dirname . DIRECTORY_SEPARATOR . $this->fieldData->node->nid;

    if ( !is_dir($dest) ){
      if ( !mkdir($dest, 0750, true) ){
        watchdog('Exporter', 'Unable to create dir %dir', array( '%dir' => $dest ), WATCHDOG_ERROR);
      }
    }

    foreach( $data as $file ){
      if ( empty($file) || !file_exists(realpath($file['filepath']))){
        continue;
      }

      copy(realpath($file['filepath']), $dest . DIRECTORY_SEPARATOR . basename($file['filepath']));
      $result[] = $dirname . DIRECTORY_SEPARATOR . $this->fieldData->node->nid . DIRECTORY_SEPARATOR . basename($file['filepath']);
    }
    return $result;
  }

}

class exportListValue extends nodeConvertMethod {

  /**
   * Array to search keys in
   * @var array
   */
  public $list = array( );

  public function convert($data) {
    if ( !is_array($this->list) ){
      throw new exception('Parameter is not array.');
    }
    return $this->list[$data];
  }

}

/**
 * Exporter for Country value
 */
class exportCountry extends exportListValue {

  private static function getList() {
    static $result;
    if ( !isset($result) ){
      $result = array( );
      $query = db_query("SELECT * FROM {uc_countries}");
      while( false != $item = db_fetch_array($query) ){
        $result[$item['country_id']] = $item['country_name'];
      }
    }
    return $result;
  }

  public function __construct($params = array( )) {
    $this->list = self::getList();
    parent::__construct($params);
  }

}

/**
 * Exporter for State value
 */
class exportState extends exportListValue {

  private static function getList() {
    static $result;
    if ( !isset($result) ){
      $result = array( );
      $query = db_query("SELECT * FROM {uc_zones}");
      while( false != $item = db_fetch_array($query) ){
        $result[$item['zone_id']] = $item['zone_name'];
      }
    }
    return $result;
  }

  public function __construct($params = array( )) {
    $this->list = self::getList();
    //allow to overwrite list
    parent::__construct($params);
  }

}

class exportCCKDate extends nodeConvertMethod {

  public function convert($data) {
    return $data;
  }

}

class exportFiles extends nodeConvertMethod {

  /**
   *
   * @var string
   */
  protected $destination;

  public function convert($data) {
    $dest = $this->destination . DIRECTORY_SEPARATOR . $this->fieldData->field . DIRECTORY_SEPARATOR . $this->fieldData->node->nid;
    if ( !is_dir($dest) ){
      if ( !mkdir($dest, 0750, true) ){
        watchdog('Exporter', 'Unable to create dir %dir', array( '%dir' => $dest ), WATCHDOG_ERROR);
      }
    }
    foreach( $data as $file ){
      if ( empty($file) || !file_exists(realpath($file->filepath))){
        continue;
      }
      copy(realpath($file->filepath), $dest . DIRECTORY_SEPARATOR . basename($file->filepath));
      $result[] = $this->fieldData->field . DIRECTORY_SEPARATOR . $this->fieldData->node->nid . DIRECTORY_SEPARATOR . basename($file->filepath);
    }
    return $result;
  }

}

class exportRelated extends nodeConvertMethod {

  public function convert($data) {
    if ( is_array($data) ){
      return implode(';', $data);
    }
    return null;
  }

}

class exportCCKText extends exportCCKArray {

}

class exportCCKNumber extends exportCCKArray {

}

class exportGrants extends nodeConvertMethod {

  public function convert($data) {
    if ( is_array($data) ){
      return implode(';', $data);
    }
    return null;
  }

}

class exportWeightUnits extends exportText {

}

class exportSizeUnits extends exportText {

}

/**
 * Exporter list value based on database info
 */
class exportSqlListValue extends exportListValue {

  /**
   * @var string field with the keys for resulting array
   */
  protected $key;
  /**
   * @var string field with the values for resulting array
   */
  protected $value;
  /**
   * @var array additional filters. Represents as "key = value" concatenated
   * with AND operator.
   */
  protected $filter = array();
  /**
   * @var string Table to retreive data.
   */
  protected $table;
  /**
   * @var string SQL-query. Can be used only for complex queries instead
   * filter and table fields. $key and $value still needed
   */
  protected $query;

  /**
   * Init parameters
   * @param array $params
   */
  protected function init($params) {
    parent::init($params);
    if ( empty($this->list) ){
      $this->list = $this->getList();
    }
  }

  /**
   * Fill array from database
   * @return array
   */
  private function getList() {
    if ( !empty($this->query) ){
      $query = db_query($this->query);
    }else{
      $query = 'SELECT %s, %s FROM {%s} ';
      foreach( $this->filter as $key => $value ){
        $where[] = $key . ' = ' . $value;
      }
      if ( !empty($where) ){
        $query .= 'WHERE ' . implode(' AND ', $where);
      }
      $query = db_query($query, array( $this->key, $this->value, $this->table ));
    }
    $result = array();
    while( false !== $item = db_fetch_array($query) ){
      $result[$item[$this->key]] = $item[$this->value];
    }
    return $result;
  }

}

/**
 * Users exporter
 */
class exportUser extends exportSqlListValue {
  /**
   * Init parameters
   * @param array $params
   */
  protected function init($params) {
    $params += array(
      'key'   => 'uid',
      'value' => 'name',
      'table' => 'users',
    );
    parent::init($params);
  }
}