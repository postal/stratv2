<?php
require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'classes.inc';

/**
 * Description of FieldExportData
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
class FieldExportData extends FieldData {

  public $caller;
  public $node;

  public function __construct(NodeExporter $caller, $params = array( )) {
    $this->caller = $caller;
    $params['classprefix'] = 'export';
    parent::__construct($params);
  }

  private function getFieldData($object, $field) {
    try{
      $item = &$object;
      $i = 0;

      $fieldset = explode('.', $field);

      foreach( $fieldset as $field ){
        $i++;
        if ( $field == '%' ){
          if ( !is_array($item) ){
            continue; //throw exception ...
          }
          $field = implode('.', array_slice($fieldset, $i));
          foreach( $item as $key => $subitem ){
            $result[$key] = $this->getFieldData($subitem, $field);
          }
          //we filled up the tree. We do not need to continue
          return $result;
        }
        if ( is_array($item) ){
          $item = &$item[$field];
        }else{
          $item = &$item->$field;
        }
      }
      return $item;
    }catch( exception $e ) {
      return null;
    }
  }

  public function execute(&$node) {
    $result = '';
    $this->node = $node;
    if ( $this->convertMethod instanceof nodeConvertMethod ){
      if ( $this->field ){
        $result = $this->convertMethod->convert($this->getFieldData($node, $this->field));
        if ( is_array($result) ){
          $result = implode(';', $result);
        }
      }
    }
    return $result;
  }

}

