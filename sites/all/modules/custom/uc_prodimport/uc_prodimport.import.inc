<?php

/*
 *  License text is not available
 */
function uc_prodimport_import_form() {

  $form = array(); // reset array
  $form['#redirect'] = array( 'admin/store/products/uc_prodimport/review/new' );
  $form['#attributes'] = array( 'enctype' => "multipart/form-data" );
  $form['go_to_review'] = array(
    '#type'  => 'button',
    '#value' => t('Advance to Product Review bypass Import'),
    '#url'   => 'admin/store/products/uc_prodimport/review',
  );
  $form['datafile'] = array(
    '#type'        => 'file',
    '#title'       => t('Upload data'),
    '#size'        => 48,
    '#description' => productImporter::allowZip()
      ? t('A CSV in the requisite format or ZIP with all requested data')
      : t('A CSV in the requisite format'),
  );
  if ( user_access('administer sellers') ) {
    $form['uid'] = array(
      '#type'              => 'textfield',
      '#title'             => t('Seller for new products'),
      '#description'       => t('Leave empty to make it yours'),
      '#autocomplete_path' => 'user/autocomplete',
    );
  }

  /**
   * Если может импортировать всё, значит есть право создавать
   * Если может обновлять, значит
   * варианты:
   * * только обновить продукты
   * * добавить все
   * * добавить новые и обновить
   */
  $options = array();
  if ( user_access('import products') ) {
    $options['add'] = t('Add all products as new');
  }
  if ( user_access_multiple('update own products', 'update any products') ) {
    $options['upd'] = t('Update exising products if possible');
  }
  if ( count($options) > 1 ) {
    $options['both'] = t('Update products if possible and add as new if not');
  }

  if ( count($options) > 1 ) {
    $form['import_method'] = array(
      '#type'    => 'select',
      '#title'   => t('Import behavior'),
      '#options' => $options
    );
  } elseif ( count($options) == 1 ) {
    $form['import_method'] = array(
      '#type'  => 'hidden',
      '#value' => key($options),
    );
  }

  $form['back'] = array(
    '#type'  => 'button',
    '#value' => 'Go Back',
    '#url'   => 'admin/store/products/uc_prodimport/export',
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Advance to Product Review Following Import'),
    '#id'    => 'upload-products',
  );

  $form['cancel'] = array(
    '#type'  => 'button',
    '#value' => t('Cancel'),
    '#url'   => 'admin/store/products/uc_prodimport/cancel',
  );

  return $form;
}

function uc_prodimport_import_form_validate(&$form, &$form_state) {
  if ( $form_state['clicked_button']['#type'] == 'button'
    && !empty($form_state['clicked_button']['#url'])
  ) {
    drupal_goto($form_state['clicked_button']['#url']);
  }
  $form_state['values']['uid'] = trim($form_state['values']['uid']);
  if ( $form_state['values']['uid']
    && !user_load(array( 'name' => $form_state['values']['uid'] ))
  ) {
    form_error($form['uid'], t('User not found'));
  }
}

function uc_prodimport_import_form_submit($form, &$form_state) {
  $uid = $form_state['values']['uid']
    ? user_load(array( 'name' => $form_state['values']['uid'] ))->uid
    : 0;
  // set datafile upload variables
  $source = 'datafile'; //todo set to random
  $validators = array(
    'file_validate_extensions' => productImporter::allowZip()
      ? array(
        'csv',
        'zip'
      )
      : array( 'csv' ),
  );
  $dest = file_directory_temp();

  // upload datafile
  $file = file_save_upload(
    $source, $validators, $dest, false
  );

  if ( !$file ) {
    drupal_set_message('No file uploaded.');
  }
  try {
    $products_to_delete = _uc_prodimport_get_products_by_user();
    $methods = array(
      'add'  => productImporter::METHOD_FORCE_ADD,
      'upd'  => productImporter::METHOD_ONLY_UPDATE,
      'both' => productImporter::METHOD_MERGE,
    );
    $importer = new BatchProductImporter($file, array(
      'newUID'       => $uid,
      'importMethod' => $methods[$form_state['values']['import_method']],
    ));
    $importer->import('uc_prodimport_import_done');
  } catch (exception $e) {
    drupal_set_message(
      "Exception raised: " . $e->getMessage(), 'error'
    );
  }
}

function uc_prodimport_import_product_submit(&$form, &$form_alter) {
  drupal_redirect_form($form, 'node/' . $form['nid']['#value'] . '/import');
}

function uc_prodimport_update_node($nid) {
  return drupal_get_form('uc_prodimport_update_node_form', $nid);
}

function uc_prodimport_update_node_form(&$form_state, $nid) {

  $form = array(); // reset array
  $form['#redirect'] = 'node/' . $nid . '/edit';
  $form['#attributes'] = array( 'enctype' => "multipart/form-data" );
  $form['datafile'] = array(
    '#type'        => 'file',
    '#title'       => t('Data to upload'),
    '#size'        => 48,
    '#description' => productImporter::allowZip()
      ? t('A CSV in the requisite format or ZIP with all requested data')
      : t('A CSV in the requisite format'),
  );

  $form['back'] = array(
    '#type'  => 'button',
    '#value' => 'Go Back',
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Update Product'),
    '#id'    => 'upload-products',
  );
  $form_state['storage']['nid'] = $nid;
  return $form;
}

function uc_prodimport_update_node_form_submit($form, &$form_state) {
  $nid = $form_state['storage']['nid'];
  // set datafile upload variables
  $source = 'datafile'; //todo set to random
  $validators = array(
    'file_validate_extensions' => productImporter::allowZip()
      ? array(
        'csv',
        'zip'
      )
      : array( 'csv' ),
  );
  $dest = file_directory_temp();

  // upload datafile
  $file = file_save_upload(
    $source, $validators, $dest, false
  );

  if ( !$file ) {
    drupal_set_message('No file uploaded.');
  }
  try {
    $importer = new productImporter(FALSE, array( 'nodeToUpdate' => $nid ));
    $importer->import($file);
    if ( $importer->getStatus() == productImporter::STATUS_SUCCESS ) {
      //Force to reindex apache solr cache
      try {
        if ( module_exists('search') ) {
          search_cron();
        }
        if ( module_exists('apachesolr') ) {
          apachesolr_cron();
        }
        if ( module_exists('apachesolr_attachments') ) {
          apachesolr_attachments_cron();
        }
      } catch (exception $e) {
        //nothing
      }
      drupal_set_message(t("Process completed."));
    } else {
      drupal_set_message(t("There was errors during import process"));
    }
  } catch (exception $e) {
    drupal_set_message(
      "Exception raised: " . $e->getMessage(), 'error'
    );
  }
  drupal_redirect_form($form);
}

function uc_prodimport_import_done($success, $results, $operations) {
  if ( $success ) {
    //find duplicate skus
    $new_products = array_map(create_function('$item', 'return $item->nid;'), $results);
    $duplicates = _uc_products_find_duplicate_skus($new_products);

    array_walk(
      $duplicates, create_function(
        '&$item, $key',
        '$item = l($item->title, "node/" . $item->nid);'
      )
    );
    //Force to reindex apache solr cache
    try {
      if ( module_exists('search') ) {
        search_cron();
      }
      //@todo module always exists even it doesn't work
      if ( module_exists('apachesolr') ) {
        apachesolr_cron();
        $apache = true;
      }
      if ( module_exists('apachesolr_attachments') ) {
        apachesolr_attachments_cron();
      }
    } catch (exception $e) {
      //nothing
    }
    $message = "Process completed. Imported %num products.";
    $message .= isset($apache)
      ? ' You must wait 3 minutes while server reindex data.'
      : '';
    drupal_set_message(
      t(
        $message,
        array( '%num' => count($new_products) )
      )
    );
    if ( !empty($duplicates) ) {
      drupal_set_message(
        t(
          'The following products have duplicate sku: !list',
          array( '!list' => theme('item_list', $duplicates) )
        ), 'warning'
      );
    }
  } else {
    drupal_set_message("There was errors during import process");
  }
  drupal_goto('admin/store/products/uc_prodimport/review/new');
}
