<?php

/**
 * The Product Import module for Ubercart.
 */
/* * ****************************************************************************
 * Includes and constants                                                          *
 * **************************************************************************** */
// Bootstrap Drupal
require 'modules/node/node.pages.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
//
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'productImporter.inc';
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'BatchProductImporter.inc';
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'productExporter.inc';
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'BatchProductExporter.inc';
//system
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'uc_prodimport.embed_form.inc';
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'uc_prodimport.apachesolr.inc';
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'uc_prodimport.export.inc';
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'uc_prodimport.import.inc';
require_once drupal_get_path('module', 'uc_prodimport')
  . DIRECTORY_SEPARATOR . 'uc_prodimport.review.inc';

define('REVIEW_STATUS_PUBLISHED', 0);
define('REVIEW_STATUS_REVIEW', 1);
define('REVIEW_STATUS_NEW', 2);

/* * ****************************************************************************
 * Help functions                                                            *
 * **************************************************************************** */

/**
 * Implementation of hook_help().
 */
function uc_prodimport_help($path, $arg) {
  switch ($path) {
    case 'admin/store/products/uc_prodimport':
    case 'admin/store/products/uc_prodimport/export':
      $output = '<p><font color="#ff0000">'
        . t(
          "WARNING!  Continuing with this module will delete all current "
            . "product information. "
        );
      $output .= t("Make sure you have good backups.") . '</font></p>';
      $output .= '<p>' . ("Disable this module when not in use to prevent accidents.") . '</p>';
    case 'admin/store/products/uc_prodimport/import':
      $output = t(
        'Please select files containing product information to upload. '
          . 'You must upload at least one .csv with correct formatting (see here '
          . 'to download a detailed template with instructions).  The files can '
          . 'be manually uploaded one at a time, or all together in a .zip file. '
          . 'Please note that the existing product record will be deleted during '
          . 'this process, so be sure to include all relevant product '
          . 'information in the .csv uploaded here.'
      );
      break;
    case 'admin/store/products/uc_prodimport/review':
      $output = t(
        'The new products are displayed as they will be seen in the '
          . 'catalog, but they can currently only be viewed by you.  Please '
          . 'review the products for accuracy by selecting the “Edit” tab in the '
          . 'product view.  If you are satisfied with the product entry, then '
          . 'you can select to publish this product from the “Publish” option. '
          . 'You can also have all of the products saved to for later review by '
          . 'selecting one or all of the products and then selecting “Edit Later” '
          . 'at the bottom of the page.  You can add tags to identify and more '
          . 'easily locate products in the Revision Log field.'
      );
      break;
    default:
      $output = '';
  }
  return $output;
}

function uc_prodimport_perm() {
  return array(
    'import products',
    'export products',
    'update own products',
    'export own products',
    'update any products',
  );
}

/* * ****************************************************************************
 * Hooks                                                                      *
 * **************************************************************************** */
/**
 * Function adds some buttons to product form
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function uc_prodimport_form_alter(&$form, &$form_state, $form_id) {
  global $user;

  if ( uc_product_is_product_form($form) ) {
    $node = node_load($form['nid']['#value']);
    if ( $node->review_status == REVIEW_STATUS_NEW ) {
      $form['buttons']['edit_later'] = array(
        '#type'   => 'submit',
        '#value'  => t('Edit Later'),
        '#submit' => array(
          'node_form_submit',
          'uc_prodimport_edit_later'
        ),
        '#weight' => 8,
      );
    }
    if ( $node->review_status != REVIEW_STATUS_PUBLISHED ) { //not published
      $form['buttons']['publish'] = array(
        '#type'   => 'submit',
        '#value'  => t('Publish'),
        '#submit' => array(
          'node_form_submit',
          'uc_prodimport_publish_node'
        ),
        '#weight' => 8,
      );
    }
    if ( uc_prodimport_user_access_and_check_current('update own products', $node->uid)
      || user_access('update any products')
    ) {
      $form['buttons']['import'] = array(
        '#type'   => 'submit',
        '#value'  => t('Update from CSV'),
        '#submit' => array( 'uc_prodimport_import_product_submit' ),
      );
    }
    if ( user_access('export own products') ) {
      $form['buttons']['export'] = array(
        '#type'   => 'submit',
        '#value'  => t('Export to CSV'),
        '#submit' => array( 'uc_prodimport_export_node' ),
      );
    }
  }
}

/* * ****************************************************************************
 * File functions                                                          *
 * **************************************************************************** */


/* * ****************************************************************************
 * Database Functions                                                            *
 * **************************************************************************** */

function _get_selected_products($keys = false, $index = 'products_to_export') {
  $values = $_SESSION[$index];
  if ( !empty($values) ) {
    $values = unserialize($values);
    $values = array_filter($values);
    if ( !$keys ) {
      $values = array_keys($values);
    }
  } else {
    $values = array();
  }
  return $values;
}

function _clean_selected_products($index = 'products_to_export') {
  $_SESSION[$index] = null;
}

function _uc_prodimport_get_url_prefix() {
  static $prefix;
  if ( !isset($prefix) ) {
    $prefix = '';
    $path = arg();
    while (false != $item = array_shift($path)) {
      //limitations without include to prefix
      if ( $item == 'export' ) {
        break;
      }
      $prefix .= $item . '/';
      //limitations with include to prefix
      if ( $item == 'uc_prodimport' ) {
        break;
      }
    }
  }
  return $prefix;
}

function _uc_products_find_duplicate_skus($ids) {
  if ( empty($ids) ) {
    return array();
  }
  $query = '
        SELECT p.nid, n.title as title, p.model as model, c.cnt as count
        FROM {uc_products} p
        LEFT JOIN (SELECT model, count(nid) as cnt FROM {uc_products} GROUP BY model) c ON p.model = c.model
        LEFT JOIN {node} n ON p.nid = n.nid
        WHERE p.nid IN (' . db_placeholders($ids) . ')
        AND c.cnt > 1';
  $result = db_query($query, $ids);
  $duplicates = array();
  while (false != $item = db_fetch_object($result)) {
    $duplicates[] = $item;
  }
  return $duplicates;
}

function _uc_prodimport_get_cck_taxonomy_vid($field) {
  $data = content_fields($field);
  if ( isset($data['vid']) ) {
    return $data['vid'];
  }
  return 0;
}

// <editor-fold defaultstate="collapsed" desc="+++">
function uc_prodimport_nodeapi(&$node, $op, $a3 = null, $a5 = null) {
  switch ($op) {
    case 'load':
      $result = db_query(
        'SELECT f.*, i.review_status, i.uid FROM {files} f LEFT JOIN {uc_imported_products} i ON (i.fid = f.fid) WHERE i.nid = %d',
        array( $node->nid )
      );
      $node->source_file = db_fetch_object($result);
      if ( $node->source_file ) {
        $node->review_status = $node->source_file->review_status;
        unset($node->source_file->review_status);
        $node->imported_by = user_load($node->source_file->uid);
        unset($node->source_file->uid);
      } else {
        //default status for non imported data
        $node->review_status = REVIEW_STATUS_PUBLISHED;
      }
      // adding field created_by
      break;
    case 'update':
      //we can't switch temporary for manually created products so there is no
      //reason for additional checks or actions
      db_query(
        'UPDATE {uc_imported_products} SET review_status=%d WHERE nid = %d',
        $node->review_status, $node->nid
      );
      break;
    case 'delete':
      if ( $node->source_file ) {
        $fid = $node->source_file->fid;
      }
      db_query(
        'DELETE FROM {uc_imported_products} WHERE nid = %d', array( $node->nid )
      );
      //removing unused file
      if ( $fid
        && 0 == db_result(
          db_query(
            'SELECT COUNT(*) FROM {uc_imported_products} WHERE fid=%d', $fid
          )
        )
      ) {
        $query = db_query('SELECT * FROM {files}');
        $file = db_fetch_object($query);
        file_delete($file->filepath);
      }
      break;
  }
}

//2 практически идентичных функции. не нравится мне это.
function _uc_prodimport_get_temporary_products() {
  global $user;

  $field_image_file = content_fields('field_image_cache');
  $db_image = content_database_info($field_image_file);
  $table_image_file = $db_image['table'];
  $column_image_file = $db_image['columns']['fid']['column'];
  $query = 'SELECT n.nid as nid, n.title as title, fv.filepath as image_path
  FROM {node} n
  RIGHT JOIN {uc_imported_products} ip ON ip.nid = n.nid
  LEFT JOIN {' . $table_image_file . '} tv ON tv.nid = n.nid AND tv.delta = 0
  LEFT JOIN {files} fv ON fv.fid = tv.' . $column_image_file . '
  WHERE n.type = "product"
  AND n.uid = ' . $user->uid . '
  AND ip.review_status = 2
';
  $query = pager_query($query);
  $result = array();
  while ($item = db_fetch_array($query)) {
    $result[] = $item;
  }
  return $result;
}

function _uc_prodimport_get_nonpublished_products() {
  global $user;

  $field_image_file = content_fields('field_image_cache');
  $db_image = content_database_info($field_image_file);
  $table_image_file = $db_image['table'];
  $column_image_file = $db_image['columns']['fid']['column'];
  $query = 'SELECT n.nid as nid, n.title as title, fv.filepath as image_path
  FROM {node} n
  RIGHT JOIN {uc_imported_products} ip ON ip.nid = n.nid
  LEFT JOIN {' . $table_image_file . '} tv ON tv.nid = n.nid AND tv.delta = 0
  LEFT JOIN {files} fv ON fv.fid = tv.' . $column_image_file . '
  WHERE n.type = "product"
  AND n.uid = ' . $user->uid . '
  AND ip.review_status = 1
';
  $query = pager_query($query);
  $result = array();
  while ($item = db_fetch_array($query)) {
    $result[] = $item;
  }
  return $result;
}

function _uc_prodimport_get_products_by_user($uid = 0) {
  global $user;
  if ( $uid == 0 ) {
    $uid = $user->uid;
  }
  $query = 'SELECT nid FROM {node} n WHERE n.uid = %d AND n.type = "product"';
  $query = db_query($query, $uid);
  $result = array();
  while ($item = db_fetch_array($query)) {
    $result[] = $item;
  }
  return $result;
}

// </editor-fold>

/* * ****************************************************************************
 * Menu functions                                                             *
 * **************************************************************************** */

function uc_prodimport_user_access_and_check_current($string, $uid) {
  global $user;
  return $user->uid == $uid && user_access($string);
}

function user_access_multiple($access) {
  if ( !is_array($access) ) {
    $access = array( $access );
  }
  foreach ($access as $value) {
    if ( user_access($value) ) {
      return true;
    }
  }
  return false;
}

function uc_prodimport_menu() {

  $items = array();
  $items['admin/store/products/uc_prodimport'] = array(
    'title'            => t('Import and export products'),
    'description'      => 'Import and export products into/from UberCart.',
    'type'             => MENU_NORMAL_ITEM,
    'page callback'    => 'uc_prodimport_export',
    'weight'           => 10,
    'access callback'  => 'user_access_multiple',
    'access arguments' => array(
      array(
        'export products',
        'export own products'
      )
    ),
  );
  $items['admin/store/products/uc_prodimport/export'] = array(
    'title'       => 'Export products',
    'type'        => MENU_DEFAULT_LOCAL_TASK,
    'description' => 'Export products to CSV',
  );
  $items['admin/store/products/uc_prodimport/import'] = array(
    'title'            => 'Import products',
    'description'      => 'Import products into UberCart.',
    'type'             => MENU_CALLBACK,
    'page arguments'   => array( 'uc_prodimport_import_form' ),
    'page callback'    => 'drupal_get_form',
    'weight'           => -2,
    'access callback'  => 'user_access_multiple',
    'access arguments' => array(
      array(
        'import products',
        'update own products',
        'update any products'
      )
    ),
  );
  $items['admin/store/products/uc_prodimport/review'] = array(
    'title'            => 'Products review',
    'type'             => MENU_CALLBACK,
    'description'      => 'Review new items',
    'page callback'    => 'uc_prodimport_review',
    'page arguments'   => array(),
    'access callback'  => 'user_access_multiple',
    'access arguments' => array(
      array(
        'import products',
        'update own products',
        'update any products'
      )
    ),
  );
  $items['admin/store/products/uc_prodimport/cancel'] = array(
    'type'            => MENU_CALLBACK,
    'title'           => '',
    'page callback'   => 'uc_prodimport_cancel_wizard',
    'page arguments'  => array(),
    'access callback' => TRUE,
  );
  $items['admin/store/products/uc_prodimport/export/set'] = array(
    'title'            => '',
    'type'             => MENU_CALLBACK,
    'page callback'    => 'uc_prodimport_set_item',
    'page arguments'   => array(),
    'access callback'  => 'user_access_multiple',
    'access arguments' => array(
      array(
        'export products',
        'export own products',
        'import products'
      )
    ),
    'file'             => 'uc_prodimport.embed_form.inc'
  );
  $items['admin/store/products/uc_prodimport/mass_delete'] = array(
    'title'           => '',
    'type'            => MENU_CALLBACK,
    'page callback'   => 'drupal_get_form',
    'page arguments'  => array( 'uc_prodimport_delete_wizard' ),
    //@todo
    'access callback' => TRUE,
//    'access arguments' => 'delete content',
  );
  $items['admin/store/products/uc_prodimport/ddd'] = array(
    'title' => t('Kill them all'),
    'type' => MENU_CALLBACK,
    'page callback' => 'uc_prodimport_delete_all',
    'page arguments' => array(),
    'access callback' => TRUE,
  );
  $items['admin/store/products/uc_prodimport/download/%'] = array(
    'title'            => '',
    'type'             => MENU_CALLBACK,
    'page callback'    => 'uc_prodimport_download_file',
    'page arguments'   => array( 5 ),
    'access arguments' => array( 'export products' ),
  );
  $items['node/%/import'] = array(
    'type'             => MENU_CALLBACK,
    'page callback'    => 'uc_prodimport_update_node',
    'page arguments'   => array( 1 ),
    'access callback'  => 'user_access_multiple',
    'access arguments' => array(
      array(
        'update own products',
        'update any product'
      )
    ),
  );
  return $items;
}

/* * ****************************************************************************
 * Form functions                                                               *
 * **************************************************************************** */

// <editor-fold defaultstate="collapsed" desc="themes">
function uc_prodimport_theme() {
  return array(
//    'export_form_table' => array(
//      'file' => 'uc_prodimport.themes.inc',
//      'arguments' => array( 'form' => null ),
//    ),
    'review_list'      => array(
      'file'      => 'uc_prodimport.themes.inc',
      'template'  => 'review-list',
      'arguments' => array( 'items' => null ),
    ),
    'review_list_item' => array(
      'file'      => 'uc_prodimport.themes.inc',
      'template'  => 'review-list-item',
      'arguments' => array( 'item' => null ),
    ),
  );
}

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="other submits">
/**
 * Delete duplicate SKU page.
 * @deprecated
 * @global stdClass $user
 * @return string page html
 */
function uc_prodimport_duplicate_sku() {
  global $user;

  $view = views_get_view('uc_prodimport_products_list');
  if ( empty($view) ) {
    die('View not installed');
    return;
  }
  $output = '';
  $output .= $view->render('duplicate_sku');

  $output .= drupal_get_form('uc_prodimport_duplicate_sku_form');
  return $output;
}

/**
 * Buttons for duplicate SKU page
 * @deprecated
 * @return array
 */
function uc_prodimport_duplicate_sku_form() {
  $form = array();
  $form['back'] = array(
    '#type'     => 'submit',
    '#value'    => t('Go Back'),
    '#redirect' => 'admin/store/products/uc_prodimport/review/new',
    //@todo or not new
  );
  return $form;
}

/**
 * implementation of hook_db_rewrite_sql
 * Hides reviewing nodes from any queries.
 *
 * @param string $query Query to be rewritten.
 * @param string $primary_table Name or alias of the table which has the primary key field for this query
 * @param string $primary_field Name of the primary field
 * @param array $args Array of additional arguments.
 * @return array
 */
function uc_prodimport_db_rewrite_sql($query, $primary_table, $primary_field, $args) {
  if ( $primary_table == '{node}' OR $primary_table == 'n' ) {
    $result['join'] = "LEFT JOIN {uc_imported_products} up ON $primary_table.nid = up.nid";
    $result['where'] = "up.review_status = 0 OR up.review_status IS NULL";
    return $result;
  }
}

function uc_prodimport_delete_all(){
  ini_set('max_execution_time', '600');
  $query = 'SELECT nid FROM {node} WHERE type = "product"';
  $result = db_query($query);
  while ( FALSE !== $item = db_result($result)){
    //node_delete((int)$item);
  }
  drupal_goto('admin/store/products/uc_prodimport');
}