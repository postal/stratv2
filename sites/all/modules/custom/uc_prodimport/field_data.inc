<?php

/**
 * Converters data for NodeConverter descendants
 */
return array(
  0 => array(
    'field'         => 'title',
    'title'         => 'Title',
    'convertMethod' => 'Text',
  ),
  array(
    'field'             => 'body',
    'title'             => 'Product Description',
    'convertMethod'     => 'Text',
    'methodPostProcess' => '$node->format = 1;',
  ),
  array(
    'field'         => 'field_technical',
    'title'         => 'Technical Information',
    'convertMethod' => 'CCKHTML',
  ),
  array(
    'field'         => 'model',
    'title'         => 'SKU',
    'convertMethod' => 'Text',
  ),
  array(
    'field'         => 'list_price',
    'title'         => 'List price',
    'convertMethod' => 'Number',
  ),
  array(
    'field'             => 'sell_price',
    'title'             => 'Sell price',
    'convertMethod'     => 'Number',
    'methodPostProcess' => 'if ( module_exists("uc_price_per_role") ){ $node->roleindependent_sell_price = $node->sell_price; }'
  ),
  array(
    'field'         => 'cost',
    'title'         => 'Cost',
    'convertMethod' => 'Number',
  ),
  array(
    'field'         => 'weight',
    'title'         => 'Weight',
    'convertMethod' => 'Number',
  ),
  array(
    'field'         => 'weight_units',
    'title'         => 'Weight units',
    'convertMethod' => 'WeightUnits',
  ),
  array(
    'field'         => 'length',
    'title'         => 'Length',
    'convertMethod' => 'Number'
  ),
  array(
    'field'         => 'width',
    'title'         => 'Width',
    'convertMethod' => 'Number',
  ),
  array(
    'field'         => 'height',
    'title'         => 'Height',
    'convertMethod' => 'Number',
  ),
  array(
    'field'         => 'length_units',
    'title'         => 'Lenght/width/height units',
    'convertMethod' => 'sizeUnits',
  ),
  array(
    'field'         => 'pkg_qty',
    'title'         => 'Package quantity',
    'convertMethod' => 'Number',
  ),
  array(
    'field'         => 'default_qty',
    'title'         => 'Default quantity',
    'convertMethod' => 'Number',
  ),
  array(
    'field'         => 'field_catalog',
    'title'         => 'Categories',
    'convertMethod' => 'CCKTaxonomy',
    'methodParams'  => array(
      'vocabulary' => variable_get('uc_catalog_vid', 0),
    )
  ),
  array(
    'field'         => 'field_brand',
    'title'         => 'Brands',
    'convertMethod' => 'CCKTaxonomy',
    'methodParams'  => array(
      'vocabulary'     => _uc_prodimport_get_cck_taxonomy_vid('field_brand'),
      'createNewTerms' => true,
    )
  ),
  array(
    'field'         => 'field_manufacturer',
    'title'         => 'Manufacturer',
    'convertMethod' => 'CCKTaxonomy',
    'methodParams'  => array(
      'vocabulary'     => _uc_prodimport_get_cck_taxonomy_vid('field_manufacturer'),
      'createNewTerms' => true,
    )
  ),
  array(
    'field'         => 'field_image_cache',
    'title'         => 'Images',
    'convertMethod' => 'CCKFile',
    'methodParams'  => array(
      'destination' => $this->variables['arcdir'],
      'exportdir'   => 'images',
    )
  ),
  array(
    'field'         => 'field_video',
    'title'         => 'Videos',
    'convertMethod' => 'CCKFile',
    'methodParams'  => array(
      'destination' => $this->variables['arcdir'],
      'exportdir'   => 'videos',
    )
  ),
  array(
    'field'             => 'field_spec',
    'title'             => 'Specification files',
    'convertMethod'     => 'CCKFile',
    'methodPostProcess' => '$item["data"]["description"] = $item["filename"];',
    'methodParams'      => array(
      'destination' => $this->variables['arcdir'],
      'exportdir'   => 'specs',
    )
  ),
  array(
    'field'         => 'field_featured',
    'title'         => 'Is the product profiled?',
    'convertMethod' => 'CCKNumber',
  ),
  array(
    'field'         => 'shippable',
    'title'         => 'Is Shippable?',
    'convertMethod' => 'Number',
  ),
  array(
    'field'         => 'related',
    'title'         => 'Related products SKU',
    'convertMethod' => 'Related',
  ),
  array(
    'field'         => 'field_volume',
    'title'         => 'Volume',
    'convertMethod' => 'CCKNumber',
  ),
  array(
    'field'         => 'field_volume_units',
    'title'         => 'Volume units',
    'convertMethod' => 'CCKText',
  ),
  array(
    'field'         => 'field_concentration',
    'title'         => 'Concentration',
    'convertMethod' => 'CCKNumber',
  ),
  array(
    'field'         => 'field_concentration_units',
    'title'         => 'Concentration units',
    'convertMethod' => 'CCKText',
  ),
  array(
    'field'         => 'field_storage_temp',
    'title'         => 'Storage temperature',
    'convertMethod' => 'CCKNumber',
  ),
  array(
    'field'         => 'field_format',
    'title'         => 'Format',
    'convertMethod' => 'CCKText',
  ),
  array(
    'field'         => 'field_size',
    'title'         => 'Size',
    'convertMethod' => 'CCKText',
  ),
  array(
    'field'         => 'field_shelf_life',
    'title'         => 'Shelf life',
    'convertMethod' => 'CCKNumber'
  ),
  array(
    'field'         => 'field_shelf_life_units',
    'title'         => 'Shelf life units',
    'convertMethod' => 'CCKText',
  ),
  array(
    'field'         => 'field_storage_cons',
    'title'         => 'Storage Considerations',
    'convertMethod' => 'CCKHTML',
  ),
  array(
    'field'         => 'field_product_cons',
    'title'         => 'Product considerations',
    'convertMethod' => 'CCKHTML',
  ),
  array(
    'field'         => 'field_safety_cons',
    'title'         => 'Safety considerations',
    'convertMethod' => 'CCKHTML',
  ),
  array(
    'field'         => 'field_sertifcates',
    'title'         => 'Certificates',
    'convertMethod' => 'CCKHTML',
  ),
  array(
    'field'         => 'field_guaranty',
    'title'         => 'Guaranty',
    'convertMethod' => 'CCKNumber',
  ),
  array(
    'field'         => 'field_guaranty_units',
    'title'         => 'Guaranty units',
    'convertMethod' => 'CCKText'
  ),
  array(
    'field'         => 'field_warranty',
    'title'         => 'Warranty',
    'convertMethod' => 'CCKNumber',
  ),
  array(
    'field'         => 'field_warranty_units',
    'title'         => 'Warranty units',
    'convertMethod' => 'CCKText'
  ),
  array(
    'field'         => 'shipping_type',
    'title'         => 'Shipping type',
    'convertMethod' => 'Text'
  ),
  array(
    'field'         => 'shipping_address.first_name',
    'title'         => 'Shippings address: First name',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.last_name',
    'title'         => 'Shipping address: Last name',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.company',
    'title'         => 'Shipping address: Company',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.street1',
    'title'         => 'Shipping address: Street address 1',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.street2',
    'title'         => 'Shipping address: Street address 2',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.city',
    'title'         => 'Shipping address: City',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.zone',
    'title'         => 'Shipping address: State/Province name',
    'convertMethod' => 'State',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.postal_code',
    'title'         => 'Shipping address: Postal code',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.phone',
    'title'         => 'Shipping address: Phone',
    'convertMethod' => 'Text',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'shipping_address.country',
    'title'         => 'Shipping address: Country name',
    'convertMethod' => 'Country',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'files',
    'convertMethod' => 'Files',
    'title'         => 'Other Files',
    'methodParams'  => array(
      'destination' => $this->variables['arcdir'],
    )
  ),
  array(
    'field'         => 'field_promotion_dates.%.value',
    'title'         => 'Deal start date',
    'convertMethod' => 'CCKDate',
  ),
  array(
    'field'             => 'field_promotion_dates.%.value2',
    'title'             => 'Deal end date',
    'convertMethod'     => 'CCKDate',
    'methodPostProcess' => '
      $this->setField($node, array("field_promotion_dates", "%", "timezone"), "America/New_York", $this->storageType);
      $this->setField($node, array("field_promotion_dates", "%", "timezone_db"), "America/New_York", $this->storageType);
      $this->setField($node, array("field_promotion_dates", "%", "date_type"), "date", $this->storageType);
    ',
  ),
  array(
    'field'         => 'field_deal_description',
    'title'         => 'Deal description',
    'convertMethod' => 'CCKHTML',
  ),
  array(
    'field'         => 'stock.stock',
    'title'         => 'Stock',
    'convertMethod' => 'Number',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'stock.threshold',
    'title'         => 'Stock threshold',
    'convertMethod' => 'Number',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'stock.active',
    'title'         => 'Is active stock',
    'convertMethod' => 'Number',
    'storageType'   => 'class',
  ),
  array(
    'field'         => 'grants_read',
    'title'         => 'Roles granted to view product',
    'convertMethod' => 'Grants',
  ),
  array(
    'field'         => 'grants_read_price',
    'title'         => 'Roles granted to view price of products',
    'convertMethod' => 'Grants',
  ),
  array(
    'field'         => 'uid',
    'title'         => 'Seller',
    'convertMethod' => 'User',
  ),
  array(
    'field'         => 'site_model',
    'title'         => 'Site SKU',
    'convertMethod' => 'Text',
  ),
  array(
    'field'         => 'nid',
    'title'         => 'Product Id',
    'convertMethod' => 'Number',
  )
);
