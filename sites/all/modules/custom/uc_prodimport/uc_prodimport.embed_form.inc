<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
drupal_add_js(drupal_get_path('module', 'uc_prodimport') . '/js/add_remove_item.js');

function uc_prodimport_views_embed_form() {
  $forms = array();
  if ( user_access('export products') ){
    $forms['uc_prodimport_export_item_embed_form'] = t('new embedded form');
  }
  if ( user_access_multiple(array('import products', 'update own products', 'update any products')) ){
    $forms['uc_prodimport_review_item_embed_form'] = t('Review item element');
  }
  return $forms;
}

function uc_prodimport_export_item_embed_form(&$form_state, $row) {
  $data = _get_selected_products(true);

  $form = array( );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => intval($row->nid),
  );
  $form['submit'] = array(
    '#type' => 'checkbox',
    '#attributes' => array(
      'class' => 'add-list-button',
    ),
    '#return_value' => $row->nid,
    '#default_value' => (isset($data[$row->nid]) && $data[$row->nid])
      ? $row->nid
      : 0,
  );
  return $form;
}

function uc_prodimport_review_item_embed_form(&$form_state, $row){
  $data = _get_selected_products(true, 'products_to_review');

  $form = array( );
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => intval($row->nid),
  );
  $form['submit'] = array(
    '#type' => 'checkbox',
    '#attributes' => array(
      'class' => 'add-list-button for-review',
    ),
    '#return_value' => $row->nid,
    '#default_value' => (isset($data[$row->nid]) && $data[$row->nid])
      ? $row->nid
      : 0,
  );
  return $form;
}

function uc_prodimport_set_item() {
  if ( !isset($_GET['nid']) ){
    die;
  }
  if ( $_GET['index'] == 'review' ){
    $index = 'products_to_review';
  }else{
    $index = 'products_to_export';
  }

  $nids = explode(',', $_GET['nid']);

  if ( isset($_GET['set']) ){
    $set = json_decode($_GET['set']);
  }else{
    $set = false;
  }

  $data = _get_selected_products(true, $index);

  foreach( $nids as $nid ){
    if ( $set ){
      $data[$nid] = true;
    }else{
      unset($data[$nid]); //there is no "false"
    }
  }
  $_SESSION[$index] = serialize($data);
  print drupal_json((int) $data[$nid]);
  die;
}