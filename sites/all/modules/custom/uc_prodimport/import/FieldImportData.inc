<?php

require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'classes.inc';

/**
 * Description of FieldImportData
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
class FieldImportData extends FieldData {

  public $methodPostProcess = null;
  public $storageType = 'array';

  public function __construct($params = array( )) {
    $params['classprefix'] = 'import';
    parent::__construct($params);
  }

  private function setField(&$node, $fieldset, $value, $type='array') {
    $newItem = $type == 'array'
      ? create_function('', 'return array();')
      : create_function('', 'return new stdClass;');

    $item = &$node;
    $i = 0;
    if ( empty($fieldset) ){
      $node = $value;
      return;
    }
    foreach( $fieldset as $field ){
      $i++;
      if ( $field == '%' ){
        if ( !is_array($value) ){
          $value = array( $value );
        }
        if ( !is_array($item) ){
          $item = array( );
        }
        $fieldSubSet = array_slice($fieldset, $i);
        foreach( $value as $key => $value_item ){
          $subnode = isset($item[$key])
            ? $item[$key]
            : $newItem();
          $this->setField($subnode, $fieldSubSet, $value_item, $type);
          $item[$key] = $subnode;
        }
        //we filled up the tree. We do not need to continue
        return;
      }elseif ( !(is_array($item)
          ? isset($item[$field])
          : isset($item->$field) ) ){
        if ( is_array($item) ){
          $item[$field] = $newItem();
        }else{
          $item->$field = $newItem();
        }
      }
      if ( is_array($item) ){
        $item = &$item[$field];
      }else{
        $item = &$item->$field;
      }
    }
    $item = $value;
    unset($item);
  }

  public function execute(&$node, $data = null) {
    if ( $this->convertMethod instanceof nodeConvertMethod ){

      if ( $this->field ){
        watchdog('fieldData', 'Setting field %field', array( '%field' => $this->field ), WATCHDOG_DEBUG);
        try{
          $this->setField(
            $node,
            explode('.', $this->field),
            $this->convertMethod->convert($data),
            $this->storageType
          );
        }catch( ConvertException $e){
          $message = t('Problem found while importing field %field for product %name: ',
            array(
              '%field' => $this->title,
              '%name' => $node->title,
            )
          ) . $e->getMessage();
          drupal_set_message($message, $e->getSevereness());
          if ( !$e->getCanContinue() ){
            //throw to next 
            throw $e;
          }
        }
      }
    }
    if ( !empty($this->methodPostProcess) ){
      eval($this->methodPostProcess);
    }
  }

}
