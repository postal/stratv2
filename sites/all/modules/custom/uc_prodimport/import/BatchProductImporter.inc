<?php
/**
 * Created by JetBrains PhpStorm.
 * Date: 28.02.12
 * Time: 11:27
 * @file Source for class BatchProductImporter
 */
function product_import_batch($settings, &$context) {
  if ( isset($sandbox['class']) ) {
    $batch = unserialize($sandbox['class']);
  } else {
    $batch = new BatchProductImporter($settings['file'], $settings);
  }
  $batch->batch($context);
  $sandbox['class'] = serialize($batch);
}

/**
 * Class for batch import products.
 */
class BatchProductImporter extends productImporter implements Serializable {

  private function getObjectProperties() {
    //@todo filter out useless and single operation properties
    $properties = array(
      'readGrantsAsReadPriceGrants',
      'nodeToUpdate',
      'newUID',
      'importMethod',
      'basePath',
      'fieldDelimiter',
      'archive',
      'file',
    );
    $data = array();
    foreach ($properties as $key) { //key is value... or value is the key...
      $data[$key] = $this->$key;
    }
    return $data;
  }

  public function serialize() {
    return serialize($this->getObjectProperties());
  }

  public function unserialize($serialized) {
    $this->init(unserialize($serialized));
  }

  public function __destruct() {
    //nothing
  }

  public function import($callback) {
    $settings = $this->getObjectProperties();
    $operations = array(
      array(
        'product_import_batch',
        array( $settings )
      ),
    );

    $batch = array(
      'operations'       => $operations,
      'finished'         => $callback,
      // We can define custom messages instead of the default ones.
      'title'            => t('Import products'),
      'init_message'     => t('Initializing.'),
      'progress_message' => '',
      'error_message'    => t('Import data has encountered an error.'),
    );
    batch_set($batch);
  }

  public function batch(&$context) {
    $sandbox = &$context['sandbox'];

    if ( !isset($sandbox['op']) ) {
      $sandbox['op'] = 'prepare';
    }
    if ( $sandbox['op'] == 'prepare' ) {
      $context['result'] = array();
      $context['message'] = 'Starting import';
      $context['finished'] = 0;
      if ( $this->file->filemime == 'application/zip' ) {
        $sandbox['op'] = 'extract';
      } else {
        $sandbox['op'] = 'csv';
      }

    } elseif ( $sandbox['op'] == 'extract' ) {
      $context['message'] = 'Extracting archive';
      $this->basePath = $this->extractArchive($this->file);
      $this->file = $this->searchCSV($this->basePath);
      $context['finished'] = 0.1;
      $sandbox['op'] == 'csv';

    } elseif ( $sandbox['op'] == 'csv' ) {
      $sandbox['data'] = $this->readCSV($this->file);
      $sandbox['total'] = count($sandbox['data']);
      $context['message'] = 'Reading CSV';
      $context['finished'] = 0.15;
      $sandbox['op'] = 'save nodes';

    } elseif ( $sandbox['op'] == 'convert' ) {
      $context['message'] = 'Analyzing CSV data';
//      try {
//        $sandbox['data'] = $this->convertParsedDataToNodeData($sandbox['data']);
//      }catch(exception $e){
//        drupal_set_message($e->getMessage(), 'error');
//      }
//      $sandbox['total'] = count($sandbox['data']);
//      $sandbox['op'] = 'save nodes';
//      $this->raiseEvent('beforeSaveNodes', new nodeListEvent($sandbox['data'], $this));
      $context['finished'] = 0.3;

    } elseif ( $sandbox['op'] == 'save nodes' ) {
      $data = array_shift($sandbox['data']);

      $context['message'] = 'Creating product ' . $data[0] . '. '
        . (count($sandbox['nodes'])) . ' done, '
        . (count($sandbox['data'])) . ' remaining';

      $params = array(
        'variables' => array(
          'arcdir' => $this->basePath,
        )
      );
      $parser = new NodeImporter($params);
      $node = new stdClass();
      $this->setPersistentValues($node);
      try {
        $data = $parser->process($node, $data);
      }catch(exception $e ){
        drupal_set_message($e->getMessage(), 'error');
        return;
      }

      $data = $this->saveNodeData($data);
      if ( !empty($data) ) {
        $sandbox['nodes'][] = $data;
      }
      $progress = ($sandbox['total'] - count($sandbox['data'])) / $sandbox['total'];
      $context['finished'] = 0.15 + $progress * 0.75; // from 0.3 to 0.9
      if ( empty($sandbox['data']) ) {
        //we are finished
        $this->raiseEvent('afterSaveNodes', new nodeListEvent($sandbox['nodes'], $this));
        $context['finished'] = 1;
        $context['results'] = $sandbox['nodes'];
        //
        if ( $this->file->filemime == 'application/zip' ) {
          fileUtils::delTree($this->basePath);
        }
        file_delete($this->file->filepath);
      }
    }
  }
}
