<?php

require_once drupal_get_path(
  'module', 'uc_prodimport'
) . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'classes.inc';
require_once drupal_get_path(
  'module', 'uc_prodimport'
) . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'NodeConverter.inc';
require_once drupal_get_path(
  'module', 'uc_prodimport'
) . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'converters.inc';
require_once drupal_get_path(
  'module', 'uc_prodimport'
) . DIRECTORY_SEPARATOR . 'import' . DIRECTORY_SEPARATOR . 'FieldImportData.inc';

/**
 * Controller class for import products
 *
 */
class productImporter extends Loadable {


  /**
   * Installed plugins
   * @var array
   */
  private $plugins = array();
  /**
   * Base path for importing files
   * @var string
   */
  protected $basePath;

  /**
   * Struct with file information
   * @var stdClass
   */
  protected $file;

  /**
   * Process status
   * @var integer
   */
  private $status;

  /**
   * Delimiter of CSV fields. Default is "," (comma)
   * @var string
   */
  protected $fieldDelimiter = ',';

  /**
   * Archive
   * @var stdClass
   */
  protected $archive;

  /**
   * Use grants for read as default grants for read price
   * @var boolean
   */
  public $readGrantsAsReadPriceGrants = true;

  /**
   * Node to update
   * @var int
   */
  public $nodeToUpdate = 0;

  /**
   * UID for new products
   * @var integer
   */
  public $newUID;

  /**
   * Importing method: add, update or merge.
   * @var integer
   */
  protected $importMethod;

  const METHOD_ONLY_UPDATE = 1;
  const METHOD_MERGE = 2;
  const METHOD_FORCE_ADD = 3;

  const STATUS_SUCCESS = 0;
  const STATUS_FAILURE = 1;
  const STATUS_NEW = 2;
  const STATUS_NO_FILE = 3;

  /**
   *
   * @param stdClass $file File to proceed
   * @param array $config
   */
  public function __construct($file = false, $config = array()) {
    $this->setStatus(self::STATUS_NO_FILE);
    if ( $file ) {
      $this->setFile($file);
    }
    $this->loadPlugins();
    parent::__construct($config);
  }

  public function __destruct() {
    if ( $this->file->filemime == 'application/zip' ) {
      fileUtils::delTree($this->basePath);
    }
    file_delete($this->file->filepath);
  }

  protected function setFile($file) {
    $this->file = $file;
    $this->setStatus(self::STATUS_NEW);
  }

  public function getFile() {
    return $this->file;
  }

  private function loadPlugins() {
    $dir = drupal_get_path('module', 'uc_prodimport')
      . DIRECTORY_SEPARATOR
      . 'import'
      . DIRECTORY_SEPARATOR
      . 'plugins'
      . DIRECTORY_SEPARATOR;
    $files = glob($dir . '*.inc');

    foreach ($files as $file) {
      require_once $file;

    }

    $classes = get_declared_classes();
    foreach ($classes as $class) {
      if ( is_subclass_of($class, 'basePlugin') ) {
        $this->plugins[] = new $class($this);
      }
    }
  }

  /**
   * setter for status field
   * @param integer $status
   */
  protected function setStatus($status) {
    $this->status = $status;
  }

  /**
   * getter for status field
   * @return integer
   */
  public function getStatus() {
    return $this->status;
  }

  public function getBasePath() {
    return $this->basePath;
  }

  public function import($file = false) {
    if ( $file ) {
      $this->setFile($file);
    }

    if ( $this->getStatus() == self::STATUS_NO_FILE ) {
      throw new Exception('No files sent to proceed');
    }

    //zip
    $this->archive = $this->file;
    if ( $this->archive->filemime == 'application/zip' ) {
      if ( self::allowZip() ) {
        $this->basePath = $dir = $this->extractArchive($this->archive);
        $this->file = $this->searchCSV($dir);
      } else {
        throw new Exception('Zip files are denied');
      }
    } else {
      //there is no matter, actually
      $this->basePath = dirname($this->file->filepath);
    }

    //load data
    $data = $this->readCSV($this->file);
    if ( empty($data) ) {
      throw new Exception('Failed to parse CSV files. Operation aborted');
    }
    if ( $this->nodeToUpdate ) {
      //leave only first line
      $data = array( reset($data) );
    }
    //parse data
    $data = $this->convertParsedDataToNodeData($data);
    file_copy($this->archive);
    $this->archive->status = FILE_STATUS_PERMANENT;
    drupal_write_record('files', $this->archive);

    $ids = $this->saveNodes($data);
    if ( !empty($ids) ) {
      $this->setStatus(self::STATUS_SUCCESS);
    }

    return $ids;
  }

  protected function setPersistentValues(&$node) {

    $node->type = 'product'; // constant
    $node->created = time(); // constant
    $node->changed = $node->created; // constant
    $node->status = 1; // constant
    $node->old_status = 0; // constant
    $node->promote = 1; // constant
    $node->sticky = 0; // constant
    $node->format = 1; // Filtered HTML - constant
    $node->language = 'en'; // constant
  }

// imports data file and calls node insert functions
  protected function convertParsedDataToNodeData($data) {
    watchdog(
      'productImporter',
      'Started convert csv to node data',
      array(),
      WATCHDOG_DEBUG
    );
    $params = array(
      'variables' => array(
        'arcdir' => $this->basePath,
      )
    );
    $parser = new NodeImporter($params);
    foreach ($data as &$row) {
      $node = new stdClass();
      $this->setPersistentValues($node);
      $row = $parser->process($node, $row);
    }
    return $data;
  }

  /**
   *
   * @return boolean
   */
  public static function allowZip() {
    return class_exists('ZipArchive');
  }

  private function mergeNodes($old, $new) {
    foreach ($new as $field => $value) {
      if ( !empty($value) ) {
        if ( $value == '<empty>' ) {
          $old->$field = null;
        } else {
          $old->$field = $value;
        }
      }
    }
    return $old;
  }

  protected function saveNodes($nodesData) {

    foreach ($nodesData as &$node) {
      $node = $this->saveNodeData($node);
    }
    //filter out null values
    $nodesData = array_filter($nodesData);
    //post process list
    $this->raiseEvent('afterSaveNodes', new nodeListEvent($nodesData, $this));
    //ids of the new nodes
    $ids = array_map(create_function('$item', 'return $item->nid;'), $nodesData);
    return $ids;
  }

  /**
   * Creates new node from given data.
   * @param $nodeData data for node
   * @return stdClass saved node object
   */
  protected function saveNodeData($nodeData) {
    global $user;

    //backup data, because some core functions rewrite data we need in future
    $backup = $nodeData;

    // insert to core node and product tables
    // hint: this function also rewrite node uid
    node_object_prepare($nodeData);

    //overwrite value
    if ( $this->newUID ) {
      $nodeData->uid = $this->newUID;
    } else {
      $nodeData->uid = $backup->uid;
    }

    //force to update
    if ( $this->nodeToUpdate ) {
      $nodeData->nid = $this->nodeToUpdate;
    }

    if ( $nodeData->nid ) {
      if ( $this->importMethod == self::METHOD_FORCE_ADD ) {
        //set as new
        unset($nodeData->nid);
        $nodeData->revision = 0;
        $nodeData->review_status = REVIEW_STATUS_NEW;
      }
    } elseif ( $this->importMethod == self::METHOD_ONLY_UPDATE ) {
      //no nid - no update
      return null;
    }

    if ( $nodeData->nid ) {
      $nodeData->revision = 1;
      $nodeData->review_status = REVIEW_STATUS_PUBLISHED;
      $old_node = node_load($nodeData->nid);
      if ( !$old_node ) {
        drupal_set_message(t('No node to update'));
        return null;
      } elseif ( $user->uid != $old_node->uid && !user_access('update any product') ) {
        drupal_set_message(
          t(
            'You have not permissions to update node %name',
            array( '%name' => $old_node->name )
          )
        );
        return null;
      } else {
        //updating
        if ( !user_access('edit price') ) {
          $nodeData->list_price = $old_node->list_price;
          $nodeData->sell_price = $old_node->sell_price;
        }
        if ( !user_access('edit product sku') ) {
          $nodeData->model = $old_node->model;
        }
        if ( !user_access('edit site sku') ) {
          $nodeData->site_model = $old_node->site_model;
        }
        if ( !user_access('title') ) {
          $nodeData->title = $old_node->title;
        }
        $nodeData = $this->mergeNodes($old_node, $nodeData);
      }
    }

    $this->raiseEvent('beforeSaveNode', new nodeEvent($this, $nodeData));
    node_save($nodeData);
    if ( !$nodeData->nid ) {
      watchdog(
        'uc_prodimport',
        'Failed to save node %name',
        array( '%name' => $nodeData->title ),
        WATCHDOG_ERROR
      );
      return null;
    }
    $this->raiseEvent('afterSaveNode', new nodeEvent($this, $nodeData));


    //content_insert($node);
    //write stock info
    if ( isset($backup->stock) ) {
      db_query(
        'INSERT INTO
            {uc_product_stock} (sku, active, stock, threshold)
          VALUES ("%s", %d, %d, %d)
          ON DUPLICATE KEY UPDATE
            active = VALUES(active),
            stock = VALUES(stock),
            threshold = VALUES(threshold)
          ',
        $nodeData->model,
        $backup->stock->active,
        $backup->stock->stock,
        $backup->stock->threshold
      );
    }
    //link node with archive
    db_query(
      'DELETE FROM {uc_imported_products} WHERE nid = %d',
      $nodeData->nid
    );
    db_query(
      'INSERT INTO {uc_imported_products} (nid, fid, uid) VALUES(%d, %d, %d);',
      array(
        $nodeData->nid,
        $this->archive->fid,
        $user->uid,
      )
    );
    //logs
    watchdog(
      'uc_prodimport',
      'Node %name saved with id %nid',
      array(
        '%name' => $nodeData->title,
        '%nid'  => $nodeData->nid
      ),
      WATCHDOG_INFO
    );
    return $nodeData;
  }

  /**
   * Read the csv file
   * @param stdClass|array $file
   * @return array Array of data
   */
  protected function readCSV($files) {
    if ( !is_array($files) ) {
      $files = array( $files );
    }

    array_walk($files, create_function('&$item', '$item = $item->filepath;'));

    $parsedData = array();
    foreach ($files as $file) {
      try {
        // read in csv file
        if ( ($handle = fopen($file, "r")) !== FALSE ) { //Open the File.
          // Set the parent multidimensional array key to 0.
          while (($data = fgetcsv($handle, 0, $this->fieldDelimiter)) !== FALSE) {
            // Count the total keys in the row.
            if ( $data != null ) {
              $parsedData[] = $data;
            }
          }
          // Close the File.
          fclose($handle);
        }
      } catch (exception $e) {
        watchdog(
          'uc_prodimport',
          'Failed to process file %file',
          array( '%file' => $file ),
          WATCHDOG_ERROR
        );
        return false;
      }
    }
    //remove column titles
    array_shift($parsedData);

    if ( empty($parsedData) ) {
      drupal_set_message('No data to import', 'error');
    }
    return $parsedData;
  }

  /**
   * Extract archive
   * @param stdClass $file archive data
   * @return string base directory for extracted files
   */
  protected function extractArchive(stdClass $file) {
    watchdog(
      'uc_prodimport',
      'Getting data from archive %name',
      array(
        '%name' => $file->filename
      ),
      WATCHDOG_DEBUG
    );
    $zip = new ZipArchive();
    $zip->open($file->filepath);
    $dir = file_directory_temp() . DIRECTORY_SEPARATOR . uniqid(time(), true);
    watchdog(
      'uc_prodimport',
      'Directory to extract %file is %dir',
      array(
        '%file' => $file->filename,
        '%dir'  => $dir
      ),
      WATCHDOG_DEBUG
    );
    mkdir($dir);
    $zip->extractTo($dir);
    $zip->close();
    return $dir;
  }

  /**
   * Scan directory for csv files. Mostly used after extract an archive.
   * @param $dir directory to search
   * @return array array of files
   */
  protected function searchCSV($dir) {
    $files = file_scan_directory($dir, '.+\.csv');
    foreach ($files as &$file) {
      //upload and scan returns different formats
      $file->filepath = $file->filename;
      $file->filename = $file->name;
    }
    //replace original archive to extracted csv
    return $files;
  }

  /**
   * Raises an event
   * @param string $name event name
   * @param baseEvent $eventClass event data
   */
  protected function raiseEvent($name, $eventClass) {
    $name = 'on' . $name;
    foreach ($this->plugins as $plugin) {
      if ( method_exists($plugin, $name) ) {
        $plugin->$name($eventClass);
      }
    }
  }
}
