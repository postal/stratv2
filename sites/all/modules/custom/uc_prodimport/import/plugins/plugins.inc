<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Юрий
 * Date: 21.02.12
 * Time: 12:27
 */
class mpQuotePlugin extends basePlugin {

  public function onAfterSaveNode(nodeEvent $event) {
    if ( module_exists('mp_quote') ) {
      db_query(
        "INSERT IGNORE {mp_quote_products} (nid, quote_method) VALUES (%d, '%s')",
        $event->node->nid,
        '0'
      );
    }
  }

}

class nodeGrants extends basePlugin {

  /**
   * Is node new?
   * @var boolean
   */
  private $isNewNode;

  private function getRoles() {
    $result = db_query('SELECT rid, name from {role}');
    $roles = array();
    while (false != $item = db_fetch_object($result)) {
      $roles[$item->rid] = $item->name;
    }
    return $roles;
  }

  private function getUsers() {
    $result = db_query('SELECT uid, name FROM {users}');
    $users = array();
    while (false != $item = db_fetch_object($result)) {
      $users[$item->uid] = $item->name;
    }
    return $users;
  }

  public function onBeforeSaveNode(nodeEvent $event) {
    $this->isNewNode = !isset($event->node->id);
  }

  public function onAfterSaveNode(nodeEvent $event) {
    if ( !$this->isNewNode ) {
      return;
    }
    $roles = $this->getRoles();
    $users = $this->getUsers();

    $indexed_grants = array();
    foreach ($event->node->grants_read as $name) {
      if ( false != $grant_id = array_search($name, $roles) ) {
        $realm = 'nodeaccess_rid';
      } elseif ( false != $grant_id = array_search($name, $users) ) {
        $realm = 'nodeaccess_uid';
      } else {
        continue;
      }
      $indexed_grants[$grant_id] = array(
        'realm' => $realm,
        'name'  => $name
      );

      $query = 'SELECT count(*) FROM {node_access} WHERE nid = %d AND gid = %d AND realm = "%s"';
      if ( 0 < db_result(db_query($query, $event->node->nid, $grant_id, $realm)) ) {
        $query = 'UPDATE {node_access} SET grant_view = 1 WHERE nid = %d AND gid = %d AND realm = "%s"';
      } else {
        $query = 'INSERT IGNORE INTO {node_access} (nid, gid, realm, grant_view)
            VALUES (%d, %d, "%s", 1)';
      }
      db_query($query, $event->node->id, $grant_id, $realm);
    }
    $event->node->grants_read = $indexed_grants;
    //read price
    $grants = $event->node->grants_read_price;
    if ( !is_array($grants) ) { //empty
      $grants = array();
    }
    foreach ($grants as &$grant) {
      $grant = array_search($grant, $roles);
    }
    $grants = array_filter($grants);
    //default grants for read price
    if ( $this->owner->readGrantsAsReadPriceGrants ) {
      $default_grants = array_map(
        create_function(
          '$item, $key', 'return $item["realm"] == "nodeaccess_rid" ? $key : false;'
        ), array_values($event->node->grants_read), array_keys($event->node->grants_read)
      );
      $default_grants = array_filter($default_grants);
      $grants += $default_grants;
      $grants = array_unique($grants);
    }
    $grants = implode(',', $grants);
    db_query("DELETE FROM {uc_price_visibility} WHERE nid = %d", $event->node->nid);
    db_query(
      "INSERT INTO {uc_price_visibility} (nid, roles) VALUES (%d, '%s')", $event->node->nid, $grants
    );
  }

}

class priceVisibility extends basePlugin {

  public function onBeforeSaveNode(nodeEvent $event) {
    if ( module_exists('uc_price_visibility') ) {
      //default value
      if ( !isset($event->node->uc_price_visibility) ) {
        $event->node->uc_price_visibility = variable_get(
          'uc_price_visibility_roles', array()
        );
      }
    }
  }

}

class ucQuote extends basePlugin {

  public function onBeforeSaveNode(nodeEvent $event) {
    //something stupid, but uc_quote looks for shipping type data in node fields list
    foreach ($event->node->shipping_address as $key => $value) {
      $event->node->$key = $value;
    }
  }
}

class ucProducts extends basePlugin {
  private $related_products = array();

  public function __construct(&$owner) {
    $this->related_products = variable_get(session_id(), array());
    parent::__construct($owner);
  }

  public function onAfterSaveNode(nodeEvent $event) {
    $this->related_products[$event->node->nid] = $event->node->related;
    //there is a good idea to put this to destructor, but "variable_set"
    //doesn't work in destructors.
    variable_set(session_id(), $this->related_products);
  }

  public function onAfterSaveNodes(nodeListEvent $event) {
    //есть список нод, у каждой есть поле related со списком SKU,
    //задача - переделать список в nid и сохранить его.
    foreach ($this->related_products as $master_nid => $models) {
      $models = array_filter($models);
      if ( !empty($models) ) {
        $related = array();
        $result = db_query('SELECT nid FROM {uc_products} WHERE model IN (' . db_placeholders($models) . ')',  $models);
        while(FALSE !== $nid = db_result($result)){
            $related[] = $nid;
        }
        db_query('DELETE FROM {uc_upsell_products} WHERE nid = %d', $master_nid);
        db_query(
          'INSERT IGNORE INTO {uc_upsell_products} (nid, related_products) VALUES ("%s", "%s")',
          $master_nid,
          serialize($related)
        );
      }
    variable_del(session_id());
    }
  }
}