<?php

/**
 * Description of converters
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
require_once drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . 'common' . DIRECTORY_SEPARATOR . 'classes.inc';

class importNumber extends convertBase {

}

class importText extends convertBase {

}

class importCCKHTML extends nodeConvertMethod {

  public $format = 1;

  public function convert($data) {
    if ( !is_array($data) ){
      $data = array( $data );
    }
    foreach( $data as &$item ){
      $item = array(
        'value' => $item,
        'format' => $this->format,
      );
    }
    return $data;
  }

}

class importCCKArray extends nodeConvertMethod {

  protected function getItem($item) {
    return $item;
  }

  public function convert($data) {
    if ( empty($data) ){
      return array( "value" => null );
    }elseif ( !is_array($data) ){
      $data = explode(';', $data);
    }
    foreach( $data as &$item ){
      $item = array( "value" => $this->getItem($item) );
    }
    return array_filter($data, create_function('$item', 'return !empty($item["value"]);'));
  }

}

class importCCKTaxonomy extends importCCKArray {

  public $vocabulary = false;
  public $createNewTerms = false;

  /**
   * Search for taxonomy term
   * @staticvar array $terms
   * @param string $name Term name for search
   * @param integer $vocabulary Vocabulary which search in
   * @param boolean $create Create term if found nothing
   * @return integer|boolan Found term id or false.
   */
  private static function getTermByName($name, $vocabulary, $create = false) {
    static $terms;
    if ( !isset($terms) ){
      $terms = array( );
    }
//    watchdog('productImporter', 'Search for term %term', array( '%term' => $name ), WATCHDOG_DEBUG);

    if ( !isset($terms[$vocabulary]) ){
      $terms[$vocabulary] = array( );
      $query = 'SELECT * FROM {term_data} WHERE vid=%d';
      $result = db_query($query, (int) $vocabulary);
      while( $item = db_fetch_object($result) ){
        $terms[$vocabulary][$item->tid] = strtolower($item->name);
      }
//      watchdog('productImporter', 'Vocabulary %id load with %cnt objects', array( '%id' => $vocabulary, '%cnt' => count($terms[$vocabulary]) ), WATCHDOG_DEBUG);
    }
    if ( false !== $key = array_search(strtolower($name), $terms[$vocabulary]) ){
//      watchdog('productImporter', 'Term %term found with id %id', array( '%term' => $name, '%id' => $key ), WATCHDOG_DEBUG);
      return $key;
    }elseif ( $create ){
      //elemend doesn't exist
      $term = array(
        'name' => $name,
        'vid' => $vocabulary,
      );
      taxonomy_save_term($term);
      $terms[$vocabulary][$term['tid']] = $term['name'];
//      watchdog('productImporter', 'Term %term created with id %id', array( '%term' => $name, '%id' => $term['tid'] ), WATCHDOG_DEBUG);
      return $term['tid'];
    }else{
//      watchdog('productImporter', 'Term %term not found', array( '%term' => $name ), WATCHDOG_DEBUG);
      return null;
    }
  }

  protected function getItem($item) {
    if ( $this->vocabulary ){
      return self::getTermByName($item, $this->vocabulary, $this->createNewTerms);
    }else{
      return null;
    }
  }

}

class importCCKFile extends importFiles {

  public function convert($data) {
    $result = parent::convert($data);
    if ( empty($result) ){
      return null;
    }
    array_walk($result, create_function('&$item', '$item = (array)$item;'));
    return $result;
  }

}

class importListValue extends nodeConvertMethod {

  /**
   * Array to search keys in
   * @var array
   */
  public $list = array( );

  public function convert($data) {
    if ( !is_array($this->list) ){
      throw new exception('Parameter is not array.');
    }
    return array_search($data, $this->list);
  }

}

class importCountry extends importListValue {

  private static function getList() {
    static $result;
    if ( !isset($result) ){
      $result = array( );
      $query = db_query("SELECT * FROM {uc_countries}");
      while ( false !== $item = db_fetch_array($query) ){
        $result[$item['country_id']] = $item['country_name'];
      }
    }
    return $result;
  }

  public function __construct($params = array( )) {
    $this->list = self::getList();
    parent::__construct($params);
  }

}

class importState extends importListValue {

  private static function getList() {
    static $result;
    if ( !isset($result) ){
      $result = array( );
      $query = db_query("SELECT * FROM {uc_zones}");
      while ( false !== $item = db_fetch_array($query) ){
        $result[$item['zone_id']] = $item['zone_name'];
      }
    }
    return $result;
  }

  public function __construct($params = array( )) {
    $this->list = self::getList();
    //allow to overwrite list
    parent::__construct($params);
  }

}

class importCCKDate extends nodeConvertMethod {

  public function convert($data) {
    return date('Y-n-d\TH:i:s', strtotime($data));
  }

}

class importFiles extends nodeConvertMethod {

  protected $destination;
  private $additionalItems = array( );

  protected function getItem($item) {
    global $user;
    $item = str_replace(
        array(
          '\\',
          '../',
        ),
        array(
          '/',
          '',
        ),
        $item
    );
    $item = $this->destination . DIRECTORY_SEPARATOR . $item;
    if ( !file_exists($item) ){
      return null;
    }
    if ( is_dir($item) ){
      $dir_files = scandir($item);
//        watchdog('productImport', 'Copy files from directory %dir', array( '%dir' => $item ), WATCHDOG_DEBUG);
      foreach( $dir_files as $dir_file ){
        $dir_file = $item . DIRECTORY_SEPARATOR . $dir_file;
        if ( is_dir($dir_file) ){
          //no recurse
          continue;
        }
//          watchdog('productImport', 'Copy file %name', array( '%name' => $dir_file ), WATCHDOG_DEBUG);
        $source = (object) array(
            'uid' => $user->uid,
            'filepath' => $dir_file,
            'filemime' => file_get_mimetype($dir_file),
            'filesize' => filesize($dir_file),
            'filesource' => $dir_file,
            'status' => FILE_STATUS_TEMPORARY,
            'timestamp' => time(),
            'list' => 1,
            'new' => true,
        );
        if ( file_copy($source) ){
          drupal_write_record('files', $source);
          $this->additionalItems[] = $source;
        }
      }
      return null;
    }elseif ( is_file($item) ){
//        watchdog('productImport', 'Copy file %name', array( '%name' => $item ), WATCHDOG_DEBUG);
      $source = (object) array(
          'uid' => $user->uid,
          'filepath' => $item,
          'filemime' => file_get_mimetype($item),
          'filesize' => filesize($item),
          'filesource' => $item,
          'status' => FILE_STATUS_TEMPORARY,
          'timestamp' => time(),
          'list' => 1,
          'new' => true,
      );
      if ( file_copy($source) ){
        drupal_write_record('files', $source);
        return $source;
      }
    }
  }

  public function convert($data) {
    if ( empty($data) ){
      return null;
    }elseif ( !is_array($data) ){
      $data = explode(';', $data);
    }
    foreach( $data as $value ){
      $value = $this->getItem($value);
      $result[$value->fid] = $value;
    }

    foreach( $this->additionalItems as $value ){
      $result[$value->fid] = $value;
    }
    return $result;
  }

}

/**
 * @todo
 */
class importRelated extends nodeConvertMethod {

  public function convert($data) {
    return explode(';', $data);
  }

}

class importCCKText extends importCCKArray {

}

class importCCKNumber extends importCCKArray {

}

class importGrants extends nodeConvertMethod {

  public function convert($data) {
    return explode(';', $data);
  }

}

class importWeightUnits extends importText{

  public function convert($data) {
    if ( empty($data) ){
      return '';
    }
    $weight_units = array('lb', 'kg', 'oz', 'g');
    if ( !in_array($data, $weight_units) ){
      if ( in_array(strtolower($data), $weight_units)){
        return strtolower($data);
      }
      throw new ConvertException(
        t(
          'Value must contain one of the following: %data',
          array('%data' => implode(', ', $weight_units))
        ),
        0,
        true,
        'warning'
      );
    }
    return parent::convert($data);
  }
}

class importSizeUnits extends importText {
  public function convert($data) {
    if ( empty($data) ){
      return '';
    }
    $length_units = array('in', 'ft', 'cm', 'mm');
    if ( !in_array($data, $length_units) ){
      if ( in_array(strtolower($data), $length_units)){
        return strtolower($data);
      }
      throw new ConvertException(
        t(
          'Value must contain one of the following: %data',
          array('%data' => implode(', ', $length_units))
        ),
        0,
        true,
        'warning'
      );
    }
    return parent::convert($data);
  }
}

/**
 * Converter gets value from database.
 */
class importSqlListValue extends importListValue {

  /**
   * @var string field with the keys for resulting array
   */
  protected $key;
  /**
   * @var string field with the values for resulting array
   */
  protected $value;
  /**
   * @var array additional filters. Represents as "key = value" concatenated
   * with AND operator.
   */
  protected $filter = array();
  /**
   * @var string Table to retreive data.
   */
  protected $table;
  /**
   * @var string SQL-query. Can be used only for complex queries instead
   * filter and table fields. $key and $value still needed
   */
  protected $query;

  /**
   * Init parameters
   * @param type $params
   */
  protected function init($params) {
    parent::init($params);
    if (empty($this->list)){
      $this->list = $this->getList();
    }
  }

  /**
   * Fills list from database
   * @return array
   */
  private function getList() {
    if ( !empty($this->query) ){
      $query = db_query($this->query);
    }else{
      $query = 'SELECT %s, %s FROM {%s} ';
      foreach( $this->filter as $key => $value ){
        $where[] = $key . ' = ' . $value;
      }
      if ( !empty($where) ){
        $query .= 'WHERE ' . implode(' AND ', $where);
      }
      $query = db_query($query, array( $this->key, $this->value, $this->table ));
    }
    $result = array( );
    while( false !== $item = db_fetch_array($query) ){
      $result[$item[$this->key]] = $item[$this->value];
    }
    return $result;
  }

}

/**
 * Users importer
 */
class importUser extends importSqlListValue {

  protected function init($params) {
    $params += array(
      'key'   => 'uid',
      'value' => 'name',
      'table' => 'users',
    );
    parent::init($params);
  }

  /**
   * Wrapper for parent convert method raises convert exceptions
   * @param type $data
   * @throws ConvertException
   */
  public function convert($data) {
    $result = parent::convert($data);
    if ( !$result && $data ) {
      throw new ConvertException(
        t(
          'User %data not found',
          array(
            '%data' => $data,
          )
        )
      );
    }
  }
}
