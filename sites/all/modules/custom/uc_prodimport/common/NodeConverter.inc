<?php

/**
 * Description of NodeConverter
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
abstract class NodeConverter extends loadable {

  protected $datafile = 'field_data.inc';
  protected $variables;
  /**
   *
   * @var fieldImportData|array
   */
  protected $converters;

  public function __construct($params = array( )) {
    parent::__construct($params);
    $this->load();
  }

  public function load($filename = false) {
    if ( !$filename ){
      $filename = $this->datafile;
    }else{
      $this->datafile = $filename;
    }
    $filename = drupal_get_path('module', 'uc_prodimport') . DIRECTORY_SEPARATOR . $filename;
    if ( is_file($filename) ){
      $this->converters = include $filename;
    }
  }

  abstract public function process($node);

}

class NodeImporter extends NodeConverter {

  /**
   *
   * @param stdClass $node Node to work with
   * @param mixed $data Data to process
   * @return stdClass|string
   */
  public function process($node, $data = null) {

    if ( !$node instanceof stdClass ){
      $node = new stdClass();
    }
    foreach( $this->converters as $key => $converter ){
      $converter = new FieldImportData($converter);
      $converter->execute($node, $data[$key]);
    }
    return $node;
  }

}

class NodeExporter extends NodeConverter {

  public function process($node) {
    $result = array();
    foreach( $this->converters as $key => $converter ){
      $converter = new FieldExportData($this, $converter);
      $result[$key] = $converter->execute($node);
    }
    return $result;
  }
}

class NodeTitleExporter extends NodeConverter {

  public function process($node){
    $result = array();
    foreach ( $this->converters as $key => $converter ){
      $result[$key] = $converter['title'];
    }
    return $result;
  }
}