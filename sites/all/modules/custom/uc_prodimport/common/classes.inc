<?php

/**
 * Base classes
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */

/**
 * Class with loadable parameteres
 */
class Loadable {

  public function __construct($params = array( )){
    $this->init($params);
  }

  protected function init($params){
    foreach( $params as $key => $value ){
      if ( property_exists($this, $key) ){
        $this->$key = $value;
      }
    }
  }
}

/**
 * Base class for converters
 */
abstract class nodeConvertMethod extends Loadable {

  /**
   *
   * @var FieldData
   */
  protected $fieldData;

  abstract public function convert($data);
}

abstract class FieldData extends Loadable {

  public $field;
  public $title;
  protected $classprefix;

  /**
   *
   * @var nodeConvertMethod
   */
  protected $convertMethod;
  protected $methodParams = array( );

  public function __construct($params = array( )){
    parent::__construct($params);
    if ( empty($this->convertMethod) ){
      throw new exception('No method defined for field ' . $fieldId);
    }
    $classname = $this->classprefix . $this->convertMethod;
    if ( class_exists($classname) || class_exists($classname = $this->convertMethod)){
      $this->methodParams['fieldData'] = &$this;
      $this->convertMethod = new $classname($this->methodParams);
    }else{
      throw new exception('Method ' . $this->convertMethod . 'doesn\'t exist');
    }
  }

  abstract public function execute(&$node);
}

class convertBase extends nodeConvertMethod {

  public function convert($data){
    return $data;
  }

}

class convertDummy extends nodeConvertMethod {

  public function convert($data){
    return null;
  }

}

class fileUtils {

  static function delTree($dir){
    $files = glob($dir . '*', GLOB_MARK);
    foreach( $files as $file ){
      if ( substr($file, -1) == '/' ) self::delTree($file);
      else unlink($file);
    }
    if ( is_dir($dir) ) rmdir($dir);
  }

}

class ConvertException extends Exception {
  /**
   * Is the exception just a notice and no fatal was happened
   * @var boolean
   */
  private $canContinue;
  
  /**
   * Severeness, can be passed to drupal_set_message as type
   * @var string
   */
  private $severeness;
  
  public function __construct($message, $code = 0, $canContinue = false, $severeness = 'warning') {
    $this->canContinue = $canContinue;
    $this->severeness = $severeness;
    parent::__construct($message, $code);
  }
  
  public function getCanContinue(){
    return $this->canContinue;
  }
  
  public function getSevereness(){
    return $this->severeness;
  }
}

class baseEvent {
  public $sender;
  public function __construct(&$sender){
    $this->sender = $sender;
  }
}

class basePlugin {
  protected $owner;

  public function __construct(&$owner){
    $this->owner = $owner;
  }
}

class nodeEvent extends baseEvent{
  public $node;

  public function __construct(&$sender, &$node){
    $this->node = &$node;
    parent::__construct($sender);
  }
}

class nodeListEvent extends baseEvent {
  public $nodes;

  public function __construct(&$nodeList, &$sender) {
    $this->nodes = $nodeList;
    parent::__construct($sender);
  }
}