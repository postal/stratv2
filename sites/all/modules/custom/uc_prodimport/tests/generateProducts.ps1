function generate-Products($count){
    #headers line from existing csv
    $headersString = "Title,Product description,Technical Information,SKU,List price,Sell Price,Cost,Weight,Weight units,Length,Width,Height,Width / Height / Length units,Package quantity,Default quantity,Category,Brands,Manufacturer,Images,Videos,Specification files,Is the product profiled,Is shippable,Related products SKU,Volume,Volume units,Concentration,Concentration units,Storage temperature,Format,Size,Shelf life,Shelf life units,Storage Considerations,Product considerations,Safety considerations,Certificates,Guaranty,Guaranty units,Warranty,Warranty units,Shipping type,Shippings address: First name,Last Name,Company,Street Address,Street Address (second),City,State / Province,Postal code,Phone,Country,Files,Deal start date,Deal end date,Deal description,Stock,Stock threshold,Is active stock?,Roles granted to view product,Roles granted to view price,Seller,Site SKU,id";
    $headers = $headersString.split(',');

    $data = @();

    for($i=0; $i -lt $count; $i++) {
        Write-Progress -activity "Creating data for csv" -status "Complete $i from $count"  -PercentComplete (($i / $count)  * 100)
        $object = New-Object PSCustomObject | Select-Object $headers;
        $object.Title = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque metus nibh, sodales eu venenatis vitae, euismod non nulla. Suspendisse ut nisi non magna tincidunt sollicitudin eget eu enim. Number $i";
        $object.sku  = "SKU $i";
        $object."List Price"= 5;
        $object.Cost = 1;
        $object."Sell Price" = 6;
        $object.Category = "Audio";
        $object.Brands = "tester";    
        Write-Output $object
    }
}

#generate-Products 10000| Export-CSV "test10k-long-title.csv" -NoTypeInformation
