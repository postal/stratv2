if (Drupal.jsEnabled) {
    Drupal.behaviors.uc_prodimport_export_functon = function () {

        $('.add-list-button').not('.ajax-list-processed').each(function () {
            $(this).bind('click', function () {
                var $btn = $(this);
                var nid = $btn.val();
                url = '/admin/store/products/uc_prodimport/export/set';
                params = {
                    nid:nid,
                    set:$btn.attr('checked')
                };
                if ($(this).hasClass('for-review')) {
                    params['index'] = 'review';
                }
                $.ajax({
                    url:url,
                    type:'GET',
                    data:params,
                    dataType:'json',
                    success:function (data, status) {
                        if (data == 1) {
                            $btn.attr('checked', 'checked');
                        } else {
                            $btn.attr('checked', '');
                        }
                    }
                });
            });
            $(this).addClass('ajax-list-processed');
        });
    }
//add checkbox to header
    Drupal.behaviors.uc_prodimport_export_select_all_function = function () {
        var $header = $('div.view-uc-prodimport-products-list table th.views-field-null');
        if ($('#view-uc-prodimport-products-list-select-all').size() < 1) {
            $header.html('<input type="checkbox" name="view-uc-products-to-export-list-select-all" id="view-uc-prodimport-products-list-select-all">');

            $('#view-uc-prodimport-products-list-select-all').bind('click', function () {
                var checked = $(this).attr('checked');
                var values = new Array();
                var data = {
                    set:checked
                };
                $('div.view-uc-prodimport-products-list table tbody input.add-list-button').each(function () {
                    $(this).attr('checked', checked);
                    values.push($(this).val());
                    if ($(this).hasClass('for-review')) {
                        data['index'] = 'review';
                    }
                });
                data['nid'] = values.join(',');
                $.ajax({
                    url:'/admin/store/products/uc_prodimport/export/set',
                    type:'GET',
                    data:data,
                    dataType:'json',
                    success:function (data, status) {
                    }
                });
            });
        }
    }
}
