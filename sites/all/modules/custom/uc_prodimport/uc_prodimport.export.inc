<?php

/*
 *  Export functions
 */

function uc_prodimport_export() {
  /* @var $view view */
  $view = views_get_view('uc_prodimport_products_list');
  if ( empty($view) ){
    die('View not installed');
    return;
  }
  $view->set_display('export_products');
  $view->display_handler->override_option('path', 'admin/store/products/uc_prodimport');
  $output = '';
  $view->pre_execute();
  $output .= $view->render();
  $output .= drupal_get_form('uc_prodimport_export_form');

  return $output;
}

/**
 * Product export form
 * @param array $form_state
 * @return array form data
 */
function uc_prodimport_export_form($form_state) {
  $form = array( );
  $form['#tree'] = TRUE;

  $form['cancel'] = array(
    '#type' => 'button',
    '#value' => t('Cancel'),
    '#url' => 'admin/store/products/uc_prodimport/cancel',
    '#weight' => -5,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export Selected'),
  );

  $form['export_all'] = array(
    '#type' => 'submit',
    '#all' => 1,
    '#value' => t('Export All'),
  );

  $form['to_import'] = array(
    '#type' => 'button',
    '#value' => t('Advance to Product Import'),
    '#url' => 'admin/store/products/uc_prodimport/import',
  );
  $form['to_review'] = array(
    '#type' => 'button',
    '#value' => t('Advance to Product Review'),
    '#url' => 'admin/store/products/uc_prodimport/review',
  );

  $form['delete_selected'] = array(
    '#type' => 'button',
    '#value' => t('Delete Selected'),
    '#url' => 'admin/store/products/uc_prodimport/mass_delete',
  );

  return $form;
}

function uc_prodimport_export_form_validate($form, &$form_state) {
  if ( $form_state['clicked_button']['#type'] == 'button' && !empty($form_state['clicked_button']['#url']) ){
    drupal_goto($form_state['clicked_button']['#url']);
  }
}

function uc_prodimport_export_form_submit($form, &$form_state) {

  if ( $form_state['clicked_button']['#all'] == 1 ){
    $query = db_query('SELECT nid from {nodes} WHERE status <> 0');
    $values = array( );
    while( false != $item = db_result($query) ){
      $values[] = $item;
    }
  }else{
    $values = _get_selected_products();
    if ( !is_array($values) ){
      drupal_set_message('No Data Selected');
      return;
    }
  }
  $exporter = new BatchProductExporter();
  $exporter->export($values);
  _clean_selected_products();
}

/**
 * Submit procedure exports node.
 * @param array $form
 * @param array $form_state
 */
function uc_prodimport_export_node($form, &$form_state) {
  $values = array($form['nid']['#value']);
  $exporter = new BatchProductExporter();
  $exporter->export($values);
}

function uc_prodimport_download_file($filename){
  $file = file_directory_path() . DIRECTORY_SEPARATOR . $filename;
   if ( file_exists($file)) {
    //todo access control
    $headers = module_invoke_all('file_download', $file);
    if (in_array(-1, $headers)) {
      return drupal_access_denied();
    }
    $headers = array(
        'Content-Type: application/zip',
        'Content-Disposition: attachment; filename="archive.zip"',
        'Content-Transfer-Encoding: binary',
        'Content-Length: ' . filesize($file),
    );
    file_transfer($file, $headers);
    die;
 }
}

function uc_prodimport_cancel_wizard() {
  _clean_selected_products('products_to_export');
  _clean_selected_products('products_to_review');
  drupal_goto('/');
}

function uc_prodimport_delete_wizard($form_state) {

  $back_url = isset($_REQUEST['destination'])
    ? $_REQUEST['destination']
    : referer_uri();

  if ( strpos($back_url, 'review') > -1 ){
    $index = 'products_to_review';
  }else{
    $index = 'products_to_export';
  }
  $values = _get_selected_products(false, $index);

  if ( empty($values) ){
    drupal_set_message('You must select at least one product', 'warning');
    drupal_goto($back_url);
    die;
  }

  $result = db_query(
    'SELECT title FROM {node} WHERE nid IN(' . db_placeholders($values) . ')',
    $values
  );
  $values = array( );
  while( $item = db_result($result) ){
    $values[] = $item;
  }
  $form = array(
    '#redirect' => $back_url,
  );

  $form['session_index'] = array('#type' => 'hidden', '#value' => $index);
  $form['destination'] = array('#type' => 'hidden', '#value' => $back_url);

  $form['list'] = array(
    '#value' => theme('item_list', $values),
  );
  return confirm_form(
      $form, t('Are you sure you want to delete these products?'),
      $back_url, t('This action cannot be undone. The products you haven\'t permission to delete won\'t be deleted.'),
      t('Delete'), t('Cancel')
  );
}

function uc_prodimport_delete_wizard_submit($form, &$form_state) {
  global $user;
  $index = $form_state['values']['session_index'];
  $values = _get_selected_products(false, $index);
  //@todo have the creator permissions to delete created products?
  foreach( $values as $nid ){
    $node = node_load($nid);
    if ( $node
      && (
        ( $node->uid == $user->uid && user_access('delete own products'))
          || (user_access('delete all products'))) ){
      node_delete($nid);
    }
  }
  _clean_selected_products($index);
}

function uc_prodimport_views_api() {
  return array(
    'api' => 2.0,
    'path' => drupal_get_path('module', 'uc_prodimport') . '/views',
  );
}
