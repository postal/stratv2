<?php

/**
 * Description of uc_prodimport_handler_field_review_status
 *
 * @author Yuri 'Jureth' Minin, J.Jureth@gmail.com
 */
class uc_prodimport_handler_field_review_status extends views_handler_field{

  private function values(){
    return array(
      REVIEW_STATUS_NEW => t('Review'),
      REVIEW_STATUS_PUBLISHED => t('Published'),
      REVIEW_STATUS_REVIEW => t('Review'),
      '' => t('Published'),
    );
  }
  
  public function render($values) {
    $items = $this->values();
    $id = $values->{$this->field_alias};
    return $items[$id];
  }
}
