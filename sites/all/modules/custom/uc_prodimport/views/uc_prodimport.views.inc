<?php
/**
 * Implementation of hook_views_handlers
 * @return array
 */
function uc_prodimport_views_handlers(){
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_prodimport') . '/views',
    ),
    'handlers' => array(
      'uc_prodimport_handler_filter_duplicate_sku' => array(
        'parent' => 'views_handler_filter_boolean_operator',
      ),
      'uc_prodimport_handler_field_review_status' => array(
        'parent' => 'views_handler_field',
      ),
      'uc_prodimport_handler_filter_review_status' => array(
        'parent' => 'views_handler_filter',
      ),
    )
  );
}

/**
 * implementation of hook_views_data
 * @return array
 */
function uc_prodimport_views_data() {
  $data = array( );
  $data['uc_imported_products']['table']['group'] = t('Product Imports');
  $data['uc_imported_products']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'nid',
  );
  $data['uc_imported_products']['review_status'] = array(
    'title' => t('Review status'),
    'help' => t('Review status of imported item'),
    'field' => array(
      'handler' => 'uc_prodimport_handler_field_review_status',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'uc_prodimport_handler_filter_review_status',
      'label' => 'Review status',
      'accept_null' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['uc_imported_products']['uid'] = array(
    'title' => t('Creator is current'),
    'help' => t('Filter the products imported by current user'),
    'filter' => array(
      'handler' => 'views_handler_filter_user_current',
    ),
  );
  $data['uc_products']['dup_model'] = array(
    'title' => t('Have duplicate sku'),
    'help' => t('Filter the products with non unique SKU'),
    'filter' => array(
      'handler' => 'uc_prodimport_handler_filter_duplicate_sku',
      'label' => t('Only with duplicate SKU'),
    )
  );
  return $data;
}
