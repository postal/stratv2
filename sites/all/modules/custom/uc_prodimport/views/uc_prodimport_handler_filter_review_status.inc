<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Юрий
 * Date: 20.02.12
 * Time: 15:06
 * To change this template use File | Settings | File Templates.
 */
class uc_prodimport_handler_filter_review_status extends views_handler_filter {

  public function option_values(){
    return array(
      REVIEW_STATUS_NEW => t('New'),
      REVIEW_STATUS_REVIEW => t('Stored for later review'),
      REVIEW_STATUS_PUBLISHED => t('Published'),
    );
  }

  /**
   * Plugin options
   * @return array
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['operator'] = array( 'default' => '=' );
    $options['value']['default'] = 0;
    return $options;
  }

  public function query(){
    $value = $this->getValue();
    /** @var $query views_query */
    $query = &$this->query;
    $this->ensure_my_table();
    $query->ensure_table('uc_imported_products');
    $query->add_where(0,  'uc_imported_products.review_status = %d', $value);
  }

  public function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }
    $options = $this->option_values();
    return $options[$this->value];
  }

  /**
   * Retreive current filter value
   * @return integer
   */
  private function getValue(){
    if ( !empty($this->options['exposed']) && !empty($this->view->exposed_input[$this->options['expose']['identifier']]) ){
      return (int)$this->view->exposed_input[$this->options['expose']['identifier']]['value'];
    }else{
      return (int)$this->options['value'];
    }
  }

  public function value_form(&$form, &$form_state) {
    $options = $this->option_values();
    $form['value'] = array(
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->value,
    );
    //todo
    if ( !empty($form_state['exposed']) ){
      $identifier = $this->options['expose']['identifier'];
      if ( !isset($form_state['input'][$identifier]) ){
        $form_state['input'][$identifier] = $this->value;
      }
    }
  }

  public function exposed_validate(&$form, &$form_state){
    if ( empty($this->options['exposed']) ){
      return;
    }
    $options = $this->option_values();
    if ( !is_array($options)
      || !in_array($form_state['values']['value'], array_keys($options))
    ){
      form_error($form[$this->field]['value'], 'Illegal choice selected');
    }
  }
}
