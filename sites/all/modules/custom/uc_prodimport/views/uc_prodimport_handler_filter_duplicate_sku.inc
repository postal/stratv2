<?php

/**
 * Filter products with duplicate model / SKU.
 * Filter only *with* duplicate SKU, but no without.
 *
 */
class uc_prodimport_handler_filter_duplicate_sku extends views_handler_filter_boolean_operator {

  public function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#type' => 'checkbox',
      '#title' => $this->value_value,
      '#default_value' => $this->value,
    );
  }

  public function query() {
    if ( !empty($this->options['exposed']) 
      && !empty($this->view->exposed_input[$this->options['expose']['identifier']]) 
    ){
      $value = (bool) $this->view->exposed_input[$this->options['expose']['identifier']]['value'];
    }else{
      $value = (bool) $this->options['value'];
    }
    if ( !$value ){
      return; //there is nothing to filter
    }

    /* @var $query views_query */
    $query = &$this->query;
    $this->ensure_my_table();

    $sql = '(SELECT model, count(nid) as cnt FROM {uc_products} GROUP BY model)';
    $join = new views_join();
    $join->construct($sql, 'uc_products', 'model', 'model');
    $query->add_relationship('sku_duplicates_count', $join, 'node');

    $query->add_where(0, 'sku_duplicates_count.cnt > 1');
  }

}
