<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

//$data = array(
//  'terms' => array(),
//  'root_item' => false,
//  'href' => 'data/{tid}',
//  'query_callback' => null,
//  'flat' => true,
//  'id_prefix' => '',
//);
/**
 * Build tree from given data.
 * $parameters is array contains following key-value pairs:
 * * 'terms': Required. Data, returned by taxonomy_get_tree or similar
 * * 'root_menu' Optional. If set, all items will be children to item with this
 * name
 * * 'href': Optional. Template for link, may contain '{tid}' and '{name}' to
 * replace them with term id and term name.
 * * query_callback: Optional. function to call to generate href and query
 * options. It takes current term as argument and must return an indexed array
 * with 'href'(value is a string) and 'query' (value is an array as in url func)
 * keys. You must specify either href or query_callback value, but not both.
 * * 'flat': Optional. Set output as flat list, instead of tree. Not affected on
 * root_item element
 * * 'id_prefix': Required. Prefix for items id.
 *
 * @param array $parameters
 * @return array
 */
function taxonomy_build_tree($parameters) {
  if ( empty($parameters['terms']) ){
    return array();
  }

  $menu = array( );
  $menu_items = array( );
  //Set root element
  if ( !empty($parameters['root_item']) ){
    if ( is_array($parameters['root_item']) ){
      $title = $parameters['root_item']['title'];
      $href = $parameters['root_item']['href'];
    }else{
      $title = $parameters['root_item'];
      $href = '#';
    }
    $root = array(
      'below' => '',
      'link' => array(
        'title' => $title,
        'mlid' => $parameters['id_prefix'] . '-0',
        'href' => $href,
        'has_children' => 1,
        'hidden' => 0,
      ),
    );
    $menu_items[0] = $root;
    $menu[0] = &$menu_items[0];
  }

  foreach( $parameters['terms'] as $term ){
    if ( !$parameters['flat'] ){
      $parent_tid = $term->parents[0];
    }else{
      $parent_tid = 0;
    }
    $tid = $term->tid;
    if ( !empty($parameters['href']) ){
      $href = str_replace(
          array(
            '{tid}',
            '{name}'
          ),
          array(
            $term->tid,
            $term->name,
          ),
          $parameters['href']
      );
      $query = null;
    }elseif ( is_callable($parameters['query_callback']) ){
      $arr = call_user_func($parameters['query_callback'], $term);
      $href = $arr['href'];
      $query = $arr['query'];
      unset($arr);
    }else{
      $href = '#';
      $query = null;
    }
    $menu_item = array(
      'below' => '',
      'link' => array(
        'title' => $term->name,
        'mlid' => $parameters['id_prefix'] . '-' . $tid,
        'href' => $href,
        'localized_options' => array(
          'query' => $query,
        ),
        'hidden' => 0,
        'has_children' => taxonomy_get_children($tid) && !$parameters['flat']
          ? 1
          : 0, /* need optimisation for better performance */
      ),
    );
    $menu_items[$tid] = $menu_item;

    if ( $parent_tid || !empty($root) ){
      $menu_items[$parent_tid]['below'][$tid] = &$menu_items[$tid];
    }else{
      $menu[$tid] = &$menu_items[$tid];
    }
  }
  return $menu;
}
