<?php
/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */


/**
 * Display a plain HTML VIEW ONLY version of the widget
 * with the specified rating
 *
 * @param $rating
 *   The desired rating to display out of 100 (i.e. 80 is 4 out of 5 stars)
 * @param $stars
 *   The total number of stars this rating is out of
 * @param $tag
 *   Allows multiple ratings per node
 * @return
 *   A themed HTML string representing the star widget
 *
 */
function theme_fivestar_filter_static($rating, $prefix = '', $suffix = '',
  $stars = 5, $tag = 'vote') {
  $output = '';
  $output .= '<div class="fivestar-widget-static fivestar-widget-static-' . $tag . ' fivestar-widget-static-' . $stars . ' clear-block">';
  $output .= $prefix;
  if ( empty($stars) ){
    $stars = 5;
  }
  $numeric_rating = $rating / (100 / $stars);
  for( $n = 1; $n <= $stars; $n++ ){
    $star_value = ceil((100 / $stars) * $n);
    $prev_star_value = ceil((100 / $stars) * ($n - 1));
    $zebra = ($n % 2 == 0)
      ? 'even'
      : 'odd';
    $first = $n == 1
      ? ' star-first'
      : '';
    $last = $n == $stars
      ? ' star-last'
      : '';
    $output .= '<div class="star star-' . $n . ' star-' . $zebra . $first . $last . '">';
    if ( $rating < $star_value && $rating > $prev_star_value ){
      $percent = (($rating - $prev_star_value) / ($star_value - $prev_star_value)) * 100;
      $output .= '<span class="on" style="width: ' . $percent . '%">';
    }elseif ( $rating >= $star_value ){
      $output .= '<span class="on">';
    }else{
      $output .= '<span class="off">';
    }
    if ( $n == 1  )$output .= $numeric_rating;
    $output .= '</span></div>';
  }
  $output .= $suffix;
  $output .= '</div>';
  return $output;
}

/**
 * @return array|bool
 */
function _get_fivestar_filter_block() {
  $vid = variable_get('uc_catalog_vid', 0);
  if ( !$vid ){
    return false;
  }
  $voc = taxonomy_vocabulary_load($vid);
  $params = array_fill(0, count($voc->types), '%s');
  $criteria = array(
    'vote_type' => 'percent',
    'content_type' => 'node',
    'function' => 'average',
  );
  $votes = votingapi_select_results($criteria);

  //count nodes for each star
  $f = create_function(
      '$result, $data',
      '
          $result[round((int) $data["value"] / 20, 0, PHP_ROUND_HALF_UP)]++;
          return $result;
        '
  );
  $rating_stats = array_reduce($votes, $f, array_fill(1, 5, 0));
  //some preparations maybe?
  $base_query = _vectur_filters_get_search_query();

  $block_content = '';
  for( $i = 5; $i >= 1; $i-- ){ // >= 20 because we don't need in 0 stars
    $query = $base_query;
//		$query['r_op'] = 'between';
    $query['rating'] = $i;
    $block_content .= theme(
        'fivestar_filter_static',
        $i * 20,
        '<a href="' . url('product/search', array( 'query' => $query )) . '">',
        '</a>'
    );
  }

  $block = array(
    'content' => $block_content,
    'subject' => 'Fivestar filter',
  );
  return $block;
}

