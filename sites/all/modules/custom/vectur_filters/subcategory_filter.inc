<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

function get_subcategory_block() {
  module_load_include('inc', 'vectur_filters', 'menu_builder');
  if ( false == $voc = variable_get('uc_catalog_vid', 0) ){
    return false;
  }

  $tree = taxonomy_get_tree($voc);
  if ( !empty($_GET['cat']) ){
    $cat = (int) $_GET['cat'];
  }else{
    $path_arr = explode('/', parse_url($_GET['q'], PHP_URL_PATH));

    if ( $path_arr[0] == 'catalog' ){
      $cat = $path_arr[1];
    }
  }

  if ( isset($cat) ){
    //index by tid
    foreach( $tree as $item ){
      $sorted_tree[$item->tid] = $item;
    }
    $tree = $sorted_tree;
    unset($sorted_tree);

    if ( isset($tree[$cat]) ){
      $node = $tree[$cat];
    }else{
      return '';
    }

    while( 0 != $newid = $node->parents[0] ){
      $node = $tree[$newid];
    }

    //item now points to item in root catalog
    $tree = array_filter(
        $tree,
        create_function(
          '$item',
          'return $item->parents[0] == ' . $node->tid . ';'
        )
    );
  }

  $tree = taxonomy_build_tree(array(
      'terms' => $tree,
      'query_callback' => '_link_callback',
      'flat' => true,
      'id_prefix' => 'subcategory',
    ));
  return menu_tree_output($tree);
}