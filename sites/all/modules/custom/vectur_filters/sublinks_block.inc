<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

function _block_sublinks_menu() {
  module_load_include('inc', 'vectur_filters', 'menu_builder');

  if ( false == $voc = variable_get('uc_catalog_vid', 0) ){
    return false;
  }

  $current_url = parse_url($_GET['q'], PHP_URL_PATH);
  $path_arr = explode('/', $current_url);

  $tree = taxonomy_get_tree($voc);
  if ( $path_arr[0] == 'catalog' ){
    $cat = $path_arr[1];
  }elseif ( arg(0) == "node" && module_exists("uc_product") && uc_product_is_product(intval(arg(1))) ){
    $node = node_load(intval(arg(1)));
    $cat = $node->field_catalog[0]['value'];
  }

  if ( isset($cat) ){
    //index by tid
    foreach( $tree as $item ){
      $sorted_tree[$item->tid] = $item;
    }
    $tree = $sorted_tree;
    unset($sorted_tree);

    if ( isset($tree[$cat]) ){
      $node = $tree[$cat];
    }else{
      $node = reset($tree);
      $cat = '';
    }

//    while( 0 != $newid = $node->parents[0] ){
//      $node = $tree[$newid];
//    }
    if ( $node->parents[0] ){
      $node = $tree[$node->parents[0]];
    }

    //item now points to item in root catalog
    $tree = array_filter(
        $tree,
        create_function(
          '$item',
          'return $item->parents[0] == ' . $node->tid . ';'
        )
    );
  }else{
    $tree = array( );
    $cat = '';
  }

  $bestsellers = array(
    'below' => '',
    'link' => array(
      'title' => 'Bestsellers',
      'mlid' => 'bestsellers',
      'href' => 'bestsellers/' . $cat,
      'has_children' => 0,
      'hidden' => 0,
    ),
  );
  $deals = array(
    'below' => '',
    'link' => array(
      'title' => 'Today\'s Deals',
      'mlid' => 'deals',
      'href' => 'deals/' . $cat,
      'has_children' => 0,
      'hidden' => 0,
    ),
  );

  $items = taxonomy_build_tree(array(
      'terms' => $tree,
      'query_callback' => '_link_callback',
      'flat' => true,
      'id_prefix' => 'sublinks',
    ));
  $items = array(
    $bestsellers,
    $deals
  ) + $items;

  return array(
    'subject' => '',
    'content' => '<ul class="nice-menu nice-menu-down" id="nice-menu-sublinks">' . theme('nice_menu_build', $items) . "</ul>\n"
  );
}
