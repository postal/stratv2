<?php
/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

/**
 * @todo need to review
 * @return array
 */
function _vectur_filters_get_search_query(){
  $current_path = parse_url($_GET['q'], PHP_URL_PATH);
  $current_path = explode('/', $current_path);
  $search_query = $_GET;
  unset($search_query['q']);

  if ( array_slice($current_path, 0, 2) == array('product', 'search') ){
    return $search_query;
  }elseif ( $current_path[0] == 'catalog' ){
    return array(
      'cat' => (int) $current_path[1]
    );
  }
  return array();
}


function _link_callback($term) {
  $href = 'catalog' . '/' . $term->tid;
  return array(
    'href' => $href,
    'query' => array(),
  );
}

/**
 * @deprecated
 * @param stdClass $term
 * @return array
 */
function _brand_link_callback($term) {
  $href = 'product/search';
  $query = $_GET;
  $query['bid'] = $term->tid;
  return array(
    'href' => $href,
    'query' => $query,
  );
}
