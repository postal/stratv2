<?php

/*
 *  License text is not available
 */

function _vectur_filters_get_vocabulary_ids() {
  $vocabularies = taxonomy_get_vocabularies('product');
  $vocs = array( );
  foreach( $vocabularies as $value ){
    $vocs[$value->vid] = $value->name;
  }
  return $vocs;
}

function vectur_filters_admin_form() {
  $vocs = _vectur_filters_get_vocabulary_ids();
  $form = array( );
  if ( !empty($vocs) ){
    $form['brands_vocabulary'] = array(
      '#type' => 'select',
      '#title' => t('Brands vocabulary'),
      '#default_value' => variable_get('vectur_filters_brands_vid', 0),
      '#options' => $vocs,
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function vectur_filters_admin_form_validate(&$form, &$form_state) {
  $vocs = _vectur_filters_get_vocabulary_ids();
  if ( !isset($vocs[$form_state['values']['brands_vocabulary']]) ){
    form_error($form['brands_vocabulary']);
  }
}

function vectur_filters_admin_form_submit(&$form, &$form_state) {
  variable_set('vectur_filters_brands_vid', $form_state['values']['brands_vocabulary']);
  drupal_set_message(t('Configuration saved'));
}
