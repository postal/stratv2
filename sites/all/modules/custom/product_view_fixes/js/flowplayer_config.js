/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */


function video_block_fullscreen(){
  this.getControls().widgets({
    time: true
  });
}

function video_block_fullscreen_exit(){
  this.getControls().widgets({
    time: false
  });
}