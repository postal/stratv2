<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

/**
 * Return product specification block. For using from hook_block
 * @return array|bool
 */
function _block_specs() {
  $output = '';
  if ( arg(0) == 'node' && is_numeric(arg(1)) ){
    $nid = arg(1);
    $node = node_load($nid);
    $files = $node->field_spec;
  }else{
    return false;
  }
  if ( is_array($files) ){
    $output .= '<div id="product-specs">';
    foreach( $files as $item ){
      $output .= theme('product_spec', $item);
    }
    foreach( $node->files as $item ){
      if ( $item->list == '1' ){
        $output .= theme('product_attachment', $item);
      }
    }
    $output .= '</div>';
  }
  return array(
    'subject' => 'Product Specifications',
    'content' => $output,
  );
}

/**
 * Theme for single product specification file
 * @param array $file File information
 * @return string
 */
function theme_product_spec($file) {
  if ( empty($file) ){
    return '';
  }
  return '<div class="product-spec-file">'
  //@todo check if description is empty
  . '<span class="label">' . $file['data']['description'] . ': </span>'
  . l('download', $file['filepath'])
  . '</div>';
}

function theme_product_attachment($file) {
  if ( empty($file) ){
    return '';
  }
  $description = empty($file->description)
    ? $file->filename
    : $file->description;
  return '<div class="product-spec-file">'
  . l($description, $file->filepath)
  . '</div>';
}

