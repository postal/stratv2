The apachesolr module provides a great replacement for core search, complete with faceted search.

The Apachesolr Ubercart integration module aims to provide a seemeless integration of apachesolr with Ubercart, a drupal commerce package.

**Installation**
Please follow the installation guide for the apachesolr project. Once solr is running, enable the apachesolr_ubercart module to add sku and price fields to the index.

**Features**
Adds the sku(model), sell price, and list price to the solr index
Enables model, list price, sell price, weight, height, length, width facet blocks.
The price blocks can be divided in divisions like from 0 to 20€ (of kg..), from 20 to 40 and this division is configurable per block.

**How to configure?**
When enabling a block one can go to the settings of that specific block (for example the price block) and change the deviation between the prices

