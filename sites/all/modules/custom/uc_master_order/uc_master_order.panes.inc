<?php
/**
 * @file order panes for uc_master_order
 */

/**
 * Implementation of ubercart hook_order_pane
 * @return array
 */
function uc_master_order_order_pane() {
  $panes = array();
  $panes[] = array(
    'id'       => 'generate_suborders',
    'callback' => 'uc_master_order_pane_custom_quotes',
    'title'    => t('Custom quotes'),
    'class'    => 'abs-left',
    'show'     => array(
//      'edit',
      'review'
    ),
    'weight'   => 5,
  );
  $panes[] = array(
    'id'       => 'order_attributes',
    'callback' => 'uc_master_order_pane_adj_attributes',
    'title'    => t('Product attributes'),
    'class'    => 'abs-left',
    'show'     => array(
//      'edit',
      'view',
      'review'
    ),
    'weight'   => 4,
  );
  $panes[] = array(
    'id'       => 'rep',
    'callback' => 'uc_master_order_pane_rep',
    'title'    => t('Order Rep Info'),
    'class'    => 'abs-left',
    'weight'   => 4,
    'show'     => array(
      'view',
      'edit',
      'review',
      'sales',
    ),
  );
  return $panes;
}

function uc_master_order_order_pane_alter($panes) {
  $must_be_in_review = array(
    'customer',
    'products',
    'uc_discounts',
  );
  $must_be_in_sales_review = array(
    'customer',
    'products',
  );
  foreach ($panes as &$pane) {
    if (in_array($pane['id'], $must_be_in_review)) {
      $pane['show'][] = 'review';
    }
    if (in_array($pane['id'], $must_be_in_sales_review)) {
      $pane['show'][] = 'sales';
    }
    //tickets
    if ($pane['id'] == 'issue_log') {
      $pane['callback'] = 'uc_master_order_pane_tickets';
    }
  }
}

/**
 * Order pange callback to order attributes "sales support" and "demo request"
 * @param string $op
 * @param mixed  $arg
 * @return array|null|string
 */
function uc_master_order_pane_adj_attributes($op, $arg) {
  switch ($op) {
    case 'view':
      $attributes = array();

      $arg->demo_request OR $attributes[] = t('Demo request');
      $arg->sales_support OR $attributes[] = t('Sales support');
      $arg->is_bundled OR $attributes[] = t('Bundled');
      return theme('item_list', $attributes);
    case 'edit-form':
//      $form = array();
//      $form['order_attributes']['demo'] = array(
//        '#type'          => 'checkbox',
//        '#title'         => t('Demo request'),
//        '#default_value' => $arg->demo_request,
//      );
//      $form['order_attributes']['support'] = array(
//        '#type'          => 'checkbox',
//        '#title'         => t('Sales support'),
//        '#default_value' => $arg->sales_support,
//      );
//      $form['order_attributes']['order_attributes_processed'] = array(
//        '#type'          => 'hidden',
//        '#default_value' => $arg->data['order_attributes_processed'],
//      );
//      return $form;
      break;
    case 'edit-theme':
      return drupal_render($arg['order_attributes']);
      break;
    case 'edit-process':
      $changes = array();
      //@todo I don't like this algorithm

      if (!$arg['order_attributes_processed']) {
        $changes['demo_request'] = (int) $arg['demo'];
        $changes['sales_support'] = (int) $arg['support'];
        $changes['order_attributes_processed'] = 1;
        if ($arg['demo']) {
          $changes['order_status'] = uc_order_state_default('demo');
        }
        elseif ($arg['support']) {
          $changes['order_status'] = uc_order_state_default('sales');
        }
      }
      return $changes;
  }
}

/**
 * Order pane callback to suborders generator
 * @param string $op
 * @param mixed  $arg
 * @return array|null|string
 */
function uc_master_order_pane_custom_quotes($op, $arg) {
  switch ($op) {
    case 'edit-form':
      $form = array();
      if (!empty($arg->data['suborders'])) {
        $sub_orders = array_map(create_function('$item', 'return l("Order $item", "admin/store/orders/$item");'),
          $arg->data['suborders']);
        $form['generate_suborders']['suborders'] = array(
          '#prefix' => t('Existing suborders:'),
          '#value'  => theme('item_list', $sub_orders),
        );
      }
      elseif (!empty($arg->master_order_id)) {
        $form['generate_suborders']['master'] = array(
          '#value' => t(
            "Master order: !name",
            array(
              "!name" => l("Order " . $arg->master_order_id, "admin/store/orders/" . $arg->master_order_id)
            )
          )
        );
      }
      else {
        $form['generate_suborders']['generate_btn'] = array(
          '#type'  => 'submit',
          '#value' => t('Generate custom quotes'),
        );
      }
      return $form;
    case 'edit-theme':
      return drupal_render($arg['generate_suborders']);
    case 'edit-ops':
      return array( t('Generate custom quotes') );
    case t('Generate custom quotes'):
      $order = uc_order_load($arg['order_id']);
      uc_master_order_create_sub_orders($order);
      break;
  }
}


/**
 * order_pane callback. Adds "Order Rep" field to the edit order form
 * @param string $op
 * @param mixed  $arg1
 * @return array|bool|null|string
 */
function uc_master_order_pane_rep($op, $arg1) {
  switch ($op) {
    case 'view':
      $user = user_load($arg1->rep_id);
      $content = theme('uc_master_order_text_with_title', t('Current Rep:'), $user->name);
      $shared = uc_master_order_get_reps($arg1);
      if (!empty($shared)) {
        $content .= theme(
          'uc_master_order_text_with_title',
          t('Reps shared with:'),
          theme_item_list($shared)
        );
      }
      return $content;
    case 'edit-form':
      $shared = uc_master_order_get_reps($arg1);
      $form = array();
      $user = user_load($arg1->rep_id);
      $form['rep']['current-rep'] = array(
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#value'  => t('<span class="label">Current rep:</span> %name', array( '%name' => $user->name )),
      );
      if (!empty($shared)) {
        $form['rep']['shared-rep-list'] = array(
          '#prefix' => '<div>',
          '#suffix' => '</div>',
          '#value'  => t('<span class="label">Already shared with:</span> %names',
            array( '%names' => implode(', ', $shared) )),
        );
      }
      $form['rep']['order-rep'] = array(
        '#type'              => 'textfield',
        '#title'             => t('Order Rep Name'),
        '#default_value'     => '',
        '#autocomplete_path' => 'uc_master_order/user_autocomplete'
      );
      $form['rep']['share-rep'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Share order instead transfer'),
        '#default_value' => 0,
      );

      return $form;
    case 'edit-theme':
      return drupal_render($arg1['rep']);
    case 'edit-process':
      $user = user_load(array( 'name' => $arg1['order-rep'] ));
      if ($user) {
        //validation section?
        $roles = array_intersect($user->roles, user_roles(TRUE, 'act as seller'));
        if (empty($roles)) {
          return FALSE;
        }
        if ($arg1['share-rep']) {
          $order = uc_order_load($arg1['order_id']);
          $order->reps[] = (int) $user->uid;
          $changes['reps'] = $order->reps;
        }
        else {
          $changes['rep_id'] = (int) $user->uid;
        }
        return $changes;
      }
      else {
        drupal_set_message('User not found', 'error');
        return FALSE;
      }
      break;
  }
}

/**
 *
 * Suborders generator
 * @param stdClass $order master order
 * @return array list of suborders
 */
function uc_master_order_create_sub_orders($order) {
  //collectiong products
  $nids = array_map(create_function('$item', 'return (int)$item->nid;'), $order->products);
  $result = db_query(
    'SELECT nid, uid FROM {node} WHERE nid in (' . db_placeholders($nids) . ')',
    $nids
  );
  $uids = array();
  while (FALSE !== $item = db_fetch_object($result)) {
    $uids[$item->nid] = $item->uid;
  }
  $sorted_products = array();
  foreach ($order->products as $key => $product) {
    if (!is_array($product->data['attributes']['Ordering attributes'])) {
      $product->data['attributes']['Ordering attributes'] = array();
    }

    $attrs = array_intersect(
      $product->data['attributes']['Ordering attributes'],
      array(
        t('Demo request'),
        t('Sales Support')
      )
    );
    if (empty($attrs)) {
      if (in_array(t('Bundled Product'), $product->data['attributes']['Ordering attributes'])) {
        $sorted_products[$uids[$product->nid]]['bundled'][] = $product;
      }
      else {
        $sorted_products[$uids[$product->nid]]['normal'][] = $product;
      }
      unset($order->products[$key]);
    }
    else {
      //nothing?
    }
  }
  $new_orders = array();
  foreach ($sorted_products as $uid => $types) {
    foreach ($types as $products) { //bundled and other
      //I need to clone order to new order... but how?
      $blank_order = uc_order_new($uid);
      $new_order = clone $order;
      $new_order->order_id = $blank_order->order_id;
      $new_order->primary_email = $order->primary_email;
      $new_order->rep_id = $order->rep_id;
      $new_order->created = time();
      $new_order->modified = time();
      unset($new_order->data['suborders']);

      $new_order->order_status = $order->order_status; //todo orly?
      $new_order->master_order_id = $order->order_id;
      $new_order->products = $products;
      uc_order_save($new_order);
      ca_pull_trigger('uc_master_order_suborder_created', $order, $new_order);
      $new_orders[] = &$new_order;
      $order->data['suborders'][] = $new_order->order_id;
    }
  }
  uc_order_save($order);

  return $new_orders;
}

function uc_master_order_pane_tickets($op, $order) {
  if (!module_exists('uc_ticket')) {
    return;
  }
  switch ($op) {
    case 'view':
      $output = '';
      $links = array();
      $notes = array();
      $SQL = "SELECT t.created, t.title, t.ticket_id, t.status, d.body
      FROM {uc_ticket_tickets} t
      LEFT JOIN {uc_ticket_tickets_data} d ON t.ticket_id = d.ticket_id
      WHERE order_id = %d ORDER BY d.delta ASC, t.created DESC";
      $result = db_query($SQL, $order->order_id);
      while ($line = db_fetch_object($result)) {
        $links[$line->ticket_id] = date('m/d/Y g:i A', $line->created) . ' - ' . l(($line->title)
            ? $line->title
            : t('No Title'),
          'admin/store/ticket/' . $line->ticket_id) . ' (' . uc_ticket_get_status_name($line->status) . ')';
        $notes[$line->ticket_id][] = $line->body;
      }
      foreach ($links as $id => &$link) {
        $link .= theme_item_list($notes[$id]);
      }

      $links[] = l(t('Click Here'),
        'admin/store/ticket/create/' . $order->order_id) . ' to create a new ticket for this order.';
      $output .= theme_item_list($links);
      return $output;
  }
}

/**
 * Returns reps shared this order to
 * @param $order
 * @return array
 */
function uc_master_order_get_reps($order) {
  $users = array();
  if (!empty($order->reps)) {
    $query = 'SELECT uid, name FROM {users} WHERE uid IN (' . db_placeholders($order->reps) . ')';
    $result = db_query($query, $order->reps);
    while (FALSE !== $item = db_fetch_object($result)) {
      $users[$item->uid] = $item->name;
    }
  }
  return $users;
}
