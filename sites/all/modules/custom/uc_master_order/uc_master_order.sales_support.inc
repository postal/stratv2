<?php
/**
 * @file
 */

function uc_master_order_sales_support_review_form($form_state, $order) {

  //$order = uc_order_load($_SESSION['cart_order']);
  if (uc_order_status_data($order->order_status, 'state') != 'sales') {
    return FALSE;
  }
  return _uc_master_order_review_form(
    $form_state,
    array(
      'order'           => $order,
      'form_id'         => 'uc-order-edit-form',
      'pane_view'       => 'sales',
      'submit'          => array(
        'uc_master_order_review_order_form_submit',
        'uc_master_order_sales_support_review_form_submit'
      ),
      'submit_complete' => array( 'uc_master_order_sales_support_review_form_complete' ),
    )
  );
}

/**
 * returns common form data for reviewing order
 * @param       $form_state
 * @param array $settings
 * @return array
 */
function _uc_master_order_review_form($form_state, $settings) {
  //
  $order = $settings['order'];

  $form['#id'] = $settings['form_id'];
  $uc_order_path = drupal_get_path('module', 'uc_order');
  drupal_set_title('Review order ' . $order->order_id);

  // @see uc_order_init()
  if (!(arg(0) == 'admin' && arg(1) == 'store' && arg(2) == 'orders' && is_numeric(arg(3)) && arg(4) == 'edit')) {
    drupal_add_js(array(
      'ucURL' => array(
        'adminOrders' => url('admin/store/orders/'),
      ),
    ), 'setting');
    drupal_add_css($uc_order_path . DIRECTORY_SEPARATOR . 'uc_order.css');
    drupal_add_js($uc_order_path . DIRECTORY_SEPARATOR . 'uc_order.js');
  }
  //

  $form['order_id'] = array(
    '#type'  => 'hidden',
    '#value' => $order->order_id
  );
  $form['order_uid'] = array(
    '#type'  => 'hidden',
    '#value' => $order->uid
  );
  $form['#validate'][] = 'uc_order_edit_form_validate';
  $form['#validate'] = array_merge($form['#validate'], (array) $settings['validate']);
  $form['#submit'][] = 'uc_order_edit_form_submit';
  $form['#submit'] = array_merge($form['#submit'], $settings['submit']);

  $modified = isset($form_state['post']['order_modified'])
    ? $form_state['post']['order_modified']
    : time();
  $form['order_modified'] = array(
    '#type'  => 'hidden',
    '#value' => $modified
  );

  $panes = _order_pane_list($settings['pane_view']);

  foreach ($panes as $pane) {
    if (in_array($settings['pane_view'], $pane['show']) &&
      variable_get('uc_order_pane_' . $pane['id'] . '_show_edit', $pane['enabled'])
    ) {
      $func = $pane['callback'];
      if (function_exists($func) && ($contents = $func('edit-form', $order)) != NULL) {
        $form = array_merge($form, $contents);
      }
    }
  }

  $form['submit-changes'] = array(
    '#type'       => 'submit',
    '#value'      => t('Submit changes'),
    '#attributes' => array( 'class' => 'save-button' ),
  );

  if (!empty($settings['submit_complete'])) {
    $form['complete-review'] = array(
      '#type'       => 'submit',
      '#value'      => t('Complete review'),
      '#attributes' => array( 'class' => 'save-button' ),
      '#submit'     => array_merge($form['#submit'], (array) $settings['submit_complete']),
    );
  }
  return $form;
}

function uc_master_order_sales_support_review_form_submit($form, &$form_state) {

}

function uc_master_order_sales_support_review_form_complete($form, &$form_state) {
  unset($_SESSION['do_review']);
  $_SESSION['do_complete'] = TRUE;
  $_SESSION['cart_order'] = $form_state['values']['order_id'];
  drupal_goto('cart/checkout/complete');

}
