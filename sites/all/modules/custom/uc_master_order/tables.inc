<?php
/**
 * @file
 */

class table_uc_order_reps {

  public static function instance() {
    static $instance;
    if (!$instance) {
      $instance = new self;
    }
    return $instance;
  }

  public function getRepsByOrder($order_id) {
    if (0 == (int) $order_id) {
      throw new ErrorException('Parameter $order_id is invalid');
    }
    $result = db_query('SELECT uid FROM {uc_order_reps} WHERE order_id = %d', (int)$order_id);
    $data = array();
    while (FALSE !== $item = db_fetch_object($result)) {
      $data[] = $item->uid;
    }
    return $data;
  }

  public function setRepsByOrder($order_id, $reps) {
    if (!is_array($reps)) {
      throw new ErrorException('Parameter $reps is not an array');
    }
    if (0 == (int) $order_id) {
      throw new ErrorException('Parameter $order_id is invalid');
    }
    $old_reps = $this->getRepsByOrder($order_id);
    $diff = (array_diff($reps, $old_reps) + array_diff($old_reps, $reps));
    //if they are differ
    //todo we must delete and insert only needed uids
    if (!empty($diff)) {
      db_query('DELETE FROM {uc_order_reps} WHERE order_id = %d');
      $sql = array();
      foreach ($reps as $rep) {
        if ((int) $rep) {
          $sql[] = sprintf('(%d, %d)', (int) $order_id, (int) $rep);
        }
      }
      if (!empty($sql)) {
        $sql = 'INSERT INTO {uc_order_reps} (order_id, uid) VALUES ' . implode(',', $sql);
        db_query($sql);
      }
    }
  }
}