<?php
/**
 * @file
 */

function uc_master_order_ca_trigger() {
  $triggers['uc_master_order_suborder_created'] = array(
    '#title'     => t('Suborder has created'),
    '#category'  => t('Order'),
    '#arguments' => array(
      'master_order' => array(
        '#entity' => 'uc_order',
        '#title'  => t('Master order'),
      ),
      'suborder'     => array(
        '#entity' => 'uc_order',
        '#title'  => t('Suborder'),
      ),
    ),
  );
  return $triggers;
}

function uc_master_order_ca_predicate(){
  $predicates = array();
  $predicates['sent_mail_to_sales_support'] = array(

  );
  return $predicates;
}
