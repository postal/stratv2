/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
$(document).ready(function(){
    $("#product-images-container .more-product-images a").mouseover(function(){
      $("#product-images-container .main-product-image img").attr('src',$(this).attr('href'));
  });
});