/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
if ( Drupal.jsEnabled ){
  Drupal.behaviors.solrSearchAutoSubmit = function(){
    var $form = $("form#solr-filters-form-top");
    if ( $form.length > 0) {
      $form.find("select").change(function(){ $form.submit(); });
      $form.find("input").change(function(){ $form.submit(); });
    }
  }
}