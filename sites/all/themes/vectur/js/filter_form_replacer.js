/* 
 *  License text is not available
 */

if ( Drupal.jsEnabled ){
  Drupal.behaviors.filter_text_replacer = function(){
    
    if ( $('#content-content #solr-filters-form-top').size() > 0 ){      
      $('#content-content #solr-filters-form-top').appendTo('#breadcrumbs-inner');
      height = $("#solr-filters-form-top").height();
      offset = parseInt($("div#breadcrumbs-inner").css("background-position-y"))+height;
      $("div#breadcrumbs-inner").css("background-position-y", offset);
    }
  }
}
