/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
if ( Drupal.jsEnabled ){
  Drupal.behaviors.vecturFiltersSlider = function() {
    $("#sorting-form select").not('.processed').bind('change', function(){$("#sorting-form").submit();});
    $("#sorting-form select").not('.processed').addClass('processed');
  }
}
