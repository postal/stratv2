<?php
// $Id: page.tpl.php 7151 2010-04-20 23:43:28Z jeremy $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $setting_styles; ?>
  <!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
  <?php print $local_styles; ?>
  <?php print $scripts; ?>
</head>

<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
  <div id="page" class="page">
    <div id="page-inner" class="page-inner">
      <div id="skip">
        <a href="#main-content-area"><?php print t('Skip to Main Content Area'); ?></a>
      </div>

      <!-- header-group row: width = grid_width -->
      <div id="header-group-wrapper" class="header-group-wrapper full-width">
        <div id="header-group" class="header-group row <?php //print $grid_width; ?>">
          <div id="header-group-inner" class="header-group-inner inner clearfix">
            <?php if ($secondary_links || $header_top): ?>
            <div id="header-top-secondary-menu-wrapper" class="header-top-secondary-menu-wrapper clearfix">
              <?php if ($header_top): ?>
              <div id="header-top-region" class="header-top row <?php //print $grid_width; ?>">
                <?php print $header_top; ?>
              </div><!-- /header-top-region -->
              <?php endif; ?>
              <?php print theme('grid_block', theme('links', $secondary_links), 'secondary-menu'); ?>
            </div><!-- /header-top-secondary-menu-wrapper -->
            <?php endif; ?>

            <div id="site-info-wrapper" class="site-info-wrapper clearfix">
              <?php if ($logo || $site_name || $site_slogan): ?>
              <div id="header-site-info" class="header-site-info block">
                <div id="header-site-info-inner" class="header-site-info-inner inner">
                  <?php if ($logo): ?>
                  <div id="logo">
                    <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
                  </div>
                  <?php endif; ?>
                  <?php if ($site_name || $site_slogan): ?>
                  <div id="site-name-wrapper" class="clearfix">
                    <?php if ($site_name): ?>
                    <span id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></span>
                    <?php endif; ?>
                    <?php if ($site_slogan): ?>
                    <span id="slogan"><?php print $site_slogan; ?></span>
                    <?php endif; ?>
                  </div><!-- /site-name-wrapper -->
                  <?php endif; ?>
                </div><!-- /header-site-info-inner -->
              </div><!-- /header-site-info -->
              <?php endif; ?>
              <?php print $header; ?>
            </div><!-- /site-info-wrapper -->

            <?php if ($primary_links_tree): ?>
            <div id="primary-menu-wrapper" class="primary-menu-wrapper clearfix">
              <?php if ( $mainbar ){
	      				print theme('grid_block', $primary_links_tree . $mainbar, 'primary-menu');
              } else {
				        print theme('grid_block', $primary_links_tree . theme('grid_block', $search_box, 'search-box'), 'primary-menu');
              }
              //if ($mainbar) print $mainbar; ?>

              <div class="corner-bottom-right corner"></div><div class="corner-bottom-left corner"></div>
            </div><!-- /primary-menu-wrapper -->
            <?php endif; ?>
          </div><!-- /header-group-inner -->
        </div><!-- /header-group -->
      </div><!-- /header-group-wrapper -->

      <!-- preface-top row: width = grid_width -->
      <?php print theme('grid_row', $preface_top, 'preface-top', 'full-width', ''); ?>

      <!-- main row: width = grid_width -->
      <div id="main-wrapper" class="main-wrapper full-width">
        <div id="main" class="main row">
          <div id="main-inner" class="main-inner inner clearfix">
            <?php if ($sidebar_first) print theme('grid_row', $sidebar_first, 'sidebar-first', 'nested', $sidebar_first_width); ?>

            <!-- main group: width = grid_width - sidebar_first_width -->
            <div id="main-group" class="main-group row <?php if ($sidebar_first) echo 'with-left-margin'?>">
              <div id="main-group-inner" class="main-group-inner inner">
                <?php print theme('grid_row', $preface_bottom, 'preface-bottom', ''); ?>

                <div id="main-content" class="main-content row">
                  <div id="main-content-inner" class="main-content-inner inner">
                    <?php if ($sidebar_last) print theme('grid_row', $sidebar_last, 'sidebar-last', 'nested', $sidebar_last_width); ?>
                    <!-- content group: width = grid_width - (sidebar_first_width + sidebar_last_width) -->
                    <div id="content-group" class="content-group row <?php if ($sidebar_last) echo 'with-right-margin'?>">
                      <div id="content-group-inner" class="content-group-inner inner">
                        <?php print theme('grid_block', $breadcrumb, 'breadcrumbs'); ?>

                        <?php if ($content_top || $help || $messages): ?>
                        <div id="content-top" class="content-top row nested">
                          <div id="content-top-inner" class="content-top-inner inner">
                            <?php print theme('grid_block', $help, 'content-help'); ?>
                            <?php print theme('grid_block', $messages, 'content-messages'); ?>
                            <?php print $content_top; ?>
                          </div><!-- /content-top-inner -->
                        </div><!-- /content-top -->
                        <?php endif; ?>
                	    <?php //print theme('grid_block', $search_box, 'search-box'); ?>

                        <div id="content-region" class="content-region row nested">
                          <div id="content-region-inner" class="content-region-inner inner">
                            <a name="main-content-area" id="main-content-area"></a>
                            <div id="content-inner" class="content-inner block">
                              <div id="content-inner-inner" class="content-inner-inner inner">
                                <?php if ($title): ?>
                                <h1 class="title"><?php print $title; ?></h1>
                                <?php endif; ?>
                                <?php print theme('grid_block', $tabs, 'content-tabs'); ?>
                                <?php if ($content): ?>
                                <div id="content-content" class="content-content">
                                  <?php print $content; ?>
                                  <?php print $feed_icons; ?>
                                </div><!-- /content-content -->
                                <?php endif; ?>
                              </div><!-- /content-inner-inner -->
                            </div><!-- /content-inner -->
                          </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                        <?php print theme('grid_row', $content_bottom, 'content-bottom', 'nested'); ?>
                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->

                  </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                <?php print theme('grid_row', $postscript_top, 'postscript-top', 'nested'); ?>
              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div><!-- /main-wrapper -->

      <!-- postscript-bottom row: width = grid_width -->
      <?php print theme('grid_row', $postscript_bottom, 'postscript-bottom', 'full-width', ''); ?>
    </div><!-- /page-inner -->
    <div id="pre-footer">&nbsp;</div>
  </div><!-- /page -->
  <!-- footer row: width = grid_width -->
  <?php print theme('grid_row', $footer, 'footer', 'full-width', ''); ?>

  <!-- footer-message row: width = grid_width -->
  <div id="footer-message-wrapper" class="footer-message-wrapper full-width">
    <div id="footer-message" class="footer-message row <?php //print $grid_width; ?>">
      <div id="footer-message-inner" class="footer-message-inner inner clearfix">
        <?php print theme('grid_block', $footer_message, 'footer-message-text'); ?>
      </div><!-- /footer-message-inner -->
    </div><!-- /footer-message -->
  </div><!-- /footer-message-wrapper -->
  <?php print $closure; ?>
</body>
</html>
