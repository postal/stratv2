<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

function vectur_preprocess_node(&$vars) {
  $criteria = array(
    'content_id' => $vars['nid']
  );

  if ( module_exists('uc_product') && uc_product_is_product($vars) && $vars['template_files'][0] == 'node-product' ){
    $node = node_build_content(node_load($vars['nid']));
    $vars['fivestar_widget'] = drupal_render($node->content['fivestar_widget']);
    $vars['description'] = drupal_render($node->content['body']);
    $vars['product_details'] = drupal_render($node->content['product_details']);
    $vars['technical_details'] = drupal_render($node->content['technical_details']);
    if ( $node->field_deal_active[0] != 'off' ){
      $vars['product_deals'] = drupal_render($node->content['group_deals']['group']['field_deal_description']);
      $vars['product_deals_dates'] = drupal_render($node->content['group_deals']['group']['field_promotion_dates']);
    }
    //rendering upsell block
    $block = module_invoke('uc_upsell_jcarousel_block', 'block', 'view', 0);
    $vars['related_products'] = $block['content'];
  }
}

function vectur_preprocess_search_block_form(&$vars) {
  // Modify elements of the search form
  $vars['form']['search_block_form']['#title'] = ''; // t('Search');
  // Set a default value for the search box
  $vars['form']['search_block_form']['#value'] = '';

  // Add a custom class to the search box
  $vars['form']['search_block_form']['#attributes'] = array( 'class' => t('cleardefault') );
  $vars['form']['search_block_form']['#size'] = 40;

  // Change the text on the submit button
  $vars['form']['submit']['#value'] = t('Go');
  $vars['form']['submit']['#weight'] = -1;

  array_walk($vars['form'], create_function('&$item', 'if (is_array($item)) unset($item["#printed"]);'));

  // Collect all form elements to make it easier to print the whole form.
  $vars['search_form'] = drupal_render($vars['form']);
}

function vectur_preprocess_page(&$vars) {
  module_load_include('inc', 'vectur_filters', 'menu_builder');
  $f = create_function(
      '$term',
      '
        return array(
          "href" => "catalog/" . $term->tid,
          "query" => array(
          ),
        );
      '
  );

  $catalog = taxonomy_build_tree(array(
      'terms' => taxonomy_get_tree(variable_get('uc_catalog_vid', 0)),
      'query_callback' => $f,
      'flat' => false,
      'root_item' => array(
        'title' => 'Catalog',
        'href' => 'catalog',
      ),
      'id_prefix' => 'catalog',
    ));
  $catalog_menu = menu_tree_output($catalog);
  $catalog_menu = preg_replace('/<ul class="menu/i', '<ul class="menu sf-menu', $catalog_menu, 1);

  $cart = l(
      '<img src="/' . drupal_get_path('theme', 'vectur') . '/images/cart.png">',
      'cart',
      array(
        'html' => true,
        'attributes' => array(
          'id' => 'cart-menu-link'
        ),
      )
  );
  $vars['primary_links_tree'] = <<<END
    <div id="catalog_container">{$catalog_menu}</div>
    <div id="primary_menu_contaier">
      <ul class="menu sf-menu"><li>{$cart}</li></ul>
      {$vars['primary_links_tree']}
    </div>
END;
}

function vectur_theme($existing, $type, $theme, $path) {
  return array(
    'theme_uc_product_image' => array(
      'function' => 'vectur_uc_product_image',
      'arguments' => array( 'images' => array( ), 'teaser' => 0, 'page' => 0 ),
    )
  );
}

/**
 * Formats a product's images with imagecache and an image widget
 * (Colorbox, Thickbox, or Lightbox2).
 *
 * @ingroup themeable
 */
function vectur_uc_product_image($images, $teaser = 0, $page = 0) {
  static $rel_count = 0;
  // Get the current product image widget.
  $image_widget = uc_product_get_image_widget();

  $first = reset($images);

  $output = '<div class="product-image"><div class="main-product-image">';
  $output .= '<a href="' . imagecache_create_url('product', $first['filepath']) . '" title="' . $first['data']['title'] . '"';
  if ( $image_widget ){
    $image_widget_func = $image_widget['callback'];
    $output .= $image_widget_func($rel_count);
  }
  $output .= '>';
  $output .= theme('imagecache', 'product', $first['filepath'], $first['data']['alt'], $first['data']['title']);
  $output .= '</a></div><div class="more-product-images">';
  foreach( $images as $thumbnail ){
    // Node preview adds extra values to $images that aren't files.
    if ( !is_array($thumbnail) || empty($thumbnail['filepath']) ){
      continue;
    }
    $output .= '<a href="' . imagecache_create_url('product', $thumbnail['filepath']) . '" title="' . $thumbnail['data']['title'] . '"';
    if ( $image_widget ){
      $output .= $image_widget_func($rel_count);
    }
    $output .= '>';
    $output .= theme('imagecache', 'uc_thumbnail', $thumbnail['filepath'], $thumbnail['data']['alt'], $thumbnail['data']['title']);
    $output .= '</a>';
  }
  $output .= '</div></div>';
  $rel_count++;

  return $output;
}

function vectur_preprocess_search_theme_form(&$vars) {
//  kpr($vars);
  unset($vars['form']['refine']);
//  $vars['form']['refine']['#options']['catalog'] = t('Catalog');

  array_walk($vars['form'], create_function('&$item', 'if (is_array($item)) unset($item["#printed"]);'));
  $vars['search']['refine'] = drupal_render($vars['form']['refine']);
  $vars['search_form'] = drupal_render($vars['form']);
}

function vectur_preprocess_search_result(&$vars) {
  $title = substr($vars['title'], 0, 55);
  if ( strlen($title) == 55 ){//if result is not a full string, can be if title less than 55 symbols
    $title = substr($title, 0, strrpos($title, ' ')) . ' ...';
    $vars['title'] = $title;
  }
  if ( $vars['info_split']['type'] == 'Product' ){
    $node = node_load($vars['result']['node']->nid);

    if ( $vars['result']['fields']['sis_votes_average'] ){
      $vars['rating'] = theme('fivestar_static', $vars['result']['fields']['sis_votes_average']);
    }

    //uc_price_visibility
    $vars['add_to_cart'] = '';
    $vars['add_to_cart'] = isset($vars['result']['fields']['sell_price'])
      ? ''
      : theme_fix_product_search_clean_add_to_cart_form($node);


    $vars['sell_price'] = isset($vars['result']['fields']['sell_price'])
      ? $vars['result']['fields']['sell_price']
      : uc_currency_format($node->sell_price);

    $vars['image'] = theme_imagecache('product_list', $node->field_image_cache[0]['filepath']);
  }
  if ( intval($vars['id']) % 3 == 1 ){
    $vars['new_line'] = true;
  }
}

function vectur_preprocess_search_results(&$vars) {
  if ( module_exists('apachesolr') && !empty($vars['search_results']) && user_access('use advanced search') ){
    
    $rows = isset($_GET['rows'])
      ? $_GET['rows']
      : 12;
    $sort = isset($_GET['solrsort'])
      ? $_GET['solrsort']
      : 'fs_uc_sell_price desc';

    $form_top = array(
      '#type' => 'form',
      '#id' => 'solr-filters-form-top',
      '#method' => 'get',
      'filters' => array(
        '#type' => 'select',
//          '#title' => t('Filter by'),
        '#name' => 'filters',
        '#id' => 'apachesolr-results-filter',
        '#options' => array(
          '' => t('Filter By'),
          'type:product' => t('Products'),
          'type:product_document' => t('Product Documents'),
          'type:news' => t('News and Updates'),
        ),
        '#value' => $_GET['filters'],
        '#weight' => 5,
      ),
      "solrsort" => array(
        '#type' => 'select',
        '#name' => 'solrsort',
        '#id' => 'apachesolr-results-sort',
        '#options' => array(
          'fs_uc_sell_price desc' => t('Highest to lowest price'),
          'fs_uc_sell_price asc' => t('Lowest to highest price'),
          'sis_votes_average desc' => t('Best rated'),
          'fs_common_num_sold desc' => t('Most popular'),
        ),
        '#value' => $sort,
        '#weight' => 10,
      ),
      "rows" => array(
        '#type' => 'select',
        '#name' => 'rows',
        '#value' => $rows,
        '#title' => t('Items per page'),
        '#id' => 'apachesolr-results-rows',
        '#options' => array(
          '2' => 2,
          '12' => 12,
          '24' => 24,
          '48' => 48,
          '72' => 72,
        ),
        '#weight' => 20,
      ),
    );
    $vars['form_top'] = drupal_render($form_top);
  }
}
