<?php
/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
 global $BASE_URL;
drupal_add_js(drupal_get_path('theme', 'cleancommerce') . '/js/jquery-ui-1.7.3.custom.min.js', 'theme');
drupal_add_css(drupal_get_path('theme', 'cleancommerce') . '/css/ui-lightness/jquery-ui-1.7.3.custom.css', 'theme', 'all', false);
drupal_add_js(drupal_get_path('theme', 'cleancommerce') . '/js/slider.js', 'theme');
drupal_add_js($BASE_URL.'misc/drupal.js');
drupal_add_js($BASE_URL.'misc/collapse.js');

?>
<div id="block-<?php print $block->module . '-' . $block->delta; ?>" class="block block-<?php print $block->module ?> <?php print $block_zebra; ?> <?php print $position; ?> <?php print $skinr; ?>">
  <div class="inner clearfix">
    <?php if (isset($edit_links)): ?>
    <?php print $edit_links; ?>
    <?php endif; ?>
    <?php if ($block->subject): ?>
    <div class="icon-product pngfix"></div>
    <div class="block-icon pngfix"></div>
    <h2 class="title block-title"><?php print $block->subject ?></h2>
    <?php endif;?>
    <div id="solr-price-filter-slider"></div>
    <div class="price-text"></div>
    <div id="solr-price-filter-submit-link-wrapper" style="display: none"><a href="" id="solr-price-filter-submit-link">Go</a></div>
    <div class="content clearfix">
      <?php print $block->content; ?>
    </div>
  </div>
</div>
