/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
if ( Drupal.jsEnabled ){
  Drupal.behaviors.solrSearchAutoSubmit = function(){
    var $form = $('form[id^="solr-filters-form-top"]');
    $form.each(function(index){
      var $f = $(this);
      $f.find("select").change(function(){
        $f.submit();
      });
      $f.find("input").change(function(){
        $f.submit();
      });
    });
  }
}