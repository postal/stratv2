if (Drupal.jsEnabled) {
  Drupal.behaviors.vectur_filters_slider = function() {
    // NOTE: the exposed filter was given a filter identifier of "slider_filter".  Views
    // adds the "edit-" and "-min/-max" parts.
    var min = $('input#edit-price-min');
    var max = $('input#edit-price-max');
    var text = $("div#price-text");

    if (!min.length || !max.length) {
      // No min/max elements on this page
      return;
    }

    // Set default values or use those passed into the form
    var init_min = ('' == min.val()) ? 0 : min.val();
    var init_max = ('' == max.val()) ? 100 : max.val();

    $("select#edit-stored-price option[value='All']"). remove();
    $("select#edit-stored-price option").each(function(){

      if ($(this).val() < init_min){
        init_min = $(this).val();
      }
      if ( $(this).val() > init_max) {
        init_max = $(this).val();
      }
    });
    $("select#edit-stored-price").remove();

    // Set initial values of the slider
    min.val(init_min);
    max.val(init_max);
    
    text.text( parseInt(init_min) + "$ to " + parseInt(init_max) + "$");

    // Insert the slider before the min/max input elements
    $("#sell-price-slider").slider({
      range: true,
      min: init_min,     // Adjust slider min and max to the range
      max: init_max,    // of the exposed filter.
      values: [init_min, init_max],
      slide: function(event, ui){
        // Update the form input elements with the new values when the slider moves
        min.val(ui.values[0]);
        max.val(ui.values[1]);
        text.text( ui.values[0] + "$ to " + ui.values[1] + "$");
      },
      stop: function(event, ui){
        //autosubmit binded on keyup event
        max.trigger('keyup');
      }
    });
    min.parents('div.views-widget').hide();
  };

//apache solr slider
  function apachesolr_ubercart_range_filter_link(template, param1, param2){
		return template.replace('*', param1).replace('*', param2);
  }

  Drupal.behaviors.vectur_solr_price_filter_slider = function(){
    var min = $('#apachesolr-ubercart-price-form #edit-from');
    var max = $('#apachesolr-ubercart-price-form #edit-to');
    var text = $("#block-apachesolr_ubercart-fs_uc_sell_price .price-text");

    var url_template = $("#apachesolr-ubercart-price-form").attr("action").replace('fs_uc_sell_price%3A%5B%2A%20TO%20%2A%5D', 'fs_uc_sell_price%3A%5B*%20TO%20*%5D');
    $("#solr-price-filter-submit-link-wrapper").show();
    var link = $("#solr-price-filter-submit-link");

    if (!min.length || !max.length) {
      // No min/max elements on this page
      return;
    }

    // Set default values or use those passed into the form
    var init_min = ('' == min.val()) ? 0 : parseInt(min.val());
    var init_max = ('' == max.val()) ? 100 : parseInt(max.val());

    // Set initial values of the slider
    min.val(init_min);
    max.val(init_max);
    
    text.text( parseInt(init_min) + "$ to " + parseInt(init_max) + "$");
    link.attr('href', apachesolr_ubercart_range_filter_link(url_template, init_min, init_max));

    // Insert the slider before the min/max input elements
    $("#solr-price-filter-slider").slider({
      range: true,
      min: init_min,     // Adjust slider min and max to the range
      max: init_max,    // of the exposed filter.
      values: [init_min, init_max],
      slide: function(event, ui){
        // Update the form input elements with the new values when the slider moves
        min.val(ui.values[0]);
        max.val(ui.values[1]);
        text.text( ui.values[0] + "$ to " + ui.values[1] + "$");
      },
      stop: function(event, ui){
        link.attr('href', apachesolr_ubercart_range_filter_link(url_template, '' + ui.values[0], '' + ui.values[1]));
      }
    });
    $("#apachesolr-ubercart-price-form").hide();
  };
}
