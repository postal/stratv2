// $Id: fusion-cleancommerce-script.js 6900 2010-03-22 22:26:41Z sheena $

Drupal.behaviors.fusionSuperfish = function (context) {
  $("#primary-menu ul.sf-menu").superfish({
    hoverClass: 'sfHover',
    delay: 250,
    animation: {opacity:'show',height:'show'},
    speed: 'fast',
    autoArrows: true,
    dropShadows: false,
    disableHI: true
  }).supposition();
};

//js support for rounded corners across all browsers
Drupal.behaviors.fusionRoundedCorners = function (context) {

//identify objects that should have all rounded corners
  $(".block.rounded-corners .inner").addClass("rounded-all");
  $(".view.rounded-corners .inner.content").addClass("rounded-all");
  $(".list-background.rounded-corners .inner").removeClass("rounded-all");
  
//identify list items that should have top corners rounded
  $(".list-background.rounded-corners div.inner").find("li:first").addClass("very-first");  
   //views edit links should not be included in this
  $(".list-background.rounded-corners div.inner .views-admin-links .very-first").removeClass("very-first");  
  $(".list-background.rounded-corners div.inner .view-content").find("li:first").addClass("very-first");  
  
  //multi column lists are a little different  
 $(".list-background.rounded-corners.fusion-multicol-menu").find(".very-first").removeClass("very-first");
 $(".list-background.rounded-corners.fusion-multicol-menu ul.menu > li").addClass("menucol-first");
 $(".list-background.rounded-corners.fusion-multicol-menu ul.menu ul.menu > li").removeClass("menucol-first");		  

  //identify list items that should have bottom corners rounded
  $(".list-background.rounded-corners div.inner").find("li:last").addClass("very-last");  
  //views edit links should not be included in this
  $(".list-background.rounded-corners div.inner .views-admin-links .very-last").removeClass("very-last");  
  $(".list-background.rounded-corners div.inner .view-content").find("li:last").addClass("very-last");  
  
  //multi column lists are a little different
  $(".list-background.rounded-corners.fusion-multicol-menu").find(".very-last").removeClass("very-last");
  $(".list-background.rounded-corners.fusion-multicol-menu ul.menu li.menucol-first").find("li:last").addClass("menucol-last");    
  
  //vertical superfish menus are also different
  $(".list-background.rounded-corners ul.sf-vertical li.last").addClass("very-last");
  $(".list-background.rounded-corners ul.sf-vertical ul.menu").find(".very-last").removeClass("very-last");
  
  //identify list items with left corners rounded 
  $(".list-background.rounded-corners.fusion-inline-menu ul.menu li.first").removeClass("very-first").addClass("inlinemenu-first");
  $(".list-background.rounded-corners.fusion-inline-menu ul.menu ul.menu > li").removeClass("inlinemenu-first");
    
  //identify list items with right corners rounded 
  $(".list-background.rounded-corners.fusion-inline-menu ul.menu li.last").removeClass("very-last").addClass("inlinemenu-last");
  $(".list-background.rounded-corners.fusion-inline-menu ul.menu ul.menu > li").removeClass("inlinemenu-last");
    			  
  //create wrappers for rounded tops
  $(".list-background.rounded-corners .very-first").wrapInner('<div class="rounded-top" />');
  $(".list-background.rounded-corners.fusion-multicol-menu .menucol-first > a").wrap('<div class="rounded-top" />');
  
  //create wrappers for rounded bottoms	  			  
  $(".list-background.rounded-corners .very-last").wrapInner('<div class="rounded-bottom" />');
  $(".list-background.rounded-corners.fusion-multicol-menu .menucol-last").wrapInner('<div class="rounded-bottom" />');
 		  
  //create wrappers for rounded left corners
  $(".list-background.rounded-corners.fusion-inline-menu .inlinemenu-first").wrapInner('<div class="rounded-left" />');
  
  //create wrappers for rounded right corners
  $(".list-background.rounded-corners.fusion-inline-menu .inlinemenu-last").wrapInner('<div class="rounded-right" />');
			  
  //round top corners
  $(".rounded-top").corner({
    tl: { radius: 11 },
    tr: { radius: 11 },
    bl: { radius: 0 },
    br: { radius: 0 },
    antiAlias: true,
    validTags: ["div"] 
  });
  //round right corners			  
  $(".rounded-right").corner({
    tl: { radius: 0 },
    tr: { radius: 11 },
    bl: { radius: 0 },
    br: { radius: 11 },
    antiAlias: true,
    validTags: ["div"] 
  });			  
  //round bottom corners			  
  $(".rounded-bottom").corner({
    tl: { radius: 0 },
    tr: { radius: 0 },
    bl: { radius: 11 },
    br: { radius: 11 },
    antiAlias: true,
    validTags: ["div"]
  });
  //round left corners			  
  $(".rounded-left").corner({
    tl: { radius: 11 },
    tr: { radius: 0 },
    bl: { radius: 11 },
    br: { radius: 0 },
    antiAlias: true,
    validTags: ["div"]
  });			  
    //round all corners
  $(".rounded-all").corner({
    tl: { radius: 11 },
    tr: { radius: 11 },
    bl: { radius: 11 },
    br: { radius: 11 },
    antiAlias: true,
    validTags: ["div"]
  });  	
  
  //fixes for IE 6 & 7 
  if ($.browser.msie && $.browser.version.substr(0,1)<8) {
    $('#main-content-inner .rounded-all #cctopcontainer').css('marginRight', 'auto');
    $('#main-content-inner .rounded-all #cctopcontainer').css('left', '21');
    $('#main-content-inner .rounded-all #ccbottomcontainer').css('marginRight', 'auto');
    $('#main-content-inner .rounded-all #ccbottomcontainer').css('left', '21');
  }			  
}; //end js support for rounded corners

