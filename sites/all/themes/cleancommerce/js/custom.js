/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
if ( Drupal.jsEnabled ){
  Drupal.behaviors.vecturFiltersSlider = function() {
    $("#sorting-form select").not('.processed').bind('change', function(){$("#sorting-form").submit();});
    $("#sorting-form select").not('.processed').addClass('processed');
    
//    cval = $("#edit-ipp").val();
//    $("#products-items-per-page [value='" + cval + "']").attr("selected", "selected");
//    var $exposed_form = $('form[id^="views-exposed-form-uc-product-catalog"]');
//    
//    $("form[id^='views-exposed-form-uc-product-catalog']").find('select[name$="sort"]').unbind();
//
//    $("#products-items-per-page").change(function(){
//      $("#edit-ipp").val($(this).val());
//      $exposed_form.submit();
//    });
//    if ( $("#edit-sell-price-sort").val() == "DESC" ) {
//      $('#sort-products option[value="price_down"]').attr('selected', 'selected');
//    }else if ( $("#edit-sell-price-sort").val() == "ASC" ) {
//      $('#sort-products option[value="price_up"]').attr('selected', 'selected');
//    }else if ( $("#edit-value-sort").val() == 'DESC' ){
//      $('#sort-products option[value="rating"]').attr('selected', 'selected');
//    }else if ( $("#edit-order-count-sort").val() == 'ASC' ){
//      $('#sort-products option[value="popular"]').attr('selected', 'selected');
//    }
//
//    $("#sort-products").change(function(){
//      $("#edit-value-sort").val("unsorted");
//      $("#edit-sell-price-sort").val("unsorted");
//      $("#edit-order-count-sort").val("unsorted");
//      switch ($(this).val()){
//        case "price_down":
//          $("#edit-sell-price-sort").val("DESC");
//          break;
//        case "price_up":
//          $("#edit-sell-price-sort").val("ASC");
//          break;
//        case "rating":
//          $("#edit-value-sort").val("DESC");
//          break;
//        case "popular":
//          $("#edit-order-count-sort").val("DESC");
//          break;
//      }
//      $exposed_form.submit();
//    });
  }
}
