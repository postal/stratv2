<?php

/*
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

function cleancommerce_preprocess_node(&$vars) {
  $criteria = array(
    'content_id' => $vars['nid']
  );

  if ( module_exists('uc_product') && uc_product_is_product($vars) && $vars['template_files'][0] == 'node-product' ){
    $node = node_build_content(node_load($vars['nid']));
    $vars['fivestar_widget'] = drupal_render($node->content['fivestar_widget']);
    $vars['description'] = drupal_render($node->content['body']);
    $vars['product_details'] = drupal_render($node->content['product_details']);
    $vars['technical_details'] = drupal_render($node->content['technical_details']);
    if ( $node->field_deal_active[0] != 'off' ){
      $vars['product_deals'] = drupal_render($node->content['group_deals']['group']['field_deal_description']);
      $vars['product_deals_dates'] = drupal_render($node->content['group_deals']['group']['field_promotion_dates']);
    }
    //rendering upsell block
    $block = module_invoke('uc_upsell_jcarousel_block', 'block', 'view', 0);
    $vars['related_products'] = $block['content'];
  }
}

function cleancommerce_preprocess_page(&$vars) {
  if ( $vars['logged_in'] || $vars['body_id'] != 'pid-user' ){
    $vars['grid_width'] = 'fluid-100';
    module_load_include('inc', 'vectur_filters', 'menu_builder');
    $f = create_function(
        '$term',
        '
          return array(
            "href" => "catalog/" . $term->tid,
            "query" => array(
            ),
          );
        '
    );

    $catalog = taxonomy_build_tree(array(
        'terms' => taxonomy_get_tree(variable_get('uc_catalog_vid', 0)),
        'query_callback' => $f,
        'flat' => false,
        'root_item' => array(
          'title' => 'Catalog',
          'href' => 'catalog',
        ),
        'id_prefix' => 'catalog',
      ));
    $catalog_menu = menu_tree_output($catalog);
    $catalog_menu = preg_replace('/<ul class="menu/i', '<ul class="menu sf-menu', $catalog_menu, 1);

    $cart = l(
        '<img src="/' . drupal_get_path('theme', 'cleancommerce') . '/images/cart.png">',
        'cart',
        array(
          'html' => true,
          'attributes' => array(
            'id' => 'cart-menu-link'
          ),
        )
    );
    $vars['primary_links_tree'] = <<<END
    <div id="catalog_container">{$catalog_menu}</div>
    <div id="primary_menu_contaier">
      {$vars['primary_links_tree']}
      <ul class="menu sf-menu"><li>{$cart}</li></ul>
    </div>
END;
  }else{
    $vars['template_files'] = array(
      'startup_page',
    );
  }
}

function cleancommerce_theme($existing, $type, $theme, $path) {
  return array(
    'theme_uc_product_image' => array(
      'function' => 'cleancommerce_uc_product_image',
      'arguments' => array( 'images' => array( ), 'teaser' => 0, 'page' => 0 ),
    )
  );
}

/**
 * Formats a product's images with imagecache and an image widget
 * (Colorbox, Thickbox, or Lightbox2).
 *
 * @ingroup themeable
 */
function cleancommerce_uc_product_image($images, $teaser = 0, $page = 0) {
  static $rel_count = 0;
  // Get the current product image widget.
  $image_widget = uc_product_get_image_widget();

  $first = reset($images);

  $output = '<div class="product-image"><div class="main-product-image">';
  $output .= '<a href="' . imagecache_create_url('product', $first['filepath']) . '" title="' . $first['data']['title'] . '"';
  if ( $image_widget ){
    $image_widget_func = $image_widget['callback'];
    $output .= $image_widget_func($rel_count);
  }
  $output .= '>';
  $output .= theme('imagecache', 'product', $first['filepath'], $first['data']['alt'], $first['data']['title']);
  $output .= '</a></div><div class="more-product-images">';
  foreach( $images as $thumbnail ){
    // Node preview adds extra values to $images that aren't files.
    if ( !is_array($thumbnail) || empty($thumbnail['filepath']) ){
      continue;
    }
    $output .= '<a href="' . imagecache_create_url('product', $thumbnail['filepath']) . '" title="' . $thumbnail['data']['title'] . '"';
    if ( $image_widget ){
      $output .= $image_widget_func($rel_count);
    }
    $output .= '>';
    $output .= theme('imagecache', 'uc_thumbnail', $thumbnail['filepath'], $thumbnail['data']['alt'], $thumbnail['data']['title']);
    $output .= '</a>';
  }
  $output .= '</div></div>';
  $rel_count++;

  return $output;
}

function cleancommerce_preprocess_search_result(&$vars) {
  $title = substr($vars['title'], 0, 55);
  if ( strlen($title) == 55 ){
    $title = substr($title, 0, strrpos($title, ' ')) . ' ...';
    $vars['title'] = $title;
  }
  if ( $vars['info_split']['type'] == 'Product' ){
    $node = node_load($vars['result']['node']->nid);

    if ( $vars['result']['fields']['sis_votes_average'] ){
      $vars['rating'] = theme('fivestar_static', $vars['result']['fields']['sis_votes_average']);
    }

    //uc_price_visibility
    $vars['add_to_cart'] = '';
//    $vars['add_to_cart'] = isset($vars['result']['fields']['sell_price'])
//      ? ''
//      : theme_uc_product_add_to_cart($node);

    $vars['sell_price'] = isset($vars['result']['fields']['sell_price'])
      ? $vars['result']['fields']['sell_price']
      : uc_currency_format($node->sell_price);

    $vars['image'] = theme_imagecache('product_list', $node->field_image_cache[0]['filepath']);
  }
  if ( intval($vars['id']) % 3 == 1 ){
    $vars['new_line'] = true;
  }
}

function cleancommerce_preprocess_search_results(&$vars) {
  if ( module_exists('apachesolr') && !empty($vars['search_results']) && user_access('use advanced search') ){
    $rows = isset($_GET['rows'])
      ? $_GET['rows']
      : 12;
    $sort = isset($_GET['solrsort'])
      ? $_GET['solrsort']
      : 'fs_uc_sell_price desc';

    $form_top = array(
      '#type' => 'form',
      '#id' => 'solr-filters-form-top',
      '#method' => 'get',
      "solrsort" => array(
        '#type' => 'select',
        '#name' => 'solrsort',
        '#id' => 'apachesolr-results-sort',
        '#options' => array(
          'fs_uc_sell_price desc' => t('Highest to lowest price'),
          'fs_uc_sell_price asc' => t('Lowest to highest price'),
          'sis_votes_average desc' => t('Best rated'),
          'fs_common_num_sold desc' => t('Most popular'),
        ),
        '#value' => $sort,
        '#weight' => 10,
      ),
      "rows" => array(
        '#type' => 'select',
        '#name' => 'rows',
        '#value' => $rows,
        '#title' => t('Items per page'),
        '#id' => 'apachesolr-results-rows',
        '#options' => array(
          '2' => 2,
          '12' => 12,
          '24' => 24,
          '48' => 48,
          '72' => 72,
        ),
        '#weight' => 20,
      ),
      'filters' => array(
        '#type' => 'select',
//        '#title' => t('Filter by'),
        '#name' => 'filters',
        '#id' => 'apachesolr-results-filter',
        '#options' => array(
          '' => t('Filter by'),
          'type:product' => t('Products'),
          'type:product_document' => t('Product Documents'),
          'type:news' => t('News and Updates'),
        ),
        '#value' => $_GET['filters'],
        '#weight' => 5,
      ),
//      'filters' => array(
//        '#type' => 'hidden',
//        '#name' => 'filters',
//        '#value' => $_GET['filters'],
//      ),
    );
//    if ( arg(0) != 'catalog' ){
//      $form_bottom = array(
//        '#type' => 'form',
//        '#id' => 'solr-filters-form-bottom',
//        '#method' => 'get',
//        'filters' => array(
//          '#type' => 'select',
//          '#title' => t('Filter by'),
//          '#name' => 'filters',
//          '#id' => 'apachesolr-results-filter',
//          '#options' => array(
//            '' => t('Select type'),
//            'type:product' => t('Products'),
//            'type:product_document' => t('Product Documents'),
//            'type:news' => 'News and Updates',
//          ),
//          '#value' => $_GET['filters'],
//          '#weight' => 40,
//        ),
//        "solrsort" => array(
//          '#type' => 'hidden',
//          '#name' => 'solrsort',
//          '#id' => 'apachesolr-results-sort-bottom',
//          '#value' => $sort,
//        ),
//        "rows" => array(
//          '#type' => 'hidden',
//          '#name' => 'rows',
//          '#value' => $rows,
//          '#id' => 'apachesolr-results-rows-bottom',
//        ),
//      );
//      $vars['form_bottom'] = drupal_render($form_bottom);
//    }
    $vars['form_top'] = drupal_render($form_top);
  }
}
