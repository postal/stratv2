; $Id: fusion_cleancommerce.info 7039 2010-04-01 21:34:34Z sheena $

name = Clean Commerce
description = Clean Commerce sub-theme. Requires <a href="http://drupal.org/project/fusion">Fusion Core</a> and the <a href="http://drupal.org/project/skinr">Skinr</a> module to enable easy point-and-click theming.
base theme = commons_roots
core = 6.x
engine = phptemplate

stylesheets[all][] = css/cleancommerce-style.css
stylesheets[all][] = css/ui-lightness/jquery-ui-1.7.3.custom.css

; Scripts
scripts[] = js/jquery.curvycorners.min.js
scripts[] = js/cleancommerce-script.js
scripts[] = js/jquery.watermarkinput.js

regions[sidebar_first] = sidebar first
regions[sidebar_last] = sidebar last
regions[header_top] = header top
regions[header] = header
regions[preface_top] = preface top
regions[preface_bottom] = preface bottom
regions[content_top] = content top
regions[content] = content
regions[content_bottom] = content bottom
regions[postscript_top] = postscript top
regions[postscript_bottom] = postscript bottom
regions[footer] = footer
regions[node_top] = node top
regions[node_bottom] = node bottom
regions[mainbar] = main bar

features[] = logo
features[] = name
features[] = slogan
features[] = node_user_picture
features[] = comment_user_picture
features[] = search
features[] = favicon
features[] = primary_links
features[] = secondary_links

; Default theme settings
settings[theme_grid] = grid16-960
settings[theme_grid_options][] = grid16-960
settings[theme_grid_options][] = grid16-fluid
settings[fluid_grid_width] = fluid-100
settings[theme_font_size] = font-size-12
settings[sidebar_layout] = sidebars-split
settings[sidebar_first_width] = 3
settings[sidebar_last_width] = 3

; Inherit Skinr styles from parent themes
skinr[options][inherit_skins] = true

skinr[grid16-width][title] = Width (16 column grid)
skinr[grid16-width][type] = select
skinr[grid16-width][features][] = block
skinr[grid16-width][options][1][label] = 1 unit wide (60px/6.25%)
skinr[grid16-width][options][1][class] = grid16-1
skinr[grid16-width][options][2][label] = 2 units wide (120px/12.5%)
skinr[grid16-width][options][2][class] = grid16-2
skinr[grid16-width][options][3][label] = 3 units wide (180px/18.75%)
skinr[grid16-width][options][3][class] = grid16-3
skinr[grid16-width][options][4][label] = 4 units wide (240px/25%)
skinr[grid16-width][options][4][class] = grid16-4
skinr[grid16-width][options][5][label] = 5 units wide (300px/31.25%)
skinr[grid16-width][options][5][class] = grid16-5
skinr[grid16-width][options][6][label] = 6 units wide (360px/37.5%)
skinr[grid16-width][options][6][class] = grid16-6
skinr[grid16-width][options][7][label] = 7 units wide (420px/43.75%)
skinr[grid16-width][options][7][class] = grid16-7
skinr[grid16-width][options][8][label] = 8 units wide (480px/50%)
skinr[grid16-width][options][8][class] = grid16-8
skinr[grid16-width][options][9][label] = 9 units wide (540px/56.25%)
skinr[grid16-width][options][9][class] = grid16-9
skinr[grid16-width][options][10][label] = 10 units wide (600px/62.5%)
skinr[grid16-width][options][10][class] = grid16-10
skinr[grid16-width][options][11][label] = 11 units wide (660px/68.75%)
skinr[grid16-width][options][11][class] = grid16-11
skinr[grid16-width][options][12][label] = 12 units wide (720px/75%)
skinr[grid16-width][options][12][class] = grid16-12
skinr[grid16-width][options][13][label] = 13 units wide (780px/81.25%)
skinr[grid16-width][options][13][class] = grid16-13
skinr[grid16-width][options][14][label] = 14 units wide (840px/87.5%)
skinr[grid16-width][options][14][class] = grid16-14
skinr[grid16-width][options][15][label] = 15 units wide (900px/93.75%)
skinr[grid16-width][options][15][class] = grid16-15
skinr[grid16-width][options][16][label] = 16 units wide (960px/100%)
skinr[grid16-width][options][16][class] = grid16-16
;
skinr[grid16-width][options][50][label] = 50% wide
skinr[grid16-width][options][50][class] = grid16-50
skinr[grid16-width][options][100][label] = 100% wide
skinr[grid16-width][options][100][class] = grid16-100


; Disable 12 column grid block widths inherited from fusion_core
skinr[grid12-width] =

;Override General Styles from Fusion Core
skinr[fusion-general-styles][title] = General styles
skinr[fusion-general-styles][type] = checkboxes
skinr[fusion-general-styles][description] = These are some generally useful options for blocks
skinr[fusion-general-styles][features][] = block
skinr[fusion-general-styles][features][] = panels_display
skinr[fusion-general-styles][features][] = panels_pane
skinr[fusion-general-styles][features][] = panels_panel
skinr[fusion-general-styles][options][1][label] = <span class="preview-text">Text: large, bold callout style <span class="preview-icon" id="fusion-general-styles-fusion-callout"></span></span>
skinr[fusion-general-styles][options][1][class] = fusion-callout
skinr[fusion-general-styles][options][2][label] = <span class="preview-text">Links: bold all links <span class="preview-icon" id="fusion-general-styles-bold-links"></span></span>
skinr[fusion-general-styles][options][2][class] = fusion-bold-links
skinr[fusion-general-styles][options][3][label] = <span class="preview-text">Padding: add 30px extra padding inside block <span class="preview-icon" id="fusion-general-styles-fusion-padding"></span></span>
skinr[fusion-general-styles][options][3][class] = fusion-padding

; Override stackable list/menu styles from Fusion Core
skinr[fusion-list-styles][title] = List/menu styles
skinr[fusion-list-styles][type] = checkboxes
skinr[fusion-list-styles][features][] = block
skinr[fusion-list-styles][features][] = panels_display
skinr[fusion-list-styles][features][] = panels_pane
skinr[fusion-list-styles][features][] = panels_panel
skinr[fusion-list-styles][features][] = views_view
skinr[fusion-list-styles][options][1][label] = <span class="preview-text">Bottom border<span class="preview-icon" id="list-styles-bottom-border"></span></span>
skinr[fusion-list-styles][options][1][class] = fusion-list-bottom-border
skinr[fusion-list-styles][options][2][label] = <span class="preview-text">Remove Bullets <span class="preview-icon" id="list-styles-no-bullets"></span></span>
skinr[fusion-list-styles][options][2][class] = fusion-list-no-bullets

; Clean Commerce - Color Skin
skinr[cleancommerce-color-styles][title] = Clean Commerce - Color Skin
skinr[cleancommerce-color-styles][type] = radios
skinr[cleancommerce-color-styles][features][] = block
skinr[cleancommerce-color-styles][features][] = panels_display
skinr[cleancommerce-color-styles][features][] = panels_pane
skinr[cleancommerce-color-styles][features][] = panels_panel
skinr[cleancommerce-color-styles][features][] = views_view
skinr[cleancommerce-color-styles][options][0][label] = <span class="preview-text">Gray <span class="preview-icon" id="gray-skin"></span></span>
skinr[cleancommerce-color-styles][options][0][class] = color-gray
skinr[cleancommerce-color-styles][options][1][label] = <span class="preview-text">Fuschia <span class="preview-icon" id="fuschia-skin"></span></span>
skinr[cleancommerce-color-styles][options][1][class] = color-fuschia
skinr[cleancommerce-color-styles][options][2][label] = <span class="preview-text">Blue <span class="preview-icon" id="blue-skin"></span></span>
skinr[cleancommerce-color-styles][options][2][class] = color-blue
skinr[cleancommerce-color-styles][options][3][label] = <span class="preview-text">Green <span class="preview-icon" id="green-skin"></span></span>
skinr[cleancommerce-color-styles][options][3][class] = color-green

; Clean Commerce - General Styles
skinr[cleancommerce-general-styles][title] = Clean Commerce - General Styles
skinr[cleancommerce-general-styles][type] = checkboxes
skinr[cleancommerce-general-styles][features][] = block
skinr[cleancommerce-general-styles][features][] = panels_display
skinr[cleancommerce-general-styles][features][] = panels_pane
skinr[cleancommerce-general-styles][features][] = panels_panel
skinr[cleancommerce-general-styles][features][] = views_view
skinr[cleancommerce-general-styles][options][1][label] = <span class="preview-text">Add background color<span class="preview-icon" id="cleancommerce-background-color"></span></span>
skinr[cleancommerce-general-styles][options][1][class] = background-color
skinr[cleancommerce-general-styles][options][2][label] = <span class="preview-text">Add 1px border<span class="preview-icon" id="cleancommerce-thin-border"></span></span>
skinr[cleancommerce-general-styles][options][2][class] = border
skinr[cleancommerce-general-styles][options][3][label] = <span class="preview-text">Add menu/list background shading<span class="preview-icon" id="cleancommerce-list-background"></span></span>
skinr[cleancommerce-general-styles][options][3][class] = list-background
skinr[cleancommerce-general-styles][options][4][label] = <span class="preview-text">Add rounded corners<span class="preview-icon" id="cleancommerce-rounded-corners"></span></span>
skinr[cleancommerce-general-styles][options][4][class] = rounded-corners

; Information added by fusiondrupalthemes.com packaging script on 2010-04-01
version = 6.x-1.0
project = cleancommerce
datestamp = 1270160404
project status url = http://updates.fusiondrupalthemes.com/release-history

