<?php
/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */
if ( isset($new_line) and $new_line ){
  print "<tr>";
}
?>
<td class="search-result-item">
<?php
if($info_split['type'] == 'Product'): ?>
<div class="list-product">
<div class="list-image">
<?php
  print $image;
?>
</div>
<div class="list-content">
<dt class="title"><a href="<?php print $url; ?>"><?php print $title ?></a></dt>
<div class="">Seller: <?php print $info_split['user']; ?></div>
<div class="uc-price">Price: <?php print $sell_price; ?></div>
<div class="uc-rating rating_with_count"><?php echo $rating; ?></div>
<?php print $add_to_cart; ?>
</div>
<dd>
  <?php if (false && $snippet) : ?>
    <p class="search-snippet"><?php print $snippet; ?></p>
  <?php endif; ?>
  <?php if ($info) : ?>
  <p class="search-info"><?php // print $info; ?></p>
  <?php endif; ?>
</dd>
</div>
<?php else: ?>
<div>
<dt class="title">
  <a href="<?php print $url; ?>"><?php print $title; ?></a>
</dt>
<dd>
  <?php if ($snippet) : ?>
    <p class="search-snippet"><?php print $snippet; ?></p>
  <?php endif; ?>
  <?php if ($info) : ?>
  <p class="search-info"><?php //print $info; ?></p>
  <?php endif; ?>
</dd>
</div>
<?php endif ?>
</td>
