<?php
// $Id: views-exposed-form.tpl.php,v 1.4.4.1 2009/11/18 20:37:58 merlinofchaos Exp $
/**
 * @file views-exposed-form.tpl.php
 *
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
drupal_add_js(drupal_get_path('theme', 'cleancommerce') . '/js/jquery-ui-1.7.3.custom.min.js', 'theme');
drupal_add_css(drupal_get_path('theme', 'cleancommerce') . '/css/ui-lightness/jquery-ui-1.7.3.custom.css', 'theme');
drupal_add_js(drupal_get_path('theme', 'cleancommerce') . '/js/slider.js', 'theme');
module_load_include('inc', 'vectur_filters', 'fivestars_filter');

//add collapsible fieldset js if it is not already included
global $BASE_URL;
drupal_add_js($BASE_URL.'misc/drupal.js');
drupal_add_js($BASE_URL.'misc/collapse.js');
?>

<?php if (!empty($q)): ?>
  <?php
    // This ensures that, if clean URLs are off, the 'q' is added first so that
    // it shows up first in the URL.
    print $q;
  ?>
<?php endif; ?>
  <div class="views-exposed-wrapper clear-block">
  <div class="views-exposed-widgets clear-block">
    <div class="views-exposed-widget">
      <label>Brands:</label>
      <div class="views-widget">
        <?php print $widgets['filter-tid']->widget; ?>
      </div>
    </div>
    <div class="views-exposed-widget">
      <label>Price:</label>
      <div id="sell-price-slider"></div>
      <div class="views-widget">
        <?php print $widgets['filter-sell_price']->widget; ?>
      </div>
    </div>
      <div class="views-exposed-widget">
          <label>Rating:</label>
          <div class="views-widget">
          <?php
          for( $i = 5; $i >= 1; $i-- ){ // >= 20 because we don't need in 0 stars
            print theme(
                'fivestar_filter_static',
                $i * 20,
                '<label><input type="radio" name="rating" value="' . ($i * 20) . '"><div style="display: inline-block">',
                '</div></label>'
            );
          }
          ?>
          </div>
      </div>
      <div style="display: none;">
        <?php print $widgets['filter-sell_price_1']->widget; ?>
        <?php print $widgets['filter-ipp']->widget; ?>
        <?php print $widgets['sort-value']->widget; ?>
        <?php print $widgets['sort-order_count']->widget; ?>
        <?php print $widgets['sort-sell_price']->widget; ?>
      </div>
  </div>
  <div class="views-exposed-widget views-exposed-submit">
    <?php print $button ?>
  </div>
  </div>
