<?php
/* 
 *  All rights reserved, Yuri 'Jureth' Minin, J.Jureth@gmail.com, 2010-2011
 */

?>
<div class="main-bar-search-form" id="main-bar-search-form">
<div class="main-bar-search-form-inner clearfix">
  <?php print $block->content; ?>
</div>
</div>
